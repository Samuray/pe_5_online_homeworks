  "use strict";


  let image = document.querySelector(".header__img-icon");
  let menu = document.querySelector(".header__mobile-menu");

    image.addEventListener("click", function() {
        if (image.getAttribute("src") === "dist/img/icon-menu.png"){
           menu.style.display = "block";
            image.src = "dist/img/menu-icon.png"}
    else{
            menu.style.display = "none";
            image.src = "dist/img/icon-menu.png"}
    });
  window.addEventListener("resize", function() {
      if (window.matchMedia("(min-width: 768px)").matches) {
          menu.style.display = "none";
          image.src = "dist/img/icon-menu.png"
      }
  });

