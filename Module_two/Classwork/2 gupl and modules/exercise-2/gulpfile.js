"use strict";
 import gulp from "gulp";

 const moveJs = () => gulp.src("./src/scripts/**/*").pipe(gulp.dest("./dist/js"));
 const moveImg = () => gulp.src("./src/img/**/*").pipe(gulp.dest("./dist/img"));

 gulp.task("moveJs",moveJs);
 gulp.task("moveImg",moveImg);
 gulp.task("moveAll", gulp.series("moveJs","moveImg"));