"use strict";
 import gulp from "gulp";
 import concatCss from "gulp-concat-css";
 import cleanCSS from 'gulp-clean-css';
 import clean from "gulp-clean";
 import autoprefixer from 'gulp-autoprefixer';
 import terser from 'gulp-terser';

 const moveJs = () => gulp.src("./src/js/**/*").pipe(terser()).pipe(gulp.dest("./dist/js"));
 const moveImg = () => gulp.src("./src/image/**/*").pipe(gulp.dest("./dist/image"));
 const moveCss = () => gulp.src("./src/css/**/*")
     .pipe(concatCss("style.min.css"))
     .pipe(cleanCSS({compatibility: 'ie8'}))
     .pipe(autoprefixer({cascade: false}))
     .pipe(gulp.dest("./dist/css"));
 const watch = () => gulp.watch('src/**/*', gulp.series(moveJs,moveImg,moveCss));
 const cleanDist = () => gulp.src("dist/**/*", {read: false}).pipe(clean());

 gulp.task("moveJs",moveJs);
 gulp.task("moveImg",moveImg);
 gulp.task("moveCss",moveCss);
 gulp.task('watch', watch);
 gulp.task("cleanDist", cleanDist);

gulp.task("build", gulp.series("cleanDist","moveJs","moveImg","moveCss"));