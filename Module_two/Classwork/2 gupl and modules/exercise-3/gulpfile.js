"use strict";
 import gulp from "gulp";

 const moveJs = () => gulp.src("./src/scripts/**/*").pipe(gulp.dest("./dist/js"));
 const moveImg = () => gulp.src("./src/image/**/*").pipe(gulp.dest("./dist/image"));
 const moveCss = () => gulp.src("./src/css/**/*").pipe(gulp.dest("./dist/css"));
 const watch = () => gulp.watch('src/**/*', gulp.series(moveJs,moveImg,moveCss))

 gulp.task("moveJs",moveJs);
 gulp.task("moveImg",moveImg);
 gulp.task("moveCss",moveCss);
 gulp.task("build", gulp.series("moveJs","moveImg","moveCss"));
 gulp.task('watch', watch);