import gulp from "gulp";
import sass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(sass);

const buildStyles = () => gulp.src('./src/scss/*').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('./dist/css'));

gulp.task("buildStyles",buildStyles);