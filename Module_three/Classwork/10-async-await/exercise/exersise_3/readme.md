[API](https://rapidapi.com/malaaddincelik/api/fitness-calculator/)

Mets

Получить список активностей и отрисовать в виде `<option>` в `<select name="activity" id="activity">`

```js

const options = {
    method: 'GET',
    url: 'https://fitness-calculator.p.rapidapi.com/activities',
    params: {intensitylevel: '1'},
    headers: {
        'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
        'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
    }
};

```

По клику на `<button>Get Activity Calories</button>` рассчитывать и выводить на экран количество потраченных калорий;

```js

var options = {
  method: 'GET',
  url: 'https://fitness-calculator.p.rapidapi.com/burnedcalorie',
  params: {activityid: 'bi_1', activitymin: '25', weight: '75'},
  headers: {
    'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
    'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
  }
};

```
