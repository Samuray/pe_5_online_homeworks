const express = require('express');
const colors = require('colors');
const cors = require('cors')
const posts = require('./scripts/data');
const app = express();
app.use(cors());

let counter = 1;

app.get('/', (_, response) => {
    const timeout = counter % 2 > 0 ? 30000 : 1000;
    counter++;
    setTimeout(() => {
        response.status(200);
        const body = {
            userId: Math.floor(Math.random() * 9),
            data: [...posts],
            status: 'success'
        }
        response.send(JSON.stringify(body));
    }, timeout)
})

app.listen(3000, () => {
    console.log('================================================'.white)
    console.log('Host is listening on PORT: 3000'.white);
    console.log('http://localhost:3000/'.cyan);
    console.log('================================================'.white)
});
