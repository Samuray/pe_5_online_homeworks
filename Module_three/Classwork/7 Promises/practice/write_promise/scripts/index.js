// Напишите функцию getImportantPostsId которая будут имитировать асинхронность и возвращать Promise,
// результатом исполнения которого будет массив с id важных постов в JSON формате. Промис должен разрешиться через 3 секунды.
// Напишите функцию getProduct которая будут имитировать асинхронность и возвращать Promise,
// результатом исполнения которого будет массив всех постов в JSON формате. Промис должен разрешиться через 4 секунды.
// Используя написанные функции выведите в консоль только важные посты. Используйте функционал Promise и метод then()




const importantPosts = [
    {
        id: 1,
    },{
        id: 3,
    },{
        id: 12,
    },{
        id: 16,
    },{
        id: 45,
    },{
        id: 65,
    },{
        id: 77,
    },{
        id: 90,
    },
];



const url = "./productsPost.json";
const posts = [];
fetch(url).then(res => res.json()).then(data => {
    data.forEach(elem => {
        posts.push(elem);
        return posts;
    })
});

const getImportantPostsId = () => new Promise(resolve => {
    setTimeout(() => {
        resolve(JSON.stringify(importantPosts));
    },3000);
});
const getPosts = () => new Promise(resolve => {
    setTimeout(() => {
        resolve (JSON.stringify(posts));
    },4000);
});

getImportantPostsId().then(postsId => {
    const mappedId = JSON.parse(postsId).map(item => item.id);
    console.log(mappedId);
    getPosts().then(posts => {
        const impotentPosts = JSON.parse(posts).filter(item => mappedId.includes(item.id));
        console.log(impotentPosts);
    });
});



