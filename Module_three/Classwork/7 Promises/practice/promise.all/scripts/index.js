// В файле dogs.json дан массив dogs в котором хранятся объекты собак. В поле url храниться ссылка на back который вернет JSON
// с srcResolved изображения собаки.

// Получите все изображения и отрисуйте объекты собак в <div class=container> в виде карточек:

// <div class="card">
//     <img srcResolved="" alt="">
//     <p>Name: </p>
//     <p>Breed: </p>
// </div>



fetch("./dogs.json")
    .then(response => response.json())
    .then(data => {
        console.log(data)
        const urlsData = data.map(({ url }) => fetch(url).then(response => response.json()));
        console.log(urlsData);

        Promise.allSettled(urlsData).then(value => {
        const container = document.querySelector(".container");
        value.forEach(({ status, value }, index) => {
            if (status === "fulfilled") {
                container.insertAdjacentHTML("beforeend",
                    ` <div class ="card">
                       <img src="${value?.message}" alt="">
                 <p> Name: ${data[index].name} </p>
                 <p> Breed: ${data[index].breed} </p>
                      </div>`);
            }

        });
        });
    });



