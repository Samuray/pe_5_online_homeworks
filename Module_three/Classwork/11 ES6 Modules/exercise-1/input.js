
class Input {
    constructor(className,type,id) {
        this.className = className
        this.type = type
        this.id = id
    }
    render() {
        const input = document.createElement("input");
        input.className = this.className;
        input.type = this.type;
        input.id = this.id;

        document.body.prepend(input);
    }
}

export default Input;