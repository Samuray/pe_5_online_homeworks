


const button = document.querySelector("button");
const select = document.querySelector("#country");
const ul = document.querySelector("ul");

button.addEventListener("click", () => {
    const request = new XMLHttpRequest();
    request.open('GET', `http://universities.hipolabs.com/search?country=${select.value}`);
    request.responseType = "json";
    request.send();
    request.onload = () => {
        if (request.status === 200) {
            ul.innerHTML = "Loading...";
            setTimeout(() => {ul.innerHTML = request.response.map(({ web_pages }) => `<li>${web_pages}</li>`).join("")},3000);
        }
    }
});
