# Поиск университетов?

По клику на кнопку считывать страну которая выбрана в селекте и отправлять `GET` запрос 
на `http://universities.hipolabs.com/search?country=${country}`.

Отрисовывать в `ul` ссылки на сайт каждого университета.

Пока идет загрузка данных - показывайте прелоадер ( можно просто слово Loading... )
