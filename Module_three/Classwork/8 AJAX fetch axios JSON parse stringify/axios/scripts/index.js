

const url =  "https://ajax.test-danit.com/api/swapi/vehicles";
const container = document.querySelector(".container");


axios.get(url).then(({ data }) => {
        console.log(data);
        data.forEach(({ model, name, films, id }) => {
            container.insertAdjacentHTML("beforeend",
                `<div class="card">
                        <p>Name: ${name}</p>
                        <p>Model: ${model}</p>
                        <ul id="vehicles-${id}"></ul>
                        </div>`);

            films.forEach(filmsUrl => {
                axios.get(filmsUrl).then(({ status, data: { name } }) => {
                    if (status === 200) {
                            document.querySelector(`#vehicles-${id}`)
                                    .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
                    }
                });
            });
        });
});