

const url = 'https://ajax.test-danit.com/api/json/photos';

fetch(url).then(response => {
    if (response.ok) {
        return response.json();
    }else {
        console.error("Bad response!");
    }
}).then((data) => {
    console.log(data);
    data.forEach(({ albumId, title, url }) => {
        const container = document.querySelector(`#container_${albumId}`);
        if(container){
            container.insertAdjacentHTML("beforeend",
            `<div class="card">
                     <img src="${url}" alt="img">
                     <span>${title}</span>
                 </div>`)}});
});

