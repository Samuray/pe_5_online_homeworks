
    const movie = "https://ajax.test-danit.com/api/swapi/films";
    const container = document.querySelector(".container");

    axios.get(movie).then(({ status, data }) => {
        if (status === 200) {
            data.forEach(({ episodeId, name, openingCrawl, characters, id }) => {
                container.insertAdjacentHTML("beforeend",
                    `<div class="card">
                        <p>Name: ${name}</p>
                         <div class="preloader">
                               <p class="preloader__p">Loading...</p>
                         </div>
                        <ul id="films-${id}"></ul>
                        <p>Episod id: ${episodeId}</p>
                        <p>Opening crawl: ${openingCrawl}</p>
                        </div>`);

                characters.forEach(charactersUrl => {
                    axios.get(charactersUrl).then(({ status, data: { name } }) => {
                        if (status === 200) {
                            setTimeout(() => {
                            document.querySelector(`#films-${id}`)
                                .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
                                document.querySelectorAll(".preloader").forEach(elem => elem.style.display = "none");
                            }, 5000);
                        }
                    }).catch(error => console.error(error));
                });
            });
        }
    }).catch(error => console.error(error));
