"use strict";

    class Employee {
        constructor(name, age, salary) {
            this._name = name;
            this._age = age;
            this._salary = salary;
        }

        get name() {
            return this._name;
        }
        get age() {
            return this._age;
        }
        get salary() {
            return this._salary;
        }
        set name(value) {
            this._name = value;
        }
        set age(value) {
            this._age = value;
        }
        set salary(value) {
            this._salary = value;
        }

    logInfo () {
        console.log(`Name: ${this.name}; Age: ${this.age}; Salary: ${this.salary}`)
            };
        }

    const human = new Employee("Boris",30,5000);
    human.logInfo();


    class  Programmer extends Employee {
        constructor(name, age, salary, lang) {
            super(name, age, salary);
            this.lang = lang;
        }

        get salary() {
           return this._salary;
        }
        set salary(value) {
            this._salary = value * 3;
        }

        logInfo() {
            super.logInfo();
            console.log(`Lang: ${this.lang}`);
        }
    }
    const humanOne = new Programmer("Volodya", 40, 15000, "ukr");
    const humanTwo = new Programmer("Karl", 35, 20000, "eng");
    const humanThree = new Programmer("Grigoriy", 27, 10000, "polski");
    humanOne.logInfo();
    humanTwo.logInfo();
    humanThree.logInfo();




