


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];



let root = document.getElementById("root");
let ul = document.createElement("ul");
root.append(ul);

books.forEach(elem => {
    try {
        if (elem.author !== undefined && elem.name !== undefined && elem.price !== undefined){
            ul.insertAdjacentHTML('beforeend', `
    <li>
         <span>Author:${elem.author} </span>        
         <span>Name:${elem.name}</span>
         <span>Price:${elem.price}</span>
    </li> `);
        } else {
            throw elem;
        }
    } catch (error) {
        console.error(error);
    }
});




