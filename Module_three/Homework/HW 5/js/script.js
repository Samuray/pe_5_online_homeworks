
document.addEventListener('DOMContentLoaded', async function () {
    const { data } = await axios.get('https://ajax.test-danit.com/api/json/users');
    const userList = data.map(item => new User(item));

    const postsData = await axios.get('https://ajax.test-danit.com/api/json/posts');
    const postList = postsData.data.map(item => new Post({
        ...item,
        user: userList.find(user => user.getUserID() === item.userId)
    }));

    postList.map(item => new PostCard(item.user, item.title, item.body, item.id).render());
});

class User {
    #id;
    constructor({ id, name, email }) {
        this.#id = id;
        this.name = name;
        this.email = email;
    }

    getUserID() {
        return this.#id;
    }
}

class Post {
    constructor({ id, user, title, body }) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.body = body;
    }
}

class PostCard {
    constructor(user, title, body, id) {
        this.user = user
        this.title = title
        this.body = body
        this.id = id
        this.container = document.createElement('div');
        this.buttonElem = document.createElement('button');
        this.section = document.querySelector(".widgets");
    }

    render() {
        this.container.innerHTML = ` <img class="widgets__container-img" src="././img/15.jpg" alt="img"/>
                            <img class="widgets__container-icon" src="././img/Twitter.png" alt="icon"/>
                            <span class="widgets__container-title">${this.user.name},${this.user.email}</span>
                           <span class="widgets__container-text">${this.title}</span>

                                <div class="widgets__content">
                                    <img class="widgets__content-foto" src="././img/gt-r-special-edition.jpg" alt="image"/>
                                   <span class="widgets__content-text">${this.body}</span>
                                </div>

                           <div class="widgets__section">
                               <img src="././img/otvet.png" alt="icon"/>
                               <p>10 тыс</p>
                                <img src="././img/arrow_arrows.png" alt="icon"/>
                                <p>15 тыс</p>
                               <img src="././img/serdce.png" alt="icon"/>
                               <p>150 тыс</p>
                               <img src="././img/loading.png" alt="icon"/>
                           </div>  `;
        this.container.className = "widgets-container";
        this.container.appendChild(this.buttonElem);

        this.buttonElem.className = 'btn';
        this.buttonElem.innerHTML = 'DELETE';
        this.buttonElem.addEventListener('click', async () => {
            const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`,{
                method: "DELETE",
                  "Content -Type": "application / json" ,
                "Access-Control-Allow-Credentials" : true,
                "Access-Control-Allow-Origin" : "*" ,

            }).then(res => console.log(res.text()));
            if (result === "") {
                this.container.remove();
            }
        });
        this.section.appendChild(this.container);
    }
}









