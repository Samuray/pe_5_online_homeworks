const dogs = [
    {
        name: 'Bob',
        breed: 'hound',
        url: 'https://dog.ceo/api/breed/hound/images/random'
    },{
        name: 'Honey',
        breed: 'husky',
        url: 'https://dog.ceo/api/breed/husky/images/random'
    },{
        name: 'John',
        breed: 'ovcharka',
        url: 'https://dog.ceo/api/breed/ovcharka/images/random'
    },{
        name: 'Sweaty',
        breed: 'germanshepherd',
        url: 'https://dog.ceo/api/breed/germanshepherd/images/random'
    },{
        name: 'Sam',
        breed: 'germanshepherd',
        url: 'https://dog.ceo/api/breed/germanshepherd/images/random'
    },{
        name: 'Conor',
        breed: 'corgi',
        url: 'https://dog.ceo/api/breed/hound/images/random'
    },{
        name: 'Ivan',
        breed: 'ovcharka',
        url: 'https://dog.ceo/api/breed/ovcharka/images/random'
    },{
        name: 'Kate',
        breed: 'whippet',
        url: 'https://dog.ceo/api/breed/whippet/images/random'
    },{
        name: 'Adam',
        breed: 'husky',
        url: 'https://do.ceo/api/breed/husky/images/random'
    },
];

    const promises = dogs.map(({ url }) => fetch(url).then(response => response.json()));
    console.log(promises)

    Promise.all(promises).then(value => {
    const container = document.querySelector(".container");
    value.forEach(({message}, index) => {
        container.insertAdjacentHTML("beforeend",  ` <div class ="card">
                                                                 <img src="${message}" alt="">
                                                                <p> Name: ${dogs[index].name} </p>
                                                                <p> Breed: ${dogs[index].breed} </p>
                                                                </div>`);
    });
    });


    // Promise.allSettled(promises).then(value => {
    //     console.log(value)
    //     const container = document.querySelector('.container');
    //     value.forEach(({ status, value }, index) => {
    //         if (status === 'fulfilled') {
    //             container.insertAdjacentHTML('beforeend',  `<div class="card">
    //                 <img src= ${value?.message} alt="">
    //                 <p>Name: ${dogs[index].name}</p>
    //                 <p>Breed: ${dogs[index].breed}</p>
    //             </div>`)
    //         }
    //     })
    // });