class Card {
    /**
     * @param editFunction {function}
     * @param removeFunction {function}
     */
    constructor(editFunction, removeFunction) {
        this.editFunction = editFunction;
        this.removeFunction = removeFunction;
        this.container = document.createElement("div");
        this.contentContainer = document.createElement("div");
        this.editButton = document.createElement("button");
        this.removeButton = document.createElement("button");
    }

    createElements () {
        this.contentContainer.classList.add('card__content-container');

        this.editButton.classList.add('card__edit');
        this.editButton.classList.add('card__btn');
        this.editButton.addEventListener('click',this.editFunction.bind(this));

        this.removeButton.classList.add('card__delete');
        this.removeButton.classList.add('card__btn');
        this.removeButton.addEventListener('click',this.removeFunction.bind(this));

        this.container.append(this.editButton);
        this.container.append(this.removeButton);
        this.container.append(this.contentContainer);
        this.container.classList.add('card');
    }

    /**'
     * @param selector {string}
     */
    render(selector = "body") {
        this.createElements();
        document.querySelector(selector).append(this.container);
    }
}

export class ArticleCard extends Card{
    /**
     * @param editFunction
     * @param removeFunction
     * @param title {string}
     * @param text {string}
     */
    constructor(editFunction, removeFunction, title, text) {
        super(editFunction, removeFunction);
        this.title = title;
        this.text = text;

        this.titleContainer = document.createElement('h3');
        this.textContainer = document.createElement('p');
    }

    createElements () {
        super.createElements();
        this.titleContainer.innerText = this.title;
        this.textContainer.innerText = this.text;

        this.contentContainer.append(this.titleContainer);
        this.contentContainer.append(this.textContainer);
    }
}


