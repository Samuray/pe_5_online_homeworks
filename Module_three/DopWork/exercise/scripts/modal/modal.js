class Modal {
    constructor() {
        this.container = document.createElement("div");
        this.background = document.createElement("div");
        this.mainContainer = document.createElement("div");
        this.closeButton = document.createElement("button");
        this.contentWrapper = document.createElement("div");
        this.buttonWrapper = document.createElement("div");
    }
    createElements() {
        this.container.classList.add('modal');
        this.container.append(this.mainContainer);
        this.container.append(this.background);

        this.background.classList.add('modal__background');

        this.closeButton.classList.add('modal__close');

        this.contentWrapper.classList.add('modal__content-wrapper');

        this.buttonWrapper.classList.add('modal__button-wrapper');

        this.mainContainer.classList.add('modal__main-container');
        this.mainContainer.append(this.closeButton);
        this.mainContainer.append(this.contentWrapper);
        this.mainContainer.append(this.buttonWrapper);

        this.closeButton.addEventListener('click', this.closeMe.bind(this));
        this.background.addEventListener('click', this.closeMe.bind(this));
    }

    closeMe() {
        this.container.remove();
    }

    render(selector = 'body') {
        this.createElements();
        document.querySelector(selector).append(this.container);
    }
}

export class DeleteModal extends Modal {
    constructor(titlePost, deleteFunction) {
        super();
        this.titlePost = titlePost;
        this.deleteFunction = deleteFunction;
        this.titleElement = document.createElement('h3');
        this.buttonYes = document.createElement('button');
        this.buttonNo = document.createElement('button');
    }
    createElements() {
        super.createElements();

        this.titleElement.innerHTML = `Do you really want to delete "${this.titlePost}"?`;

        this.buttonYes.innerHTML = 'Yes';
        this.buttonYes.classList.add('modal__confirm-btn');
        this.buttonYes.addEventListener('click', this.onConfirm.bind(this));

        this.buttonNo.innerHTML = 'No';
        this.buttonNo.classList.add('modal__cancel-btn');
        this.buttonNo.addEventListener('click', this.closeMe.bind(this));

        this.contentWrapper.append(this.titleElement);
        this.buttonWrapper.append(this.buttonYes);
        this.buttonWrapper.append(this.buttonNo);
    }
    onConfirm(){
        this.deleteFunction();
        this.closeMe();
    }
}

export class EditModal extends Modal{
    constructor(title, text, editFunction) {
        super();

        this.title = title;
        this.text = text;
        this.editFunction = editFunction;

        this.form = document.createElement('form');
        this.input = document.createElement('input');
        this.textarea = document.createElement('textarea');
        this.buttonYes = document.createElement('button');
    }

    createElements(){
        super.createElements();

        this.input.value = this.title;
        this.textarea.value = this.text;

        const inputLabel = document.createElement('label');
        inputLabel.innerText = 'Title';

        const textAreaLabel = document.createElement('label');
        textAreaLabel.innerText = 'Post';

        this.form.append(inputLabel);
        this.form.append(this.input);
        this.form.append(textAreaLabel);
        this.form.append(this.textarea);

        this.contentWrapper.append(this.form);

        this.buttonYes.innerHTML = 'Yes';
        this.buttonYes.classList.add('modal__confirm-btn');
        this.buttonYes.addEventListener('click', this.onConfirm.bind(this));

        this.buttonWrapper.append(this.buttonYes);
    }

    onConfirm(){
        console.log(this.input.value)
        console.log(this.textarea.value)
        this.editFunction(this.input.value, this.textarea.value);
        this.closeMe();
    }
}
