
const sum = require("./script");
const sum2 = require("./script");

// запуск теста - npm run test;

    //тест
    test("testing sum fn", () => {
        expect(sum(2, 3)).toBe(5);
    });
    // toEqual - проверка на сложные значения
    test("testing sum2 fn", () => {
        expect(sum2([1, 2, 3, 4])).toEqual([1, 2, 3, 4]);
    });

    // обьединяет несколько тестов.
    describe("testing one & two fn", () => {
    // тесты
        expect(sum(3, 3)).toBe(6);
        expect(sum(4, 3)).toBe(7);
    });

    // TDD - разработка на основе теста
    // BDD - разработка на основе поведения