import React from "react";
import style from "./CartList.module.scss";
import { Button } from "../Button/Button";
import CardCart from "../CardCart/CardCart";
import Modal from "../Modal/Modal";
import { useDispatch, useSelector } from "react-redux";

import {
  idModalProduct,
  toggleShowModal,
} from "../../Redux/modal/action-creators";
import {
  addInCart,
  removeCart,
  removeOneCart,
} from "../../Redux/product/action-creators";
import {FormCart} from "../FormCart/FormCart";

const CartList = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.product.product);
  const isModal = useSelector((state) => state.modal.isModal);
  const modalProduct = useSelector((state) => state.modal.idModalProduct);
  const cartList = products.filter((e) => e.cart.inCart === "true");

  const modalActive = (id) => {
    dispatch(idModalProduct(id));
    dispatch(toggleShowModal());
  };

  const total = () => {
    let count = 0;
    cartList.forEach((e) => {
      count += e.price * e.cart.count;
    });
    return count;
  };

  return (
    <div className={`${style.wrapperCart} container`}>
      <ul className="collection with-header">
        <li className="collection-header">
          <h4>Your purchases</h4>
        </li>
        {products
          .filter((e) => e.cart.inCart === "true")
          .map((product) => {
            return (
              <CardCart
                key={product.id}
                product={product}
                removeOneCart={(id) => dispatch(removeOneCart(id))}
                addToCart={(id) => dispatch(addInCart(id))}
                modal={modalActive}
              />
            );
          })}
        <li className={`${style.footer} collection-header`}>
          <h4
            className={`${style.totalValue}`}
          >{`TOTAL: ${`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(
            total()
          )}`}`}</h4>
        </li>
        <li className={`${style.form} collection-header`}>
          <FormCart />
        </li>
      </ul>
      {isModal && (
        <Modal
          isOpen={modalActive}
          header={"Removing an item from the cart?"}
          textmodal={"Are you sure you want to remove an item from your cart?"}
          actions={
            <div className={style.modalFooter}>
              <Button
                size={"l"}
                click={() => {
                  dispatch(removeCart(modalProduct));
                  modalActive();
                }}
                color={"green accent-3"}
                text={"OK"}
              />
              <Button
                size={"l"}
                click={modalActive}
                color={"green accent-3"}
                text={"Cancel"}
              />
            </div>
          }
        />
      )}
    </div>
  );
};
export default CartList;
