
 export const toggleFavorite = (state, payload) => {
  const cartUpdate = state.map((e) =>
    e.id === payload ? { ...e, isFavourite: !e.isFavourite } : e
  );
  const isFavorite = cartUpdate.filter((e) => e.isFavourite === true);
  setLocalProduct(cartUpdate);
  setLocalFavorites(isFavorite);
  return cartUpdate;
};

export const addToCart = (state, payload) => {
  const cartUpdate = state.map((e) =>
    e.id === payload
      ? { ...e, cart: { inCart: "true", count: e.cart.count + 1 } }
      : e
  );
  const toCart = cartUpdate.filter((e) => e.cart.inCart === "true");
  setLocalProduct(cartUpdate);
  setLocalCart(toCart);
  return cartUpdate;
};

export const removeOneCart = (state, payload) => {
  const cartUpdate = state.map((e) =>
    e.id === payload
      ? { ...e, cart: { inCart: "true", count: e.cart.count - 1 } }
      : e
  );
  const removeOneCart = cartUpdate.filter((e) => e.cart.inCart === "true");
  setLocalProduct(cartUpdate);
  setLocalCart(removeOneCart);

  return cartUpdate;
};

export const removeProductCart = (state, payload) => {
  const cartUpdate = state.map((e) =>
    e.id === payload ? { ...e, cart: { inCart: false, count: 0 } } : e
  );
  const removeProductCart = cartUpdate.filter((e) => e.cart.inCart === "true");
  setLocalProduct(cartUpdate);
  setLocalCart(removeProductCart);
  return cartUpdate;
};
export const clearCart = (state, payload) => {
  const cartUpdate = state.map((e) =>
    e ? { ...e, cart: { inCart: "false", count: 0 } } : null
  );
  const clearCart = cartUpdate.filter((e) => e.cart.inCart === "true");
  setLocalProduct(cartUpdate);
  setLocalCart(clearCart);
  return cartUpdate;
};

const setLocalProduct = (product) =>
  localStorage.setItem("products", JSON.stringify(product));
const setLocalFavorites = (product) =>
  localStorage.setItem("favorites", JSON.stringify(product));
const setLocalCart = (product) =>
  localStorage.setItem("cart", JSON.stringify(product));
