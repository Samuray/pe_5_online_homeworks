import { createStore, applyMiddleware, combineReducers } from "redux";
import { modalReducer } from "./modal/modalReducer";
import { productsReducer } from "./product/productsReducer";
import { cartReducer } from "./cart/cartReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  product: productsReducer,
  modal: modalReducer,
  cart: cartReducer,
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
