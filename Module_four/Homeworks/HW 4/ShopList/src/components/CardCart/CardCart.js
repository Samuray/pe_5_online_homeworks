import React from "react";
import PropTypes from "prop-types";
import style from "./CardCart.module.scss";

 const CardCart = ({ product, removeOneCart, addToCart, modal }) => {
  const { imgUrl, productName, vendorСod, color, price, cart, id } = product;
  return (
    <li className="collection-item">
      <div>
        <div className={`${style.cartToCard}`}>
          <img className={`${style.img}`} src={imgUrl} alt={productName} />
          <div className={"info"}>
            <h5 className={`${style.productName} grey-text text-darken-4`}>
              {productName}
            </h5>
            <div className={`${style.code} grey-text text-darken-4`}>
              {`Code: ${vendorСod}`}
            </div>
            <div className={`${style.color} grey-text text-darken-4`}>
              {" "}
              {`Color: ${color}`}
            </div>
            <div className={`${style.price} grey-text text-darken-4`}>
              {" "}
              {`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price)}`}
            </div>
          </div>
          <div className={`${style.calc}`}>
            <span onClick={() => addToCart(id)} className="btn-flat">
              <i className={`${style.increment} Tiny material-icons`}>add</i>
            </span>

            <div className={`${style.value}`}>{cart.count}</div>

            <span
              onClick={() => removeOneCart(id)}
              className={`${
                cart.count === 1 ? "disabled" : "enabled"
              } btn-flat`}
            >
              <i className={`${style.decrement} Tiny material-icons`}>remove</i>
            </span>
          </div>

          <div className={`${style.total}`}>{`\u20B4 ${new Intl.NumberFormat(
            "ua-Ua"
          ).format(price * cart.count)}`}</div>
          <i
            onClick={() => modal(id)}
            className={`${style.remove} material-icons`}
          >
            remove_shopping_cart
          </i>
        </div>
      </div>
    </li>
  );
}

CardCart.propTypes = {
  product: PropTypes.object,
  removeFullCart: PropTypes.func,
  removeOneCart: PropTypes.func,
  addToCart: PropTypes.func,
};

export default CardCart;
