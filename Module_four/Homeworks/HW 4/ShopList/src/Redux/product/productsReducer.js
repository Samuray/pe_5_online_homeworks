import {
  LOADING_CARDS,
  REMOVE_PRODUCT_CART,
  LOADING_ERROR,
  ADD_TO_CART,
  REMOVE_ONE_CART,
  FETCH_CARTS_SUCCESS,
  TOGGLE_FAVORITE,
} from "./actions";
import {
  addToCart,
  removeOneCart,
  removeProductCart,
  toggleFavorite,
} from "./operations";

const initialState = {
  isLoading: false,
  isError: null,
  product: JSON.parse(localStorage.getItem("products")) || [],
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_CARDS:
      return { ...state, isLoading: true, isError: null };
    case LOADING_ERROR:
      return { ...state, isError: action.payload, isLoading: false };
    case FETCH_CARTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: null,
        product: [...state.product, ...action.payload],
      };
    case TOGGLE_FAVORITE:
      const favoritesUpdate = toggleFavorite(state.product, action.payload);
      return {
        ...state,
        product: [...favoritesUpdate],
      };
    case ADD_TO_CART:
      const addToCartUpdate = addToCart(state.product, action.payload);
      return {
        ...state,
        product: [...addToCartUpdate],
      };
    case REMOVE_ONE_CART:
      const removeOneCartUpdate = removeOneCart(state.product, action.payload);
      return {
        ...state,
        product: [...removeOneCartUpdate],
      };
    case REMOVE_PRODUCT_CART:
      const removeProductCartUpdate = removeProductCart(state.product, action.payload );
      return {
        ...state,
        product: [...removeProductCartUpdate],
      };
    default:
      return state;
  }
};
