import React, { useEffect, useState } from "react";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import styles from  './App.module.scss';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import CartList from "./components/CartList/CardList";
import List from "./components/List/List";
import Empty from "./components/Empty/Empty";
import Preloader from "./components/Preloader/Preloader";

const App = () =>  {

  const [cards, setCards] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(null);
  const [carts, setCarts] = useState(JSON.parse(localStorage.getItem("carts")) || []);
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem("favorites")) || []);
  const [products, setProducts] = useState(JSON.parse(localStorage.getItem("products")) || []);

  useEffect(() => {
      setIsLoading(true);
    (async () => {
      await fetch("product.json")
          .then((res) => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error("Error to loads!");
            }
          })
          .then((card) => {
            setCards(   card );
            setIsLoading( false );
          })
          .catch((error) => {
              setIsError(error.message);
              setIsLoading(false);
          });
    })();
  },[]);

   const addToCart = (id) => {
       const cartUpdate = products.map(elem => elem.id === id ? {...elem, cart:{inCart: "true", count: elem.cart.count+1 }} : elem)
       setProducts(cartUpdate)
       localStorage.setItem("products", JSON.stringify(cartUpdate));
       const isCart = cartUpdate.filter(elem => elem.cart.inCart === "true")
       setCarts(isCart)
       localStorage.setItem("carts", JSON.stringify(isCart));
    };

    const removeOneCart=(id)=> {
        const cartUpdate = products.map(elem => elem.id === id ? {...elem, cart:{inCart: "true", count: elem.cart.count-1 }} : elem)
        setProducts(cartUpdate)
        localStorage.setItem("products", JSON.stringify(cartUpdate));
        const isCart = cartUpdate.filter(elem => elem.cart.inCart === "true")
        setCarts(isCart)
        localStorage.setItem("carts", JSON.stringify(isCart));
    }

    const removeFullCart=(id)=> {
        const cartUpdate = products.map(elem => elem.id === id ? {...elem, cart:{inCart: false ,count: 0 }} : elem)
        setProducts(cartUpdate)
        localStorage.setItem("products", JSON.stringify(cartUpdate));
        const isCart = cartUpdate.filter(elem => elem.cart.inCart === "true")
        setCarts(isCart)
        localStorage.setItem("carts", JSON.stringify(isCart));
    }

    const isFavorite = (id) => {
        const cartUpdate = products.map(elem => elem.id === id ? {...elem, isFavourite : !elem.isFavourite} : elem)
        setProducts(cartUpdate)
        localStorage.setItem("products", JSON.stringify(cartUpdate));
        const isFavorite = cartUpdate.filter(elem => elem.isFavourite === true)
        setFavorites(isFavorite)
        localStorage.setItem("favorites", JSON.stringify(isFavorite));
    };

    const renderContainer = () => {
        if (isError) {
            return <div>{isError}</div>;
        }
        if (isLoading) {
            return <Preloader />;
        }
        if (cards.length === 0) {
            return <div>{"Product list empty!!!"}</div>;
        }
        if (cards) {
            if (!products.length){
                setProducts(cards)
            }
            return <List
                productList={products}
                addToCart={addToCart}
                isFavorite={isFavorite}
            />;
        }
        return null;
    };


    return(
        <Router>
            <div className={styles.App}>
                <header>
                    <Navbar carts={carts} favorites={favorites}/>
                </header>
                <main className={styles.main}>
                    <Switch>
                        <Route exact path="/">
                            {renderContainer()}
                        </Route>
                        <Route path="/cart">
                            {!carts.length ?
                                <Empty text={'Your basket is empty'}/>
                                :
                                <CartList
                                    cartList={carts}
                                    addCart={addToCart}
                                    removeOneCart={removeOneCart}
                                    removeFullCart={removeFullCart}
                                />
                            }
                        </Route>
                        <Route path="/favorites">
                            {!favorites.length ?
                                <Empty text={'Your favorites list is empty'}/>
                                :
                                <List
                                    productList={favorites}
                                    addToCart={addToCart}
                                    isFavorite={isFavorite}
                                />}
                            ;
                        </Route>
                    </Switch>
                </main>
                <footer>
                    <Footer />
                </footer>
            </div>
        </Router>

    )
}

export default App;
