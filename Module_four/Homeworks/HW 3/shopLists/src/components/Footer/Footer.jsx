import React from "react";

 const Footer  = () => {

    return (
      <footer className="page-footer green">
        <div className="container">
          <div className="row">
            <div className="col l5 s12 hide-on-small-only">
              <h5 className="yellow-text">Shop of Phones</h5>
              <p className="yellow-text text-lighten-1">Electronics store</p>
            </div>
            <div className="col l4 s12 hide-on-small-only">
              <h5 className="yellow-text">New phones Apple</h5>
              <ul>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    iPhone 14 Pro Max
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    iPhone 14 Pro
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    iPhone 14
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    iPhone 14 Mini
                  </a>
                </li>
              </ul>
            </div>
            <div className="col l3 s12">
              <h5 className="yellow-text">For clients</h5>
              <ul>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    Payment
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    Delivery
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    Public offer
                  </a>
                </li>
                <li>
                  <a className="yellow-text text-lighten-3" href="/">
                    Guarantee
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright hide-on-small-only">
          <div className="container center yellow-text">© 2022 Shop of Phones</div>
        </div>
      </footer>
    );
}

export default Footer;
