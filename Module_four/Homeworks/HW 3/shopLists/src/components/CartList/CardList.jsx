import React, { useState } from "react";
import styles from "./CardList.module.scss";
import Empty from "../Empty/Empty";
import  Button  from "../Button/Button";
import CardCart from "../CardCart/CardCart";
import Modal from "../Modal/Modal";


 const CardList = ({ cartList, addCart, removeOneCart, removeFullCart }) => {
     const [modal, setModal] = useState(false);
     const [cod, setCod] = useState(0);

     const modalActive = (id) => {
         setCod(id)
         setModal(!modal)
     };

     const total = () => {
         let count = 0;
         cartList.forEach(e => {
             count += e.price * e.cart.count
         })
         return count
     }

    return (
        <div className={`${styles.wrapperCart} container`}>
            <ul className="collection with-header">

                <li className="collection-header"><h4>Your purchases</h4></li>
                {cartList.map(product => {
                    return (
                        <CardCart
                            key={product.id}
                            product={product}
                            removeOneCart={removeOneCart}
                            addToCart={addCart}
                            modal={modalActive}
                        />
                    )
                })}
                <li className={`${styles.footer} collection-header`}>
                    <Button text={'checkout'} size={styles.checkout} color={'green accent-3'}/>
                    <Empty/>
                    <h4 className={`${styles.totalValue}`}>{`TOTAL:${`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(total())}`}`}</h4>
                </li>
            </ul>
            {modal && (
                <Modal
                    isOpen={modalActive}
                    header={"Removing an item from the cart?"}
                    textmodal={"Are you sure you want to remove an item from your cart?"}
                    actions={
                        <div className={styles.modalFooter}>
                            <Button
                                size={"l"}
                                click={() => {
                                    removeFullCart(cod);
                                    modalActive();
                                }}
                                color={"green accent-3"}
                                text={"OK"}
                            />
                            <Button
                                size={"l"}
                                click={modalActive}
                                color={"green accent-3"}
                                text={"Cancel"}
                            />
                        </div>
                    }
                />
            )}
        </div>
    );
}


 export default CardList;