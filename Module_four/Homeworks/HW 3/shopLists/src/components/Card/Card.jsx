import React from "react";
import PropTypes from "prop-types";
import styles from "./Card.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarAdd } from "../../assets/svg/star-check-outline.svg";
import { ReactComponent as StarRemove} from "../../assets/svg/star-check.svg";


const Card = ({ product, favorite, isFavorite, modal }) => {

    const { imgUrl, alt, productName, vendorСode, color, price, id } = product;

    return (
        <div className={`${styles.card} card hoverAble`}>
          <div className="card-image waves-effect waves-block waves-light">
            <h6 className={`${styles.code} grey-text text-darken-4 right`}>
              {`code: ${ vendorСode }`}
            </h6>
            <img className={`${styles.img} activator`} src={ imgUrl } alt={alt} />
          </div>
          <div className={`${styles.card_content} card-content`}>
            <span
              className={`${styles.card_title} card-title activator grey-text text-darken-4`}
            >
              <h6 className={styles.cardTitle}>{productName}</h6>
            </span>

            <h6 className={styles.cardColor}>
              {`Color: ${color}`}
            </h6>
            <h5 className={`${styles.price} grey-text text-darken-4`}>
              {`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price)}
              `}
            </h5>
            <p className={styles.footer}>
              <Button
                  click={()=>modal(id)}
                color={"green accent-3"}
                text={"Add to cart"}
              />
                <span className={styles.favorite}
                      onClick={() => isFavorite(id)}>
             {favorite ? <i className={`${styles.favorite__icon} material-icons`}>
                     <StarRemove />
                 </i> :
                 <i className={`${styles.favorite__icon_sel} material-icons`}>
                     <StarAdd />
                 </i>}
           </span>
            </p>
          </div>
          <div className="card-reveal">
            <span className="card-title grey-text text-darken-4">
              { productName }
              <i className="material-icons right">close</i>
            </span>
            <p>
              iPhone 14 works with existing power adapters, EarPods with
              Lightning Connector, and USB‑A to Lightning cables. Because there
              are billions of those out in the world, new ones often go unused.
              So we’re removing them from the box — across the entire iPhone
              family. This reduces carbon emissions and avoids the mining and
              use of precious materials. It also shrinks the package, allowing
              more boxes per shipment and fewer shipments overall.
            </p>
          </div>
        </div>
    );
}

Card.propTypes = {
          favorite: PropTypes.func,
          isFavorite: PropTypes.func,
          modal: PropTypes.func,
};

export default Card;


