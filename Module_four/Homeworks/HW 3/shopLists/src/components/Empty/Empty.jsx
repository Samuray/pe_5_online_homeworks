import styles from "./Empty.module.scss";
import { NavLink } from "react-router-dom";
import React from "react";

const Empty = ({text}) => {
    return (<div className={`${styles.emptyWrapper} `}>
            <div className={`${styles.empty} center`}>{text}</div>
            <NavLink to='/'  className={`${styles.toHome} center-align`}>
                <i className="material-icons "> </i>
                Back to home page
                <i className="material-icons">done</i>
            </NavLink>
        </div>
    )
}
export default Empty;
