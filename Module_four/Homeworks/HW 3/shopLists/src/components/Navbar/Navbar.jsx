import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Navbar.module.scss";

// const Navbar = (cards, favorites) => {
//
//     return (
//         <nav>
//             <div className="nav-wrapper  green">
//                 <div className="container">
//                     <a
//                         href="#!"
//                         className="brand-logo"
//                         style={{ color: "gold", fontSize: 40, fontWeight: "bold" }}
//                     >
//                         Shop of Phones
//                     </a>
//                     <ul className="right hide-on-med-and-down">
//                         <li>
//                             <a href="#!" className="yellow-text">
//                                 <i className="material-icons left"> </i>
//                                 Home
//                             </a>
//                         </li>
//                         <li>
//                             <a href="#!" className="yellow-text">
//                                 <i className="material-icons left"> </i>
//                                 Favorites: {favorites.length}
//                             </a>
//                         </li>
//                         <li>
//                             <a href="#!" className="yellow-text">
//                                 <i className="material-icons left "> </i>
//                                 Cart: {cards.length}
//                             </a>
//                         </li>
//                     </ul>
//                 </div>
//             </div>
//         </nav>
//     )
// }
//
// export  default  Navbar;

const Navbar = () => {

    return (
        <nav>
            <div className="nav-wrapper  green darken-3">
                <div className="container">
                    <NavLink to='/'>
                        <div
                            className="brand-logo"
                            style={{ color: "gold", fontSize: 40, fontWeight: "bold" }}
                        >
                            <i
                                className="material-icons"
                                style={{ color: "green", fontSize: 45 }}
                            >

                            </i>
                            Shop of Phones
                        </div>
                    </NavLink>
                    <ul className="right hide-on-med-and-down">
                        <li>
                            <NavLink exact to='/' activeClassName={styles.selected} className="yellow-text">
                                <i className="material-icons left"> </i>
                                Home
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to='/favorites' activeClassName={styles.selected} className="yellow-text">
                                <i className="material-icons left"> </i>
                                Favorites
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to='/cart' activeClassName={styles.selected} className="yellow-text">
                                <i className="material-icons left"> </i>
                                Cart
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;