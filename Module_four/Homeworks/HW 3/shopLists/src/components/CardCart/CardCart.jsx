import React from "react";
import styles from "./CardCart.module.scss";
import PropTypes from "prop-types";

 const CardCart = ({ product, removeOneCart, addToCart, modal }) => {

     const { imgUrl, productName, vendorСode, color, price, cart, id } = product;

    return (
        <li
            className="collection-item">
            <div>
                <div className={`${styles.cartToCard}`}>
                    <img className={`${styles.img}`} src={imgUrl} alt={ productName }/>
                    <div className={'info'}>
                        <h5 className={`${styles.productName} grey-text text-darken-4`}>{ productName }</h5>
                        <div className={`${styles.code} grey-text text-darken-4`}> {`Code: ${ vendorСode }`}</div>
                        <div className={`${styles.color} grey-text text-darken-4`}> {`Color: ${ color }`}</div>
                        <div className={`${styles.price} grey-text text-darken-4`}> {`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price)}`}</div>
                    </div>
                    <div className={`${styles.calc}`}>
                       <span
                           onClick={() => addToCart(id)}
                           className="btn-flat"><i
                           className={`${styles.increment} Tiny material-icons`}>add</i>
                       </span>

                        <div className={`${styles.value}`}>{cart.count}</div>

                        <span
                            onClick={() => removeOneCart(id)}
                            className={`${cart.count === 1 ? "disabled" : null} btn-flat`}><i
                            className={`${styles.decrement} Tiny material-icons`}>remove</i>
                         </span>
                    </div>

                    <div className={`${styles.total}`}>{`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price * cart.count)}`}</div>
                    <i
                        onClick={()=>modal(id)}
                        className={`${styles.remove} material-icons`}>DELETE
                    </i>

                </div>
            </div>
        </li>

    );

}

CardCart.propTypes = {
    product:PropTypes.object,
    removeFullCart:PropTypes.func,
    removeOneCart: PropTypes.func,
    addToCart: PropTypes.func,
};


 export default CardCart;