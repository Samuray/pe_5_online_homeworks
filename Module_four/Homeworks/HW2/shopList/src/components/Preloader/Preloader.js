import { Component } from "react";
import styles from "./Preloader.module.scss";

 class Preloader extends Component {
  render() {
    return (
      <div className={styles.preloader}>
        <div className={styles.loader}> </div>
      </div>
    );
  }
}

export default Preloader;
