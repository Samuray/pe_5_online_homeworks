import { Component } from "react";
import PropTypes from "prop-types";

 class Button extends Component {
  render() {
      const { click, size, color, icon, text } = this.props;
    return (
      <button
        onClick={click}
        className={`${size} ${color} waves-effect btn`}
      >
        {icon}
        {text}
      </button>
    );
  }
}
Button.propTypes = {
  click: PropTypes.func,
  size: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.object,
  text: PropTypes.string,
};
Button.defaultProps = {
  color: "green",
  icon: null,
};

export default Button;
