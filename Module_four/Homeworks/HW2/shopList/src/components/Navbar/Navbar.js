import { Component } from "react";

 class Navbar extends Component {

  render() {
    const { cards, favorites} = this.props;
    return (
      <nav>
        <div className="nav-wrapper  green">
          <div className="container">
            <a
              href="#!"
              className="brand-logo"
              style={{ color: "gold", fontSize: 40, fontWeight: "bold" }}
            >
              Shop of Phones
            </a>
            <ul className="right hide-on-med-and-down">
              <li>
                <a href="#!" className="yellow-text">
                  <i className="material-icons left"> </i>
                  Home
                </a>
              </li>
              <li>
                <a href="#!" className="yellow-text">
                  <i className="material-icons left"> </i>
                  Favorites: {favorites.length}
                </a>
              </li>
              <li>
                <a href="#!" className="yellow-text">
                  <i className="material-icons left "> </i>
                  Cart: {cards.length}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;



