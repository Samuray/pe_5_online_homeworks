import { Component } from "react";
import styles from "./App.module.scss";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import CardList from "./components/CardList/CardList";
import Preloader from "./components/Preloader/Preloader";

class App extends Component {
  state = {
        cards: [],
        isLoading: false,
        isError: null,
        carts: JSON.parse(localStorage.getItem("carts")) || [],
        favorites: JSON.parse(localStorage.getItem("favorites")) || [],
  };

  componentDidMount() {

    this.setState({ isLoading: true });
    fetch("product.json")
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else {
            throw new Error("Error to loads!");
          }
        })
        .then((card) => {
          this.setState({ cards: card, isLoading: false });
        })
        .catch((error) => {
          this.setState({ isError: error.message, isLoading: false });
        });
  }

    addToCart = (code) => {
        let cards = [...this.state.carts, code];
        this.setState({ carts: cards });
        localStorage.setItem("carts", JSON.stringify(cards));
    };

    selectFavorite = (isFavorites) => {
        this.setState({ favorites: isFavorites });
        localStorage.setItem("favorites", JSON.stringify(isFavorites));
    };

  render() {
    const { carts, isLoading, isError, favorites, cards } = this.state;
    return (
        <div className={styles.App}>
          <header>
            <Navbar cards={carts} favorites={favorites} />
          </header>
          <main className={styles.main}>
            {isLoading && <Preloader />}
            {isError && <div>{isError}</div>}
            {cards && !cards.length ? (
                <div>{"Product list empty!!!"}</div>
            ) : (
                <CardList productList={cards} favorites={favorites} addToCart={this.addToCart} selectFavorite={this.selectFavorite} />
            )}
          </main>
          <footer>
            <Footer />
          </footer>
        </div>
    );
  }
}

export default App;
