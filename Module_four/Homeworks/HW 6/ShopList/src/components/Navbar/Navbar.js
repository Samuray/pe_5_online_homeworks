import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import style from "./Navbar.module.scss";
import { Button } from "../Button/Button";
import ListContext from "../../context/ListContext";

export const Navbar = () => {

  let { setIsTabView } = useContext(ListContext);
  const handleClick = () => {
    setIsTabView(prev => !prev)
  }

  return (
    <nav>
      <div className="nav-wrapper  green darken-3">
        <div className="container">
          <NavLink to="/">
            <div
              className="brand-logo"
              style={{ color: "gold", fontSize: 40, fontWeight: "bold" }}
            >
              Shop of Phones
            </div>
          </NavLink>
          <ul className="right hide-on-med-and-down ">
            <Button
                click={() => handleClick()}
                size={"l"}
                color={"yellow accent-6 black-text"}
                text={"List"}
            />
            <li>
              <NavLink to="/favorites" activeClassName={style.selected}>
                <i className="material-icons left yellow-text">favorite_border</i>
                Favorites
              </NavLink>
            </li>
            <li>
              <NavLink to="/cart" activeClassName={style.selected}>
                <i className="material-icons left yellow-text">shopping_cart</i>
                Cart
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};
