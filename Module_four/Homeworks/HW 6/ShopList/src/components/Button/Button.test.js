import { Button } from "./Button";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Test for button", () => {
  test("Should smoke component footer", () => {
    render(<Button />);
  });
  test("Should show text for button", () => {
    const text = "testText";
    const { getByText } = render(<Button text={text} />);
    getByText(text);
  });
  test("Should show icon for button", () => {
    const icon = '<i className="material-icons left">shopping_cart</i>';
    const { getByText } = render(<Button text={icon} />);
    getByText(icon);
  });
  test("Should show color for button", () => {
    const color = "red";
    const { getByTestId } = render(<Button color={color} />);
    expect(getByTestId("button")).toHaveClass(color);
  });
  test("Should test working function onclick", () => {
    const onClickFn = jest.fn();
    const { getByTestId } = render(<Button click={onClickFn} />);
    userEvent.click(getByTestId("button"));
    expect(onClickFn).toHaveBeenCalled();
  });
  test("Should snapshot test for button", () => {
    const { asFragment } = render(<Button />);
    expect(asFragment()).toMatchSnapshot();
  });
});
