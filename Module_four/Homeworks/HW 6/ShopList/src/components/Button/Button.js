import React from "react";
import PropTypes from "prop-types";

export const Button = ({ click, color, icon, text }) => {
  return (
    <button
        onClick={click}
        className={`${color} waves-effect btn`}
        data-testid="button"
    >
      {icon}
      {text}
    </button>
  );
};
Button.propTypes = {
  click: PropTypes.func,
  size: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.object,
  text: PropTypes.string,
};
Button.defaultProps = {
  icon: null,
  click: null,
  size: null,
};
