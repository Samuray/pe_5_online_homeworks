import React from "react";
import { PatternFormat } from "react-number-format";
import styles from "./formCart.module.scss";
import { Form, Formik, useField } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { cartCheckout } from "../../Redux/cart/action-creators";
import { clearCart } from "../../Redux/product/action-creators";

export const FormCart = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.product.product).filter(
    (elem) => elem.cart.inCart === "true"
  );

  return (
    <>
      <h4>Order form</h4>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          email: "",
          phone: "",
          address: "",
        }}
        validationSchema={validationFormsSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          dispatch(clearCart());
          dispatch(cartCheckout({ customer: values, product: cart }));
        }}
      >
        <Form>
          <div className="row">
            <div className={"col s4"}>
              <TextInput label="First Name" name="firstName" type="text" />
            </div>
            <div className={"col s4"}>
              <TextInput label="Last name" name="lastName" type="text" />
            </div>
            <div className={"col s4"}>
              <TextInput label="Age" name="age" type="number" />
            </div>
          </div>

          <div className="row">
            <div className={"col s6"}>
              <TextInput label="Email Address" name="email" type="email" />
            </div>
            <div className={"col s6"}>
              <PatternFormat className={styles.tel} format="+38 (###) ### ## ##" label="Telephone" name="phone" type="tel" placeholder="Telephone"/>
            </div>
          </div>
          <div className="row">
            <div className={"col s12"}>
              <TextareaInput label="Delivery address" name="address" />
            </div>
          </div>

          <div className="row center-align">
            <button
              className="btn waves-effect waves-light green accent-3 btn-large "
              type="submit"
              name="action"
            >
              Checkout
              <i className="material-icons right">shopping_cart</i>
            </button>
          </div>
        </Form>
      </Formik>
    </>
  );
};

const TextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <div className="input-field">
        <label htmlFor={props.id || props.name}>{label}</label>
        <input
          className={meta.touched && meta.error ? "invalid" : "validate"}
          {...field}
          {...props}
        />
        <span
          className="helper-text"
          data-error={meta.error}
          data-success="right"
        />
      </div>
    </>
  );
};

const TextareaInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <div className="input-field">
        <label htmlFor={props.id || props.name}>{label}</label>
        <textarea
          className={`materialize-textarea ${
            meta.touched && meta.error ? "invalid" : "validate"
          }`}
          {...field}
          {...props}
        />
        <span
          className="helper-text"
          data-error={meta.error}
          data-success="right"
        />
      </div>
    </>
  );
};

const phoneRegExp =
  /[+3]|^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const validationFormsSchema = Yup.object().shape({
  firstName: Yup.string()
    .typeError("Should be a string")
    .max(24, "Must be 24 characters or less")
    .required("Required"),
  lastName: Yup.string()
    .typeError("Should be a string")
    .max(24, "Must be 24 characters or less")
    .required("Required"),
  age: Yup.number()
    .typeError("Should be a number")
    .required("Required")
    .max(90, "some error, maximum age 90")
    .min(18, "some error, minimum age 18"),
  email: Yup.string().email("Invalid email address").required("Required"),
  phone: Yup.string()
    .required("Required")
    .matches(phoneRegExp, "Phone number is not valid")
    .min(13, "to short")
    .max(15, "to long"),
  address: Yup.string()
    .typeError("Should be a string")
    .min(8, "Must be 8 characters or less")
    .max(120, "Must be 120 characters or less")
    .required("Required"),
});
