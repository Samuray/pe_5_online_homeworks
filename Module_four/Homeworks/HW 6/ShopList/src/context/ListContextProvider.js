import React from "react";
import  ListContext  from "./ListContext";

const ListContextProvider = (props) => {
    const  { value, children } = props;

    return (
        <ListContext.Provider value={value}>
            {children}
        </ListContext.Provider>
    )
}

export  default ListContextProvider;