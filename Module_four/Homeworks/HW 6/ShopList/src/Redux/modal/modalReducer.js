import { MODAL_SHOW, MODAL_ID_PRODUCT } from "./actions";

const initialState = {
  isModal: false,
  idModalProduct: null,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case MODAL_SHOW:
      return { ...state, isModal: !state.isModal };
    case MODAL_ID_PRODUCT:
      return { ...state, idModalProduct: action.payload };
    default:
      return state;
  }
};
