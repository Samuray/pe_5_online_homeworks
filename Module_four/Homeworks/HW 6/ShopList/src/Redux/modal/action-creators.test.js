import { MODAL_SHOW, MODAL_ID_PRODUCT } from "./actions";
import { idModalProduct, toggleShowModal } from "./action-creators";

describe("Test for action-creator modalReducer", () => {
  it("toggleShowModal", () => {
    expect(toggleShowModal()).toEqual({
      type: MODAL_SHOW,
    });
  });
  it("idModalProduct", () => {
    expect(idModalProduct(1)).toEqual({
      type: MODAL_ID_PRODUCT,
      payload: 1,
    });
  });
});
