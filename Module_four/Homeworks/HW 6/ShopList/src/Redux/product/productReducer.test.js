import { productsReducer } from "./productsReducer";
import {
    LOADING_CARDS,
    REMOVE_PRODUCT_CART,
    LOADING_ERROR,
    ADD_TO_CART,
    REMOVE_ONE_CART,
    FETCH_CARTS_SUCCESS,
    TOGGLE_FAVORITE,
    CLEAR_CART,
} from "./actions";


describe("Test for productsReducer", () => {
    test("LOADING_CARDS", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: JSON.parse(localStorage.getItem("products")) || [],
        };
        const action = {
            type: LOADING_CARDS,
        };
        const reducer = productsReducer(stateBefore, action);
        expect(reducer.isLoading).toBeTruthy;
    });
    test("LOADING_ERROR", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: JSON.parse(localStorage.getItem("products")) || [],
        };
        const action = {
            type: LOADING_ERROR,
            payload: "Error message",
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            isError: action.payload,
            isLoading: false,
        });
    });
    test("FETCH_CARTS_SUCCESS", () => {
        const stateBefore = {
            isLoading: true,
            isError: null,
            product: JSON.parse(localStorage.getItem("products")) || [],
        };
        const action = {
            type: FETCH_CARTS_SUCCESS,
            payload: [{ product: "product" }],
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            isError: null,
            isLoading: false,
            product: [...stateBefore.product, ...action.payload],
        });
    });
    test("TOGGLE_FAVORITE", () => {
        const stateBefore = {
            product: [
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/6/764f8071b21fa3d7b7430ba518665d4d.jpg",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        };
        const action = {
            type: TOGGLE_FAVORITE,
            payload: 1,
        };
        const reducer = productsReducer(stateBefore, action);
        expect(reducer.product[0].isFavourite).toBeTruthy();
    });
    test("ADD_TO_CART", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: [
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        };
        const action = {
            type: ADD_TO_CART,
            payload: 1,
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            product: [
                {
                    cart: { inCart: "true", count: 1 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        });
    });
    test("REMOVE_ONE_CART", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: [
                {
                    cart: { inCart: "true", count: 5 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        };
        const action = {
            type: REMOVE_ONE_CART,
            payload: 1,
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            product: [
                {
                    cart: { inCart: "true", count: 4 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        });
    });
    test("REMOVE_PRODUCT_CART", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: [
                {
                    cart: { inCart: "true", count: 5 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        };
        const action = {
            type: REMOVE_PRODUCT_CART,
            payload: 1,
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            product: [
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: false, count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        });
    });
    test("CLEAR_CART", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: [
                {
                    cart: { inCart: "true", count: 5 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: "true", count: 4 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        };
        const action = {
            type: CLEAR_CART,
        };
        expect(productsReducer(stateBefore, action)).toEqual({
            ...stateBefore,
            product: [
                {
                    cart: { inCart: "false", count: 0 },
                    color: "Deep Purple",
                    id: 1,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
                {
                    cart: { inCart: "false", count: 0 },
                    color: "Deep Purple",
                    id: 2,
                    imgUrl:
                        "https://www.istore.ua/upload/iblock/470/z6h2othyor9pdqseuydm53v2hbzt44sk/14_pro_max_gpurpl_3_is.png",
                    isFavourite: false,
                    price: 59999,
                    productName: "Apple iPhone 14 Pro 256GB ",
                    vendorСod: "001",
                },
            ],
        });
    });
    test("undefined", () => {
        const stateBefore = {
            isLoading: false,
            isError: null,
            product: ["test"],
        };
        expect(productsReducer(stateBefore, {})).toEqual(stateBefore);
    });
});