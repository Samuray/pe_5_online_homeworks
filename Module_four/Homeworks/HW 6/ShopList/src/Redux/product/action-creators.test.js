import {
    LOADING_CARDS,
    REMOVE_PRODUCT_CART,
    LOADING_ERROR,
    ADD_TO_CART,
    REMOVE_ONE_CART,
    FETCH_CARTS_SUCCESS,
    TOGGLE_FAVORITE,
    CLEAR_CART,
} from "./actions";
import {
    isLoading,
    isError,
    fetchCarts,
    favorite,
    addInCart,
    removeOneCart,
    removeCart,
    clearCart,
} from "./action-creators";


describe("Test for action-creator productsReducer", () => {
    test("isLoading", () => {
        expect(isLoading()).toEqual({
            type: LOADING_CARDS,
        });
    });
    test("isError", () => {
        expect(isError("message")).toEqual({
            type:LOADING_ERROR,
            payload: "message",
        });
    });
    test("fetchCarts", () => {
        expect(fetchCarts(["fetchCarts"])).toEqual({
            type:FETCH_CARTS_SUCCESS,
            payload: ["fetchCarts"],
        });
    });
    test("favorite", () => {
        expect(favorite(1)).toEqual({
            type:TOGGLE_FAVORITE,
            payload: 1,
        });
    });
    test("addInCart", () => {
        expect(addInCart(1)).toEqual({
            type:ADD_TO_CART,
            payload: 1,
        });
    });
    test("removeOneCart", () => {
        expect(removeOneCart(1)).toEqual({
            type:REMOVE_ONE_CART,
            payload: 1,
        });
    });
    test("removeCart", () => {
        expect(removeCart(1)).toEqual({
            type:REMOVE_PRODUCT_CART,
            payload: 1,
        });
    });
    test("clearCart", () => {
        expect(clearCart(1)).toEqual({
            type:CLEAR_CART,
            payload: 1,
        });
    });
});