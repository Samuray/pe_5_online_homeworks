import { CHECKOUT } from "./actions";

const initialState = {
  checkout: [],
};

export const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHECKOUT:
      const checkout = [...state.checkout, payload];
      console.log(checkout)
      return { ...state, checkout: checkout };

    default:
      return state;
  }
};
