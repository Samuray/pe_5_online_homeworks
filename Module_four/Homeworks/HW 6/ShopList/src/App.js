import React, {useEffect, useState} from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import style from "./App.module.scss";
import tabView from "./styles/List.module.scss";
import listView from "./styles/Lists.module.scss";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import { Navbar } from "./components/Navbar/Navbar";
import { Footer } from "./components/Footer/Footer";
import List from "./components/List/List";
import { Preloader } from "./components/Preloader/Preloader";
import CartList from "./components/CartList/CartList";
import Title from "./components/Title/Title";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import ListContextProvider from "./context/ListContextProvider";

import {
  fetchCarts,
  isError,
  isLoading,
} from "./Redux/product/action-creators";

export default function App() {

  const [isTabView, setIsTabView] = useState(true);
  const viewStiles = isTabView ? tabView : listView;
  const contextValue = {
    viewStiles,
    setIsTabView,
    isTabView
  }

  const dispatch = useDispatch();
  const loading = useSelector((state) => state.product.isLoading);
  const error = useSelector((state) => state.product.isError);
  const products = useSelector((state) => state.product.product);
  const cart = products.filter((e) => e.cart.inCart === "true");
  const favorites = products.filter((e) => e.isFavourite === true);

  const LoadingCarts = () => {
    return async (dispatch) => {
      try {
        dispatch(isLoading());
        const response = await axios("http://localhost:8080/products");
        dispatch(fetchCarts(response.data));
      } catch (e) {
        dispatch(isError(e));
      }
    };
  };

  useEffect(() => {
    if (products.length) {
      return localStorage.setItem("products", JSON.stringify(products));
    }
    dispatch(LoadingCarts());
  }, [dispatch, products]);



  return (
      <ListContextProvider value={contextValue}>
            <Router>
              <div className={style.App}>
                <header>
                  <Navbar />
                </header>
                <main className={style.main}>
                  {error && <Title text={error} />}
                  {loading && <Preloader />}
                  {!error && !loading && (
                    <Switch>
                      <Route exact path="/">
                        {!products.length ? (
                          <Title text={"Product list empty!!!"} />
                        ) : (
                          <List productList={products} />
                        )}
                        ;
                      </Route>
                      <Route path="/cart">
                        {!cart.length ? (
                          <Title text={"Your basket is empty"} />
                        ) : (
                          <CartList />
                        )}
                      </Route>
                      <Route path="/favorites">
                        {!favorites.length ? (
                          <Title text={"Your favorites list is empty"} />
                        ) : (
                          <List productList={favorites} />
                        )}

                      </Route>
                    </Switch>
                  )}
                </main>
                <footer>
                  <Footer />
                </footer>
              </div>
            </Router>
      </ListContextProvider>
  );
}
