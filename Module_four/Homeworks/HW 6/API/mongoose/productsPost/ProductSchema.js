import mongoose from "mongoose";

const ProductSchema = new mongoose.Schema({
    productName: String,
    price: String,
    imgUrl:String,
    vendorСod: String,
    color: String,
    isFavorite: Boolean,
    cart: {
        count: String,
        inCart: Boolean,
    }
});

export default ProductSchema;