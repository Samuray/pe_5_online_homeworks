import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();
import routes from "./routes/index.js";
import cors from "cors";
import bodyParser from "body-parser";

const app = express();
const PORT = 8080;


app.use(cors())
app.use(bodyParser.json())

mongoose.connect(process.env.DB_CONNECT, () => {
    console.log('-----------------------------------------------------');
    console.log('DB Connected');
    console.log('-----------------------------------------------------');

    routes(app);

    app.listen(PORT, () => {
        console.log(`Server opened at port ${PORT}`)
    })
});
