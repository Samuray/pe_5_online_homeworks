import ProductModel from "../../mongoose/productsPost/ProductModel.js";

const getProduct = (app) => {
    app.get("/products", (req, res) => {
        // const data = await ProductModel.find();
        // res.send(data)
        ProductModel.find((err, data) => {
            if (err) {
                res.status(500).send(err);
            }

            res.status(200).send(data);
        })
    })
};

export default getProduct;