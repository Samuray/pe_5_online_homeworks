import getProduct from "./products/getProduct.js";
import addProduct from "./products/addProduct.js";
import deleteProduct from "./products/deleteProduct.js";
import editProduct from "./products/editProduct.js";

const routes = app => {
    getProduct(app);
    addProduct(app);
    deleteProduct(app);
    editProduct(app);
};

export default routes;