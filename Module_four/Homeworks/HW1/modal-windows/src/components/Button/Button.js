import { Component } from 'react';
import styles from "./Button.module.scss";


class Button extends Component {
    render() {
        const { onClick, backgroundColor, text } = this.props;
        return (
            <button className={styles.borderButton}
                    onClick={onClick}
                     style={{backgroundColor: backgroundColor ? backgroundColor : "rgba(0, 0, 0, 0.5)"}} >
                {text}
            </button>
        )
    }
}

export  default Button;
