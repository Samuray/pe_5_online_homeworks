import { Component } from 'react';
import styles from './Modal.module.scss'

 class Modal extends Component {
    render() {
        const { isOpen, header, closeButton, textModal, actions } = this.props;
        return (
            <div onClick={isOpen}
                className={styles.modal}>
                <div onClick={event => event.stopPropagation()}
                     className={styles.modalContent}>
                    <div className={styles.modalHeader}>
                        <h5>{header}</h5>
                        {closeButton && <span onClick={isOpen}>&#10761;</span>}
                    </div>
                    <div className={styles.modalBody}>
                        <p className={styles.bodyText}>{textModal}</p>
                    </div>
                    {actions}
                </div>
            </div>
        )
    }
}

export default Modal;







