import { Component } from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import styles from './App.module.scss';

class App extends Component {
    state = {
        firstModalActive: false,
        secondModalActive: false
    }

    firstModal = () => {
        this.setState({
            firstModalActive: !this.state.firstModalActive
        })
    }

    secondModal = () => {
        this.setState({
            secondModalActive: !this.state.secondModalActive
        })
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.buttonGroup}>
                    <Button
                        onClick={this.firstModal}
                        backgroundColor={"#eb4034"}
                        text={"Open first modal"}
                    />

                    <Button
                        onClick={this.secondModal}
                        backgroundColor={"#21aeeb"}
                        text={"Open second modal"}
                    />
                </div>

                {this.state.firstModalActive &&
                    <Modal
                        header={"Do you want to delete this file?"}
                        closeButton={false}
                        isOpen={this.firstModal}
                        textModal={"Once you delete this file, it won't be possible to undo this action.  Are you sure you want to delete it?"}
                        actions={
                            <div className={styles.modalFooter}>
                                <Button text={"Ok"}/>
                                <Button text={"Cancel"} onClick={this.firstModal}/>
                            </div>}
                    />}
                {this.state.secondModalActive &&
                    <Modal
                        header={'Well, is it interesting for you here?'}
                        closeButton={true}
                        isOpen={this.secondModal}
                        textModal={"You can click as much as you like with these buttons. This is just a test!"}
                        actions={
                            <div className={styles.modalFooter}>
                                <Button backgroundColor={"#021f2b"} text={"Yes"}/>
                                <Button backgroundColor={"#021f2b"} text={"No"} onClick={this.secondModal}/>
                            </div>}
                    />}
            </div>
        );
    }
}

export default App;
