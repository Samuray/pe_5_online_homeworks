import express from "express";
import mongoose from "mongoose";
// import bodyParser from "body-parser";
// import cors from "cors";
import dotenv from "dotenv";
dotenv.config();

const app = express();

const PORT = 8080;

const ProductSchema = new mongoose.Schema({
    name: String,
    price: {
        type: String,
        required: true,
    }
});

const Product = mongoose.model("Products", ProductSchema);

mongoose.connect(process.env.DB_CONNECT, () => {
    console.log("----------------------------------------------");
    console.log("DB Connected");
    console.log("----------------------------------------------");

    app.get("/",async (req, res) => {
      //  // искать все ____________________________________________________
      //   Product.find((err, data) => {
      //       res.send(data);
      //   });
        const data = await Product.find()

        // // искать по id и one _____________________________________________
        // try{
        //     // const data = await Product.findById("63dabaaff9d55c5e9de53951");
        //     const data = await Product.findOne({price: "250"});
        //     res.json(data);
        // } catch (err){
        //     res.send(err.message);
        // }
       // // добавление элемента ______________________________________________
       //  try{
       //      const newProduct = new Product({ name: "lemon", price: "200" });
       //      const data = await newProduct.save();
       //      res.json(data);
       //  } catch (err){
       //      res.send(err.message);
       //  }

        // //редактирование и удаление  ____________________________________________________
        //
        // try{
        //     const data = await Product.findByIdAndUpdate("63dabaaff9d55c5e9de53951", ({ price:"150" }));
        //     const data = await Product.findByIdAndDelete("63dabaaff9d55c5e9de53951");
        //     res.json(data);
        // } catch (err){
        //     res.send(err.message);
        // }

    });

    app.listen(PORT, () => {
        console.log(`Server running to Port: ${PORT}`)
    });
});