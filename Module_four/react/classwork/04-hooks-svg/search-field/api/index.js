const express = require('express')
const colors = require('colors')
const cors = require('cors')
const posts = require('./scripts/data')
const app = express()
app.use(cors())

app.get('/posts', (_, response) => {
	const body = {
		data: [...posts],
		status: 'success',
	}

	setTimeout(() => {
		response.status(200).send(JSON.stringify(body))
	}, 2000)
})

app.get('/search', (request, response) => {
	const { value } = request.query
	console.log(value)

	if (!value) {
		setTimeout(() => {
			response.status(200).send(JSON.stringify({
				data: [...posts],
				status: 'success',
			}))
		}, 1000);
	} else {
		const valueRegExp = new RegExp(value, 'gi')
		const filteredData = [...posts].filter(({ title, body }) => valueRegExp.test(title) || valueRegExp.test(body))

		setTimeout(() => {
			response.status(200).send(JSON.stringify({
				data: [...filteredData],
				status: 'success',
			}))
		}, 1000)
	}
})

app.listen(3001, () => {
	console.log('================================================'.white)
	console.log('Host is listening on PORT: 3000'.white)
	console.log('http://localhost:3001/'.cyan)
	console.log('================================================'.white)
})
