## Installations
`yarn` or `npm i`

## Scripts
`yarn start` or `npm run start` - start express server on 3000 port;

[http://localhost:3000/](http://localhost:3000/)

## Descriptions

### `GET /posts`

Return all posts

#### Response:
```js
  {
    status: 'success',
    data: [
              {
                body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
                id: 1,
                title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                userId: 1,
              },
              ...
	]  
  }
```


### `GET /search?value=someValue`

Return filtered posts

#### Response:
```js
  {
    status: 'success',
    data: [
              {
                body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
                id: 1,
                title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                userId: 1,
              },
              ...
	]  
  }
```

