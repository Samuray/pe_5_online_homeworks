import './App.css';
import { Component } from 'react'

class App extends Component {
  render(){
    return (
      <div className="App">
        <div className="wrapper">
          <label htmlFor="search">Search</label>
          <input type="text" name="search" id="search" placeholder="search" />
        </div>

        <div className="post-container">
          <div className="card">
            <p>12</p>
            <h3>Title</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem cumque dolor dolorem ducimus fugit id ipsum laborum nam ullam voluptatibus.</p>
          </div>

          <div className="card">
            <p>13</p>
            <h3>Title</h3>
            <p>Lorem ipsum dolor sit amet, consectetur.</p>
          </div>

          <div className="card">
            <p>14</p>
            <h3>Title</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores explicabo officia provident.</p>
          </div>
        </div>
      </div>
    );
  }

}

export default App;
