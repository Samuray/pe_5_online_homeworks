import React from 'react';
import ReactDOM from 'react-dom/client';
import App_for_student from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App_for_student />
);