import { useState, useEffect } from 'react';
import styles from './HeaderUser.module.scss';
import Preloader from '../Preloader/';

const HeaderUser = () => {
    const [userName, setUserName] = useState("");
    const [userImg, setUserImg] = useState("");
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        (async () => {
            const { data: { name, avatar } } = await fetch('http://localhost:3001/user').then(response => response.json())
            setUserName(name)
            setUserImg(avatar)
            setIsLoading(false)
        })()
    }, []);

    return (
        <div className={styles.container}>
            {isLoading && <Preloader color='#fff' size={30} />}


            {!isLoading && <> <img
                src={userImg}
                alt="John Smith"
                width={30}
                height={30}
            />
                <span>{userName}</span> </>}
        </div>
    );
}

// class HeaderUser extends PureComponent {
//     state = {
//         userName: '',
//         userImg: '',
//         isLoading: true
//     }
//
//
//     async componentDidMount() {
//         const { data: { name, avatar } } = await fetch('http://localhost:3001/user').then(response => response.json())
//
//         this.setState({ userName: name, userImg: avatar, isLoading: false })
//     }
//
//
//     render() {
//         const { userName, userImg, isLoading } = this.state
//         return (
//             <div className={styles.container}>
//                 {isLoading && <Preloader color='#fff' size={30} />}
//
//
//                 {!isLoading && <> <img
//                     src={userImg}
//                     alt="John Smith"
//                     width={30}
//                     height={30}
//                 />
//                     <span>{userName}</span> </>}
//             </div>
//         );
//     }
// }
//
export default HeaderUser;