import React from 'react';
import CardItem from '../CardItem';
import styles from './CardContainer.module.scss';

const CardContainer = ({ addToCart, cards }) => {

	return (
		<div>
			<ul className={styles.list}>
				{cards.map(({title, img, description, isFavorite, id}) => (
					<li key={id}>
						<CardItem
							addToCart={addToCart}
							id={id}
							title={title}
							description={description}
							img={img}
							isFavorite={isFavorite}
						/>
					</li>
				))}
			</ul>
		</div>
	);
}

// class CardContainer extends PureComponent {
// 	render() {
// 		const { addToCart, cards} = this.props;
// 		console.log(cards);
// 		return (
// 			<div>
// 				<ul className={styles.list}>
// 					 {cards.map(({title, img, description, isFavorite, id}) => (
// 						<li key={id}>
// 							<CardItem
// 							addToCart={addToCart}
// 								id={id}
// 								title={title}
// 								description={description}
// 								img={img}
// 								isFavorite={isFavorite}
// 								/>
// 						</li>
// 					 ))}
// 				</ul>
// 			</div>
// 		);
// 	}
// }

export default CardContainer;
