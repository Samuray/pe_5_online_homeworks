import React from 'react';
import CartItem from '../CartItem/'

const CartContainer  = ({ carts, incrementCartItem, dicrementCartItem, toggleModal, setModalProps }) => {
    console.log(setModalProps)

    return(
                <ul>
        {carts.map(({title, img, count, id }) => {
        return <CartItem setModalProps={setModalProps} key={id} toggleModal={toggleModal} title={title} img={img} count={count} id={id} incrementCartItem={incrementCartItem} dicrementCartItem={dicrementCartItem} />
        })}
                </ul>
            )

}

export default CartContainer;