import React from 'react';
import styles from './Header.module.scss';
import HeaderUser from '../HeaderUser/';
import cartIcon from '../../svg/cart-outline.svg'

const Header = () =>  {
        return (
            <header className={styles.root}>
                <div>
              <HeaderUser />

                </div>

                {/* Centered navigation */}
                <nav>
                    <ul>
                        <li>
                            <a href="">Link</a>
                        </li>
                        <li>
                            <a href="">Link</a>
                        </li>
                    </ul>
                </nav>

                {/* Right container */}
                <ul>
                    <li>
                        <a href="#"><img src={cartIcon} alt="Cart" /></a>
                    </li>
                </ul>
            </header>
        );
}
   
export default Header;
