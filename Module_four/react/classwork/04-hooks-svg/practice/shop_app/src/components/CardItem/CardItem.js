import React from 'react';
import styles from './CardItem.module.scss';
import Button from '../Button';
import heartSvg from '../../svg/heart.svg';
import outlineHeartSvg from '../../svg/heart-outline.svg'



const CardItem = ({ addToCart, title, img, description, isFavorite, id } ) => {

        return (
            <div className={styles.card}>
                <button type="button" className={styles.likeButton}>
                    <img src={isFavorite ? heartSvg :outlineHeartSvg} alt="Favourite" />
                </button>
                <span className={styles.title}>{title}</span>
                <img className={styles.itemAvatar} src={img}
                     alt={title}/>
                <span className={styles.description}>{description}</span>

                <div className={styles.btnContainer}>
                    <Button onClick={() => {
                        addToCart({title, img, id})
                    }} >В корзину</Button>
                </div>
            </div>
        )
}

// class CardItem extends PureComponent {
//     render() {
//         const { addToCart, title, img, description, isFavorite, id} = this.props;
//         return (
//             <div className={styles.card}>
//                 <button type="button" className={styles.likeButton}>
//                     <img src={isFavorite ? heartSvg :outlineHeartSvg} alt="Favourite" />
//                 </button>
//                 <span className={styles.title}>{title}</span>
//                 <img className={styles.itemAvatar} src={img}
//                      alt={title}/>
//                 <span className={styles.description}>{description}</span>
//
//                 <div className={styles.btnContainer}>
//                     <Button onClick={() => {
//                         addToCart({title, img, id})
//                     }} >В корзину</Button>
//                 </div>
//             </div>
//         )
//     }
// }

export default CardItem;