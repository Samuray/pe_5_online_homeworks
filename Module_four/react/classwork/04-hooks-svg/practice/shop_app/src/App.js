import { useEffect, useState } from "react";
// import { Component } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import Preloader from './components/Preloader';
import Modal from './components/Modal';
import CardContainer from "./components/CardContainer/CardContainer";
import CartContainer from "./components/CartContainer/CartContainer";

const App = () =>  {
    const [cards, setCards] = useState([]);
    const [isCardsLoading, setIsCardsLoading] = useState(true);
    const [carts, setCarts] = useState([]);
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [modalProps, setModalProps] = useState({});

    const  addToCart = (card) => {
                const index = carts.findIndex(el => el.id === card.id)

                if (index === -1) {
                    carts.push({ ...card, count: 1 })
                } else {
                    carts[index].count += 1
                }

                localStorage.setItem("carts", JSON.stringify(carts));
                setCarts(carts);
    }

   const incrementCartItem = (id) => {

            const index = carts.findIndex(el => el.id === id)
            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
                setCarts(carts);
    }

   const dicrementCartItem = (id) => {

            const index = carts.findIndex(el => el.id === id)
            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
             setCarts(carts);
    }

    const  deleteCartItem = (id) => {
            const index = carts.findIndex(el => el.id === id)
            if (index !== -1) {
                carts.splice(index, 1);
            }

            localStorage.setItem("carts", JSON.stringify(carts))
             setCarts(carts)
    }

   const toggleModal = (value) => {
        setIsOpenModal(  value )
    }

    const setModalPropsVal = (value) => {
        setModalProps( value )
    }
    useEffect(() => {
        (async () => {
            const { data } = await fetch('http://localhost:3001/items').then(res => res.json());
            const carts = localStorage.getItem('carts')
            if(carts){
                setCarts( JSON.parse(carts))
            }
            setIsCardsLoading(false);
            setCards(data);
        })()
    }, [carts]);


        return (
            <div className={styles.App}>
                <Header />
                <main>
                    <section className={styles.leftContainer}>
                        <h1>Каталог</h1>
                        {isCardsLoading && <Preloader size={90} />}
                        {!isCardsLoading && <CardContainer addToCart={addToCart} cards={cards} />}
                    </section>

                    <section className={styles.rightContainer}>
                        <h2>Корзина</h2>
                        <CartContainer setModalProps={setModalPropsVal} toggleModal={toggleModal} incrementCartItem={incrementCartItem} dicrementCartItem={dicrementCartItem} carts={carts} />


                    </section>
                </main>
                <Modal modalProps={modalProps} deleteCartItem={deleteCartItem} isOpen={isOpenModal} toggleModal={toggleModal} />
            </div>
        );
}
//
// class App extends Component {
//     state = {
//         cards: [],
//         isCardsLoading: true,
//         carts: [],
//         isOpenModal: false,
//         modalProps: {}
//     }
//
//     addToCart = (card) => {
//         this.setState((current) => {
//             const carts = [...current.carts]
//
//             const index = carts.findIndex(el => el.id === card.id)
//
//             if (index === -1) {
//                 carts.push({ ...card, count: 1 })
//             } else {
//                 carts[index].count += 1
//             }
//
//             localStorage.setItem("carts", JSON.stringify(carts))
//             return { carts }
//         })
//     }
//
//     incrementCartItem = (id) => {
//         this.setState((current) => {
//             const carts = [...current.carts]
//
//             const index = carts.findIndex(el => el.id === id)
//
//             if (index !== -1) {
//                 carts[index].count += 1
//             }
//
//             localStorage.setItem("carts", JSON.stringify(carts))
//             return { carts }
//         })
//     }
//
//     dicrementCartItem = (id) => {
//         this.setState((current) => {
//             const carts = [...current.carts]
//
//             const index = carts.findIndex(el => el.id === id)
//
//             if (index !== -1 && carts[index].count > 1) {
//                 carts[index].count -= 1
//             }
//
//             localStorage.setItem("carts", JSON.stringify(carts))
//             return { carts }
//         })
//     }
//
//     deleteCartItem = (id) => {
//         this.setState((current) => {
//             const carts = [...current.carts]
//
//             const index = carts.findIndex(el => el.id === id)
//
//             if (index !== -1) {
//                 carts.splice(index, 1);
//             }
//
//             localStorage.setItem("carts", JSON.stringify(carts))
//             return { carts }
//         })
//     }
//
//     toggleModal = (value) => {
//         this.setState({ isOpenModal: value })
//     }
//
//     setModalProps = (value) => {
//         this.setState({ modalProps: value })
//     }
//
//     async componentDidMount() {
//         const { data } = await fetch('http://localhost:3001/items').then(res => res.json());
//
//         const carts = localStorage.getItem('carts')
//
//         if(carts){
//             this.setState({carts: JSON.parse(carts)})
//         }
//
//
//         this.setState({ cards: data, isCardsLoading: false });
//     }
//
//
//
//     render() {
//         const { isCardsLoading, cards, carts, isOpenModal, modalProps } = this.state;
//
//         // if(isCardsLoading) {
//         //     return <h1>Loading.....</h1>
//         // }
//
//         return (
//             <div className={styles.App}>
//                 <Header />
//                 <main>
//                     <section className={styles.leftContainer}>
//                         <h1>Каталог</h1>
//                         {isCardsLoading && <Preloader size={90} />}
//                         {!isCardsLoading && <CardContainer addToCart={this.addToCart} cards={cards} />}
//                     </section>
//
//                     <section className={styles.rightContainer}>
//                         <h2>Корзина</h2>
//                         <CartContainer setModalProps={this.setModalProps} toggleModal={this.toggleModal} incrementCartItem={this.incrementCartItem} dicrementCartItem={this.dicrementCartItem} carts={carts} />
//
//
//                     </section>
//                 </main>
//                 <Modal modalProps={modalProps} deleteCartItem={this.deleteCartItem} isOpen={isOpenModal} toggleModal={this.toggleModal} />
//             </div>
//         );
//     }
// }



export default App;
