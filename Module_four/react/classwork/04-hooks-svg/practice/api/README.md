# TODO API

#### URL: http://localhost:3001/

<hr>

## GET /user

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
            name: '',
            avatar: ''
        },
}
```
<hr>

## GET /items

#### Responses:

status: 200
```js
{
    status: 'success',
    data: [
        {
            title: '',
            img: '',
            description: '',
            isFavorite: false,
            id: '',
        }
    ],
}
```
<hr>


## PATCH /toggle-favorite

#### Body:
```js
{
    id: '',
}
```

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
        title: '',
        img: '',
        description: '',
        isFavorite: true,
        id: '',
    }
}
```
<hr>