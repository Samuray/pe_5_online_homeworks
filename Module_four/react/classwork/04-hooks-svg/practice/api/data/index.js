const {v4: getId} = require('uuid');

module.exports = {
    user: {
        name: 'Jake the dog',
        avatar: 'http://localhost:3001/Jake_the_dog.png',
    },
    items: [
        {
            title: 'Аккумуляторная дрель-шуруповерт Daewoo DAA 1220Li',
            img: 'http://localhost:3001/Daewoo_DAA_1220Li.jpg',
            description: 'Аккумуляторный шуруповерт — 1 шт. Аккумуляторная батарея — 2 шт. Зарядное устройство — 1 шт.',
            isFavorite: false,
            id: getId()
        },
        {
            title: 'Угловая шлифмашина Makita GA5030',
            img: 'http://localhost:3001/Makita_GA5030.jpg',
            description: 'Углошлифовальная машина Makita GA5030 – это компактный и легкий инструмент с мощным двигателем на 720 Вт. УШМ эффективна при выполнении различных отрезных, шлифовальных и зачистных строительных работ. Лабиринтное уплотнение защищает все элементы механизма от пыли и абразивного строительного мусора, поэтому с помощью этой УШМ можно без проблем работать даже с такими пылеобразующими материалами, как бетон, камень и кирпич.',
            isFavorite: false,
            id: getId()
        },
        {
            title: 'Пила циркулярная Stanley SC16',
            img: 'http://localhost:3001/Stanley_SC16.jpg',
            description: 'Компактная, легкая, простая в использовании дисковая пила значительно снизит наступление усталости оператора при продолжительных работах. Увеличена надежность внутренних деталей и узлов инструмента – срок наработки на отказ увеличен практически в 2 раза.',
            isFavorite: false,
            id: getId()
        },
        {
            title: 'Фрезер RZTK MC 1600',
            img: 'http://localhost:3001/RZTK_MC_1600.jpg',
            description: 'RZTK MC 1600 — надежный и производительный электрический фрезер, предназначенный для фрезерования изделий из дерева и древесных материалов. Он готов к выполнению работ различной сложности, в частности обработки кромки, проделывания пазов.',
            isFavorite: false,
            id: getId()
        }
    ],
    token: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NSIsIm5hbWUiOiJKb2huIEdvbGQiLCJhZG1pbiI6dHJ1ZX0K.LIHjWCBORSWMEibq-tnT8ue_deUqZx1K0XxCOXZRrBI`
}