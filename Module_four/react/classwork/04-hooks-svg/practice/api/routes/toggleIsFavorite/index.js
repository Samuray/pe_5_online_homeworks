const {items, token} = require('../../data');
const {sendBadResponse, sendGoodResponse} = require("../utils");

const toggleIsFavorite = (app) => {
    app.patch('/toggle-favorite', (req, res) => {
        if (!req.body?.id) {
            sendBadResponse(res, 400, `Id is required`);
            return;
        }

        const index = items.findIndex(({id}) => id === req.body?.id)
        if (index === -1) {
            sendBadResponse(res, 404, `Item with id ${req.body?.id} not found`);
            return;
        }

        items[index].isFavorite = !items[index].isFavorite;
        setTimeout(() => {
            sendGoodResponse(res, items[index]);
        }, 2000)
    })
}
module.exports = toggleIsFavorite;