let http = require('http');
let fs = require('fs');
let path = require('path');

let app = http.createServer(function (req, res){
    let filePath = getFilePath(req.url);
    fs.readFile(filePath, function (err, data) {
        res.end(data);
    })
})

app.listen(3002);

let getFilePath = function (url) {
    let fileName = url.length > 1 && url.includes('.')? url.substring(1) : 'index.html';
    return path.resolve(__dirname, 'build', fileName);
};

// let app = http.createServer(function (req, res){
//     let filePath = getFilePath(req.url);
//     fs.readFile(filePath, function (err, data) {
//         if(err) {
//             res.end('<h1>Some Error</h1>' . err)
//         } else {
//             res.end(data);
//         }
//     })
// })

// let app = http.createServer(function (req, res){
//     let filePath = getFilePath(req.url);
//     fs.readFile(filePath, function (err, data) {
//         if(err) {
//             res.end('<h1>Some Error</h1>' + err.message)
//         } else {
//             res.end(data);
//         }
//     })
// })
//
// app.listen(3006);