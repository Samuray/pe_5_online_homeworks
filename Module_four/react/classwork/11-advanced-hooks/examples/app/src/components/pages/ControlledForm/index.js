import React, {useEffect, useRef, useState} from "react";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const ControlledForm = () => {

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [touched, setTouched] = useState({})
    const [errors, setErrors] = useState({});
    const [values, setValues] = useState({
        email: '',
        password: '',
    })

    const validateForm = () => {
        const newErrors = {};

        const {email,password} = values

        if(!email) {
            newErrors.email = 'This field is required';
        } else if(!EMAIL_REGEX.test(email)) {
            newErrors.email = 'This is not a valid email';
        }

        if(!password) {
            newErrors.password = 'This field is required';
        }

        return newErrors;
    }

    useEffect(() => {
        setErrors(validateForm())
    }, [values])

    const handleChange = (e) => {
        const {name, value} = e.target;
        setValues({...values, [name]: value});
    }

    const handleBlur = (e) => {
        const {name} = e.target;
        setTouched({...touched, [name]: true})
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setIsSubmitting(true);

        setTouched({email: true, password: true})

        const newError = validateForm();
        setErrors(newError);

        if(Object.keys(newError).length) {
            setIsSubmitting(false);
            return;
        }

        setTimeout(()=>{ setIsSubmitting(false); }, 3000)

    }


    return (
        <form onSubmit={handleSubmit} noValidate>
            <h3>Uncontrolled from</h3>
            <div>
                <label>
                    <input
                        type="email"
                        name='email'
                        placeholder="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        required
                        value={values.email} />
                </label>
                {errors.email && touched.email && <div className='error'>{errors.email}</div>}
            </div>
            <div>
                <label>
                    <input
                        type="password"
                        name='password'
                        placeholder="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                           required />
                </label>
                {errors.password && touched.password && <div className='error'>{errors.password}</div>}
            </div>
            <div>
                <button type='submit' disabled={isSubmitting}>Submit</button>
            </div>
        </form>
    )
}

export default ControlledForm;