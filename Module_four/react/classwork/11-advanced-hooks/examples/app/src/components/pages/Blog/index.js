import {NavLink, useNavigate} from 'react-router-dom'
import ThemeContext from "../../../context";
import {useContext} from "react";

export default function Blog () {
    const navigate = useNavigate();

    const {theme, setTheme, setColor} = useContext(ThemeContext)






    return (<>
            <h1 style={{color: theme.color}}>Blog</h1>

            <button onClick={()=>{ setTheme({color:'blue'}) }}>Change color to blue</button>
            <button onClick={()=>{ setColor({color:'blue'}) }}>Change color to blue</button>
            <NavLink to='1'>post 1</NavLink>
            <NavLink to='2'>post 2</NavLink>
            <NavLink to='/blog/3'>post 3</NavLink>
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium ad amet at consectetur fugiat hic illum in, modi nemo neque numquam, obcaecati odio, sit tempore ut vel veritatis voluptatibus.</div>
            </>
    )
}