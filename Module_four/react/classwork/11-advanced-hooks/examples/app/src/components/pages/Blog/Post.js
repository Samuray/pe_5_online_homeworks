import {useParams, useNavigate, Navigate} from "react-router-dom";
import {useEffect} from 'react';

export default function Post () {

    const navigate = useNavigate();
    const params = useParams()

    useEffect(() => {
        if(true) {
            navigate('/');
        }
    })

    // if(true) {
    //     return <Navigate to='/'></Navigate>;
    // }

    return (<>
            <h1>Post {params.id}</h1>
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium ad amet at consectetur fugiat hic illum in, modi nemo neque numquam, obcaecati odio, sit tempore ut vel veritatis voluptatibus.</div>
        </>
    )
}