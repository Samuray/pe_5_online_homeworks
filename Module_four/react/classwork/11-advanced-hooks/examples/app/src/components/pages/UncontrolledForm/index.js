import React, {useRef, useState} from "react";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const UncontrolledForm = () => {

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [errors, setErrors] = useState({})

    const emailRef = useRef();
    const passwordRef = useRef();

    const validateForm = () => {
        const newErrors = {};
        const email = emailRef.current.value;
        const password = passwordRef.current.value;

        if(!email) {
            newErrors.email = 'This field is required';
        } else if(!EMAIL_REGEX.test(email)) {
            newErrors.email = 'This is not a valid email';
        }

        if(!password) {
            newErrors.password = 'This field is required';
        }

        return newErrors;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setIsSubmitting(true);

        const newError = validateForm();
        setErrors(newError);

        if(Object.keys(newError).length) {
            setIsSubmitting(false);
            return;
        }


        setTimeout(()=>{ setIsSubmitting(false); }, 3000)

    }


    return (
        <form onSubmit={handleSubmit} noValidate>
            <h3>Uncontrolled from</h3>
            <div>
                <label>
                    <input type="email" name='email' placeholder="email" ref={emailRef} required defaultValue='' />
                </label>
                {errors.email && <div className='error'>{errors.email}</div>}
            </div>
            <div>
                <label>
                    <input type="password" name='password' placeholder="password" ref={passwordRef} required />
                </label>
                {errors.password && <div className='error'>{errors.password}</div>}
            </div>
            <div>
                <button type='submit' disabled={isSubmitting}>Submit</button>
            </div>
        </form>
    )
}

export default UncontrolledForm;