import {useCallback, useMemo, useState} from "react";

import SubElementMemo from "../SubElementMemo";
import NewSubElement from "../SubElement";



const Memo = () => {

    const [count, setCount] = useState(0);

    const buttonHandler = useCallback(() => {
        setCount((count) => {return count + 1})
    }, [])

    return (<>
        <h1>UseMemo</h1>
        <SubElementMemo title="SubElementMemo"></SubElementMemo>


        <SubElement count={count} someValue={2}></SubElement>

        <button onClick={()=>{ setCount(count+1) }}>Increase</button>

        <NewSubElement buttonHandler={buttonHandler}></NewSubElement>
        </>
     )
}

function SubElement({count, someValue}) {

   // let summedValue = expensiveCalculation(someValue)
    const summedValue = useMemo(
        ()=>{ return expensiveCalculation(someValue) },
        [someValue]
    );

    return <div><h1>{count}</h1> {summedValue} </div>
}

const expensiveCalculation = (num) => {
    num += num;
    //console.log('expensive Calculation...')
    return num;
}


export default Memo;