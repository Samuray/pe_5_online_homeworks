import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Routes, Route, HashRouter, Navigate, useRoutes, Form, NavLink} from "react-router-dom";
import Blog from './components/pages/Blog';
import Home from './components/pages/Home';
import Contact from './components/pages/Contact';
import PageNotFound from './components/pages/PageNotFound'
import Template from "./components/Template";
import Post from "./components/pages/Blog/Post";
import UncontrolledForm from "./components/pages/UncontrolledForm"
import ControlledForm from "./components/pages/ControlledForm"
import FormikForm1 from "./components/pages/FormikForm1"
import Reducer from "./components/pages/Reducer"
import Callback from "./components/pages/Callback"
import Memo from "./components/pages/Memo"
import ThemeContext from "./context";

import {useState} from "react";
import {Provider} from "react-redux";

import {PersistGate} from "redux-persist/integration/react";
import store, {persistor} from "./store";

function Main() {
    const [clicked, setClicked] = useState(0);

    const clickHandler = () => {
        setClicked(clicked + 1);
    }

    let elements = useRoutes([
        {
            path: '/',
            element: <Template clicked={clicked}/>,
            children: [
                {path: '/', element: <Home/>},
                {path: '/blog', element: <Blog/>},
                {path: '/contact', element: <Contact clickHandler={clickHandler}/>},
                {path: '/post', element: <Post/>},
                {path: '/uncontrolled', element: <UncontrolledForm/>},
                {path: '/controlled', element: <ControlledForm/>},
                {path: '/FormikForm1', element: <FormikForm1/>},
                {path: '/reducer', element: <Reducer/>},
                {path: '/callback', element: <Callback/>},
                {path: '/memo', element: <Memo/>},


                {path: '*', element: <PageNotFound/>},
            ]
        }
    ])

    return elements;
}

function App() {
    const savedThemeConfig = localStorage.getItem('theme')
    const [theme, setTheme] = useState(savedThemeConfig ? JSON.parse(savedThemeConfig) : {color: 'red'});

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <BrowserRouter>
                    <ThemeContext.Provider value={{theme: theme, setTheme: setTheme, setColor: (theme)=> { localStorage.setItem('theme', JSON.stringify(theme));  setTheme(theme)}}}>
                        <Main/>
                    </ThemeContext.Provider>
                </BrowserRouter>
            </PersistGate>
        </Provider>
    )
}

export default App;
