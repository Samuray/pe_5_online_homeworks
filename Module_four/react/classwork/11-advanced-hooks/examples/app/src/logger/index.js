const loggerMiddleware = (store) => (next) => (action) => {
    console.log(store.getState(), action);
    next(action);
}

export default loggerMiddleware;