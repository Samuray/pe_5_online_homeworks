import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const initialState =  {
    clicked: 0,
    date: '',
    status: 'new',
    user: {
        isLogged: false,
    }
}

export const fetchAsyncCount = createAsyncThunk(
    'counter/fetchAsyncCount',
    async (_, {rejectWithValue}) => {
        try {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            return result
        } catch (error) {
            return rejectWithValue(error.message)
        }
    }
)

const counterSlice = createSlice({
    name: 'counter',
    initialState: initialState,
    reducers: {
        increment(state, action) {
            state.clicked += action.payload.step
        },
        setClicked(state, action) {
            state.clicked = action.payload.count
        },
    },
    extraReducers: {
        [fetchAsyncCount.pending]: (state, action) => {
            state.status = 'loading';
        },
        [fetchAsyncCount.fulfilled]: (state, action) => {
            state.status = 'loaded';
            state.clicked = action.payload.count;
        },
        [fetchAsyncCount.rejected]: (state, action) => {
            state.status = 'rejected: error ' + action.payload;
        },
    }
})

export const {increment, setClicked} = counterSlice.actions;

export default counterSlice.reducer;