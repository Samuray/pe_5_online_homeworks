import mysql from 'mysql2';

const pool = mysql.createPool({
    host: 'db4free.net',
    user: 'owermysqltest',
    password: 'bxboRZQU1qneb0w3',
    database: 'owermysqltest',
}).promise();

export async function getPosts() {
    const [rows] = await pool.query("SELECT * FROM `posts`");
    return rows;
}

export async function getPost(id) {
    const [rows] = await pool.query("SELECT * FROM `posts` WHERE `id` = ? ", [id]);
    return rows[0];
}

export async function createPost(title, text) {
    const [result] = await pool.query("INSERT INTO `posts` (`title`,`text`) VALUES (?,?) ", [title, text]);
    const id = result.insertId;
    return getPost(id);
}

export async function updatePost(id, title, text) {
    await pool.query("UPDATE `posts` SET `title` = ? ,`text` = ? WHERE `id` = ? ", [title, text, id]);
    return getPost(id);
}

export async function deletePost(id) {
    await pool.query("DELETE FROM `posts` WHERE `id` = ? ", [id]);
}