import express from 'express'

import {getPosts, getPost} from "./db.js";

const app = express()


app.get('/posts', async (req, res) => {
    const posts = await getPosts();
    res.send(posts)
})

app.get('/post/:id', async (req, res) => {
    const post = await getPost(req.params.id);
    res.send(post)
})





app.listen(3002);