import "./App.css";
import { Routes, Route, Navigate } from "react-router-dom";
import Films from "./components/Films";
import FullFilm from "./components/Film/FullFilm";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import Login from "./components/Login/Login";
import { useState } from "react";

function App() {
  const [isAuth, setIsAuth] = useState(false);
  function LoginUser() {
    setIsAuth(true);
  }
  return (
    <Routes>

      <Route path="/" element={<Navigate to="/films" replace />}></Route>
      <Route path="/films" element={<ProtectedRoute isAuth={isAuth} Component={<Films />} />}></Route>
      <Route path="/films/:id" element={<ProtectedRoute isAuth={isAuth} Component={<FullFilm />} />}></Route>
      <Route path="/login" element={<Login isAuth={isAuth} setIsAuth={LoginUser} />}></Route>

    </Routes>
  );
}

export default App;
