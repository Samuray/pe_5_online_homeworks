import { useState } from "react";
import Detail from "../Detail";
import { Link } from "react-router-dom";

export default function Film({ name, filmId, openingCrawl }) {
  const [clicked, setClicked] = useState(false);

  return (
    <li>
      <span>{name}</span>
      {clicked ? (
        <Detail filmId={filmId} openingCrawl={openingCrawl} />
      ) : (
        <Link to={`/films/${filmId}`}>Детальнее</Link>
      )}
    </li>
  );
}
