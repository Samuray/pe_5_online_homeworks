import Film from "../Film";
import { useEffect, useState } from "react";
import Loader from "../Loader";

export default function Films() {
  const [isLoading, setIsLoading] = useState(true);
  const [films, setFilms] = useState([]);

  useEffect(() => {
    fetch("https://ajax.test-danit.com/api/swapi/films")
      .then((res) => res.json())
      .then((films) => {
        setFilms(films);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return <Loader />;
  }
  return (
    <ol>
      {films.map((film) => {
        return (
          <Film
            key={film.id}
            name={film.name}
            filmId={film.id}
            openingCrawl={film.openingCrawl}
          />
        );
      })}
    </ol>
  );
}
