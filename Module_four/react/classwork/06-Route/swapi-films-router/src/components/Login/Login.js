import React from "react";
import { Navigate } from "react-router-dom";

export default function Login({ isAuth, LoginUser }) {
  return isAuth ? (
    <Navigate to="/films" replace />
  ) : (
    <form onSubmit={LoginUser}>
      <input type="text"></input>
      <input type="password"></input>
      <input type="email"></input>
    </form>
  );
}
