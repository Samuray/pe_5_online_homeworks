import React from "react";


export default function Button({ text, changeClicked }) {
    const clickHandler = () => {
        changeClicked(true);
    }
    return <button onClick={clickHandler}>{text}</button>
}