const express = require("express");
const app = express();
const pug = require("pug");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

app
    .route("/")
    .get((req, res) => {
        res.render("pug", { title: "PUG-TITLE" });
    })
    .post((req, res) => {});

app
    .route("/goods")
    .get((req, res) => {
        res.send("GOODS");
    })
    .post((req, res) => {});

const accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), { flags: "a" });
app.use(morgan("combined", { stream: accessLogStream }));
app.use("/template", express.static("public"));

app.engine("pug", require("pug").__express);
app.set("views", "./views");
app.set("view engine", "pug");

app.listen(8080, () => {});

// app.get('/', (req, res)=> {
//   res.send("hello world")
// })
// app.post("/", (req, res) => {

// });

// app
//   .route("/static")
//   .get((req, res) => {
//     res.send("GOODS");
//   })
//   .post((req, res) => {});
