const express = require('express');

const app = express();

app.get("/", (req, res) => {
    res.send("<h1>Home page</h1>");
});

app.get("/about", (req, res) => {
    res.send("<h1>about page</h1>");
});

app.route('/user')
    .get((req, res) => {
        res.send("<h1>Get some user</h1>");
    })
    .post((req, res) => {
        res.send("<h1>Add new user</h1>");
    })
    .put((req, res) => {
        res.send("<h1>Edit user</h1>");
    });


app.all("/all", (req, res) => {
    res.send("<h1>All page example</h1>");
});

app.listen(3000);

