const express = require('express');
const app = express();

const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());

//app.engine('pug', require('pug').__express);
app.set('views', './views');
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => {
    res.send('<form method="POST"><input name="username"><input type="submit"></form>')
});

app.post('/', (req, res) => {
    console.log(req.headers);
    res.send('ok: ' + req.body.username);
});

app.get('/pug/', (req, res) => {
    res.render('pug', {title: 'Hi', message: 'Test pug'});
})

app.get('/ejs/', (req, res) => {
    //const films = Model.getFilms();
    res.render('ejs', {title: 'Hi', message: 'Test ejs'});
})

app.listen(3000);