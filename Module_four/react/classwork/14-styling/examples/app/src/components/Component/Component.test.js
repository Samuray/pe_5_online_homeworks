import Component from "./index";
import {fireEvent, render, screen} from "@testing-library/react";



describe('Component testing', () => {
    test('Component should increment', () => {

        const view = render(<Component />);
        const button  = screen.getByText('MyButton');

        const counter = screen.getByRole('counter');

        expect(counter.textContent).toBe("0");

        fireEvent.click(button);

        expect(counter.textContent).toBe("1");
    })
})