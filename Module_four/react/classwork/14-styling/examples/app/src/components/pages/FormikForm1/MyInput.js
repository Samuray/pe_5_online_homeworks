import React from "react";

export default function MyInput(props) {
    const {field, form, label, ...rest} = props;

    const {name} = field;

    return (
        <>
            <div>
                <label>
                    <input{...field} {...rest} />
                </label>
                {form.errors[name] && form.touched[name] && <div className='error'>{form.errors[name]}</div>}
            </div>
        </>

    )
}

