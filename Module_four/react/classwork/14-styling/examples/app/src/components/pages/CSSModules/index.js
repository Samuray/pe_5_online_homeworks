import styles from './CSSModules.module.scss';
import React from 'react'
import classNames from 'classnames';

export default function CSSModules() {

    console.log(styles);
    const selected = false;
    return (
        <>
            <h1 className={styles['css-modules']}>CSSModules</h1>
            <h1 className={styles.blue}>CSSModules</h1>

            <h2 className='h2'>Global h2 example</h2>

            <h1 className={classNames(styles.cssModules, {[styles.selected]: selected})}>classNames</h1>
            <h1 className={classNames(styles.cssModules, {[styles.selected]: true})}>classNames</h1>

            <h1 className={()=>{ let classes = '';classes += selected ? styles.cssModules : ''; return classes;}}>classNames</h1>
        </>
    )
}