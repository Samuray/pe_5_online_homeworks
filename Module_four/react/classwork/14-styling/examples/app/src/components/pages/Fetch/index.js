import {useEffect, useState} from "react";

const Fetch = () => {
    const [count, setCount] = useState(0);

    useEffect(()=> {
        (async () => {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            setCount(result.count)
        })()
    },[])

    return (
        <>
            <h1>Fetch examnple</h1>
            <h2 onClick={()=>{ setCount(count+1) }}>{count}</h2>
        </>
    )
}

export default Fetch;