import ReactDom from "react-dom/client";
import Button from "./Button";
import { act } from "react-dom/test-utils";
import renderer from 'react-test-renderer';

global.IS_REACT_ACT_ENVIRONMENT = true;


let container = null;
let root = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
    root = ReactDom.createRoot(container)

})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

jest.mock("./SubButton",() => () => {
    return <mock-modal />;
})

test("Button tests", async () => {

    await act(async ()=>{
        root.render(<Button title='React' />);
    });
    const element = document.querySelector('button');
    expect(container.textContent).toBe('Hi, React');
    expect(element.textContent).toBe('Hi, React');

    console.log(container.querySelector('.test').innerHTML);

})

test("Button snapshot", async () => {
    const tree = renderer.create('<Button title="test"/>').toJSON();

    expect(tree).toMatchSnapshot();
})