import {Outlet, NavLink} from 'react-router-dom'
import {connect} from "react-redux";
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {increment, setClicked, fetchAsyncCount} from "../store/counterSlice";
import {setClickedAsync} from '../actions'

import '../App.css'
import ThemeContext from "../context";
import {useContext} from "react";
import Fetch from "./pages/Fetch";
import Styled from "./pages/Styled";

export default function Template () {

    const {theme} = useContext(ThemeContext)

    const clicked = useSelector(state => state.counter.clicked, shallowEqual);
    const status = useSelector(state => state.counter.status, shallowEqual);

    const dispatch = useDispatch();

    const incrementHandler = () => dispatch(increment({step: 10}))

    function myAsyncAction(dispatch) {
        return async () => {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            dispatch({type: 'counter/setClicked', payload: result})
        }
    }

    return (
        <div className='container'>
            <nav>
                <NavLink to='/' className={(prop) =>  prop.isActive ? 'active-link' : ''}>Home</NavLink>
                <NavLink style={{color: theme.color}} to='blog' className={({isActive}) =>  isActive ? 'active-link' : ''}>Blog</NavLink>
                <NavLink to='form'>Form</NavLink>
                <NavLink to='contact' className={({isActive}) =>  isActive ? 'nav-link active-link' : 'nav-link'}> Contact </NavLink>

                <NavLink to='uncontrolled'>uncontrolled Form</NavLink>
                <NavLink to='controlled'>controlled Form</NavLink>
                <NavLink to='FormikForm1'>FormikForm1</NavLink>
                <NavLink to='reducer'>UseReducer</NavLink>
                <NavLink to='callback'>UseCallback</NavLink>
                <NavLink to='memo'>UseMemo</NavLink>
                <NavLink to='Fetch'>Fetch</NavLink>
                <NavLink to='Component'>Component</NavLink>
                <NavLink to='Dispatch'>Dispatch</NavLink>
                <NavLink to='CSSModules'>CSSModules</NavLink>
                <NavLink to='Styled'>Styled</NavLink>
                <NavLink to='Material'>Material</NavLink>


            </nav>
            <div>
                Clicked: {clicked}<br />
                Status: {status}
            </div>

            <Outlet />
        </div>
    )
}

// const mapStateToProps = (state) => {
//     return {
//         clicked: state.clicked
//     }
// }
//
// const mapDispatchToProps = (dispatch) => ({
//     increment: () => dispatch({
//         type: 'INCREMENT',
//         payload: { step: 10 },
//     }),
// })
//
// export default connect(mapStateToProps, mapDispatchToProps)(Template)
