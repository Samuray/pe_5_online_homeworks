import styled, {css} from 'styled-components';

const Title = styled.h1`
    color: red;
    font-weight: bold;
    
    ${(props) => props.primary && css`
        background: green;
    `}
`;

const SuperTitle = styled(Title)`
    color: gold;
`;

export default function Styled() {

    return (
        <>
            <Title>StyledTitle</Title>
            <Title primary>StyledTitle</Title>

            <SuperTitle>StyledSuperTitle</SuperTitle>
            <SuperTitle primary>StyledSuperTitle</SuperTitle>
        </>
    )
}