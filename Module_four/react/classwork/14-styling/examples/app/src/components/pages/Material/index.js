import  Button from '@mui/material/Button'
import PrimarySearchAppBar from "./PrimarySearchAppBar";
import AgricultureIcon from '@mui/icons-material/Agriculture';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';


export default function Material() {

    return (
        <div>

            <Button variant='contained'><AddShoppingCartIcon /> &nbsp; Button</Button>

            <PrimarySearchAppBar />
        </div>
    )

}