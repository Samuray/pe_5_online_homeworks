import {Link} from "react-router-dom";

export default function Contact ({clickHandler}) {
    return (
        <>
            <h1>Contacts</h1>

            <button onClick={clickHandler}>Click</button>
            <br /><br />
            <Link to='/'>Go home</Link>
        </>

    )
}