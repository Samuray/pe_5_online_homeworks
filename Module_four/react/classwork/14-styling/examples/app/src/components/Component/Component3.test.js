import {ComponentWithRedux} from "./index";
import {fireEvent, render, screen} from "@testing-library/react";
import * as reduxHooks from "react-redux";

jest.mock("react-redux");

describe('Component testing ', () => {
    test('Component should increment', () => {

        jest.spyOn(reduxHooks, 'useSelector').mockReturnValue([])

        const view = render(<ComponentWithRedux />);

        expect(view).toMatchSnapshot();
    }) 
})