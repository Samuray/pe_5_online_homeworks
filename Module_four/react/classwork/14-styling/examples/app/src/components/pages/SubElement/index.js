import {memo} from "react";


function SubElement({buttonHandler}) {

     console.log('SubElement rendering');

    return <><hr /><button onClick={buttonHandler}>SubElement Increase</button></>
}

export default memo(SubElement);