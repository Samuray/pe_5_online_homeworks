import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../Loader";
import { loadFilm } from "../../features/filmsSlice";

export default function Detail() {

    const dispatch = useDispatch();

    const status = useSelector(state => state.movies.film.status)

    const film = useSelector(state => state.movies.film.filmElement);

    const error = useSelector(state => state.movies.film.error);

    const { id } = useParams();

    useEffect(() => {
        dispatch(loadFilm(id))
    }, [])

    if (error) {
        return <p>error.message</p>
    } else {
       return status === 'loading'
        ?
        <Loader />
        :
        <div>
            <p>Film: {film.name}</p>
            <p>Episode: {film.episodeId}</p>
            <p>{film.openingCrawl}</p>
        </div>
    }


}