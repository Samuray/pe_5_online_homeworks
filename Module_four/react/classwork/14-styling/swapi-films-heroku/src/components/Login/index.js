import { Navigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';

export default function Login({ isAuth, setIsAuth }) {

    const formik = useFormik({
        initialValues: {
            email: 'test@gmail.com',
            password: '*******'
        },
        validationSchema: Yup.object({
            email: Yup.string().email('Invalid email address').required('Required'),
            password: Yup.string().required('Required'),
        }),
        onSubmit: values => {
            const email = values.email;
            setIsAuth(true);
            localStorage.setItem('authData', JSON.stringify(email));
        }
    });

    return (
        isAuth
            ?
            <Navigate to="/films" replace />
            :
            <form onSubmit={formik.handleSubmit} style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }} noValidate>
                <label><input type="email" name="email" onChange={formik.handleChange} value={formik.values.email} />
                    {formik.touched.email && formik.errors.email ? (
                        <div>{formik.errors.email}</div>
                    ) : null}
                </label><br />
                <label><input type="password" name="password" onChange={formik.handleChange} value={formik.values.password} />
                {formik.touched.password && formik.errors.password ? (
                        <div>{formik.errors.password}</div>
                    ) : null}
                </label><br />
                <button type="submit">Авторизуватись</button>
            </form>
    );
}