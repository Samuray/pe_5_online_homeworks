import Loader from "../Loader";
import Film from "../Film";
import {useState, useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadFilms } from "../../features/filmsSlice";


export default function Films() {

    const dispatch = useDispatch();

    const status = useSelector(state => state.movies.films.status)

    const films = useSelector(state => state.movies.films.filmsList);

    const error = useSelector(state => state.movies.films.error);

    useEffect(() => {
        dispatch(loadFilms());
    }, []);

    if (error) {
        return <p>{error.message}</p>
    } else {
        if (status === 'loading') {
            return <Loader/>;
        } else {
            return (
                <ol>
                    {
                            films
                            .map(film => <Film key={film.id} id={film.id} name={film.name}/>)
                    }
                </ol>
            );
        }
    }

}