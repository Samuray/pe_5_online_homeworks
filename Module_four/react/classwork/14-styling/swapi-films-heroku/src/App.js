import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Header from './components/Header';
import Films from './components/Films';
import Detail from './components/Detail';
import NotFound from './NotFound';
import ProtectedRoute from './components/ProtectedRoute';
import Login from './components/Login';

import './App.css';

function App() {

  const [isAuth, setIsAuth] = useState(false);

  // Обработчик для формы
  const onUserLogIn = (e) => {
    e.preventDefault();
    const email = document.getElementById('user_email').value;
    setIsAuth(true);
    localStorage.setItem('authData', JSON.stringify(email));
  }

  // Обработчик при клике на логаут
  const onUserLogOut = () => {
    localStorage.removeItem('authData')
    setIsAuth(false);
  }

  // Проверка залогинен ли пользователь при запуске приложения
  useEffect(() => {
    const authData = JSON.parse(localStorage.getItem('authData'));
    if (authData) {
      setIsAuth(true);
    }
  }, []);

  return <BrowserRouter>
    <Routes>
      <Route path='/' element={<Header isAuth={isAuth} clickHandler={onUserLogOut} />}>
        <Route index element={<Navigate to='/films' replace />} />
        <Route path='/films' element={<ProtectedRoute isAuth={isAuth} content={<Films />} />} />
        <Route path='/films/:id' element={<ProtectedRoute isAuth={isAuth} content={<Detail />} />} />
        <Route path='/login' element={<Login isAuth={isAuth} setIsAuth={setIsAuth} onUserLogIn={onUserLogIn} />} />
      </Route>
      <Route path='*' element={<NotFound />} />
    </Routes>
  </BrowserRouter>

}

export default App;
