import { Link } from "react-router-dom";

export default function Film({ name, id }) {

    return <li>
        <span>{name}</span>
        <Link to={`/films/${id}`}>Детальнее</Link>
    </li>
}