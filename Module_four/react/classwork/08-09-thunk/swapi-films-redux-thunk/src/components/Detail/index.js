import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Loader from "../Loader";

export default function Detail() {
    const [isLoading, setIsLoading] = useState(true);

    const [film, setFilm] = useState([]);

    const { id } = useParams();

    useEffect(() => {
        fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`)
            .then(res => res.json())
            .then(film => {
                setFilm(film);
                setIsLoading(false);
            })
    }, [])
    return (
        isLoading
            ?
            <Loader />
            :
            <div>
                <p>Film: {film.name}</p>
                <p>Episode: {film.episodeId}</p>
                <p>{film.openingCrawl}</p>
            </div>
    );

}