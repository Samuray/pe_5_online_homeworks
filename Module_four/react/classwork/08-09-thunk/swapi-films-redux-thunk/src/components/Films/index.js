import Loader from "../Loader";
import Film from "../Film";
import {useState, useEffect} from "react";
import {store} from "../../index.js"

export default function Films() {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        (async () => {
            const res = await fetch('https://ajax.test-danit.com/api/swapi/films');
            store.dispatch({type: 'films/set', payload: await res.json()});
            setIsLoading(false);
        })()
    }, []);

    if (isLoading) {
        return <Loader/>;
    } else {
        return (
            <ol>
                {
                    store
                        .getState()
                        .films
                        .map(film => <Film key={film.id} id={film.id} name={film.name}/>)
                }
            </ol>
        );
    }
}