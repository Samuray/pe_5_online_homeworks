import {Outlet, NavLink} from 'react-router-dom'
import {connect} from "react-redux";
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {increment, setClicked, fetchAsyncCount} from "../store/counterSlice";
import {setClickedAsync} from '../actions'

import '../App.css'

export default function Template () {

    const clicked = useSelector(state => state.counter.clicked, shallowEqual);
    const status = useSelector(state => state.counter.status, shallowEqual);

    const dispatch = useDispatch();

    const incrementHandler = () => dispatch(increment({step: 10}))

    function myAsyncAction(dispatch) {
        return async () => {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            dispatch({type: 'counter/setClicked', payload: result})
        }
    }

    return (
        <div className='container'>
            <nav>
                <NavLink to='/' className={(prop) =>  prop.isActive ? 'active-link' : ''}>Home</NavLink>
                <NavLink to='blog' className={({isActive}) =>  isActive ? 'active-link' : ''}>Blog</NavLink>
                <NavLink to='form'>Form</NavLink>
                <NavLink to='contact' className={({isActive}) =>  isActive ? 'nav-link active-link' : 'nav-link'}> Contact </NavLink>
            </nav>
            <div>
                Clicked: {clicked}<br />
                Status: {status}
            </div>

            <button onClick={ () => {dispatch(increment({step:1}))} }>Increment</button>
            <br />
            <br />
            <button onClick={ myAsyncAction(dispatch) }>myAsyncAction</button>
            <br />
            <br />
            <button onClick={ () => {dispatch(setClickedAsync())} }>with action</button>
            <br />
            <br />
            <button onClick={ () => {dispatch(setClicked())} }>with action Slice</button>
            <br />
            <br />
            <button onClick={ () => {dispatch(fetchAsyncCount())} }>with fetchAsyncCount</button>

            <Outlet />
        </div>
    )
}

// const mapStateToProps = (state) => {
//     return {
//         clicked: state.clicked
//     }
// }
//
// const mapDispatchToProps = (dispatch) => ({
//     increment: () => dispatch({
//         type: 'INCREMENT',
//         payload: { step: 10 },
//     }),
// })
//
// export default connect(mapStateToProps, mapDispatchToProps)(Template)
