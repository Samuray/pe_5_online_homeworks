import reducer from './counterSlice';
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import storage from 'redux-persist/lib/storage';
import {persistStore, persistReducer} from "redux-persist";
// import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import loggerMiddleware from '../logger'

const persistConfig = {
    key: 'root',
    storage
}

const rootReducer = combineReducers({counter: reducer})

const persistedReducer = persistReducer(persistConfig, rootReducer);

//const store  = createStore(persistedReducer, applyMiddleware(thunk))

const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk, loggerMiddleware],
})

export const persistor = persistStore(store);

export default store;