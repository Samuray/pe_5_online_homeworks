

export const setClickedAsync = () => {
    return async dispatch => {
        dispatch(setClickedStarted())

        try {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            dispatch(setClickedSuccess(result.count))
        } catch (error) {
            dispatch(setClickedRejected(error.message))
        }

    }
}

const setClickedSuccess = (count) => ({
    type: 'counter/setClicked', payload: {count:count}
});

const setClickedStarted = () => ({
    type: 'counter/setStarted',
})

const setClickedRejected = (error) => ({
    type: 'counter/setRejected',
    payload: {error}
})