import Film from "../Film";
import Loader from "../Loader";
import { useEffect, useState } from "react";


const Films = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [films, setFilms] = useState([]);

    useEffect(() => {
        fetch('https://ajax.test-danit.com/api/swapi/films')
            .then(res => res.json())
            .then(films => {
                setFilms(films);
                setIsLoading(false);
            })
    }, [])

    if (isLoading) {
        return <Loader />;
    }
    return <ol>{films.map((film) => {
        return <Film key={film.id} name={film.name} episodeId={film.episodeId} openingCrawl={film.openingCrawl}/>
    })}</ol>

}

export  default  Films;