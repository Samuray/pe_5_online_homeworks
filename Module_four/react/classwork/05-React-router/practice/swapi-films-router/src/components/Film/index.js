import { useState } from "react";
import Button from "../Button";
import Detail from "../Detail";
import {Link} from "react-router-dom";

export default function Film({name, episodeId, openingCrawl}) {

    const [clicked, setClicked] = useState(false);
    
    return <li>
    <Link to={`films/${episodeId}`}>{name}</Link>
    {clicked 
    ? 
    <Detail episodeId={episodeId} openingCrawl={openingCrawl}/>
    : 
    <Button text='Детальнее' changeClicked={setClicked} />

    }
    </li>
}