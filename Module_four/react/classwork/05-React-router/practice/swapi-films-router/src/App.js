import './App.css';
import { Routes, Route } from "react-router-dom";
import Films from "./components/Films";
import FullFilm from "./components/Film/FullFilm";

function App() {

 return (
     <Routes>
        <Route path="/" element={<Films />}></Route>
        <Route path="/films" element={<Films />}></Route>
         <Route path="/films/:id" element={<FullFilm />}></Route>
     </Routes>
 );
}

export default App;
