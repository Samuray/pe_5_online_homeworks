import { Component } from "react";
import styles from "./counter.module.scss";
import Button from "../Button/Button";

class Counter extends Component {
    state = {
        count: 0,
    }
    getIncr = () => {
        this.setState(current => {
            return {
                count: current.count = this.state.count + 1
            }
        });
    }
    getDecr = () => {
        this.setState(current => {
            return {
                count: current.count = this.state.count - 1
            }
        });
    }

    getZero = () => {
        this.setState(current => {
            return {
                count: current.count = this.state.count = 0
            }
        });
    }

    render() {
       return (
           <div>
           <p className={styles.text}> Count: {this.state.count}</p>
             <Button onClick={this.getIncr} text={"+"}/>
               <Button onClick={this.getZero} text={"0"}/>
             <Button onClick={this.getDecr} text={"-"}/>

           </div>
       )

    }
}
export  default Counter;