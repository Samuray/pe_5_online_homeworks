import { Component } from "react";
import styles from  './App.module.scss';
import Counter from "./components/Counter/Counter";

class App extends Component {
  render() {
    return(
    <div className={styles.div}>
        <Counter/>
    </div>
    );
  }
}

export default App;
