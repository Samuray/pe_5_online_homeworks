import { Component } from "react";
import styles from "./button.module.scss";

class Button extends Component {
    render() {
        const { text, onClick } = this.props;
       return (
           <button onClick={onClick} className={styles.counterButton}>
               {text}
           </button>
       )
    }
}
export  default Button;