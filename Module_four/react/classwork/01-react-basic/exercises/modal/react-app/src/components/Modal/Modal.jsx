import { Component } from "react";
import styles from "./modal.module.scss";

class Modal extends Component {
    render() {
        const { text } = this.props;
       return(
        <div className={styles.modal}>
            <p className={styles.text}>{text}</p>
        </div>
       )
    }
}

export  default  Modal;