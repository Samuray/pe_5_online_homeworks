import { Component } from "react";
import styles from  './App.module.scss';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {
    state = {
        openModal: false,
    }

    clickModal = () => {
        this.setState(current => {
            return {
                openModal: !current.openModal ? current.openModal = true : current.openModal = false
            }
        })
    }

  render() {
    return(
        <>
        <Button text={"Open Modal"} openModal={this.clickModal}/>

        <div className={styles.div}>
            {this.state.openModal && <Modal text={"WTF?"}/>}
        </div>
        </>
    )
  }
}

export default App;
