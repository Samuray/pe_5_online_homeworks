import { Component } from "react";
import styles from "./button.module.scss";

class Button extends Component {
    render() {
        const { text, openModal } = this.props;
       return(
           <button className={styles.counterButton} onClick={openModal}>{text}</button>
       )
    }
}

export  default  Button;