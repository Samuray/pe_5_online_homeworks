import { useState } from "react";
import Button from "../Button";
import Detail from "../Detail";

export default function Film({name, episodeId, openingCrawl}) {

    const [clicked, setClicked] = useState(false);
    
    return <li>
    <span>{name}</span>
    {clicked 
    ? 
    <Detail episodeId={episodeId} openingCrawl={openingCrawl}/>
    : 
    <Button text='Детальнее' changeClicked={setClicked} />

    }
    </li>
}