import { Component } from 'react';
import getNameWithTime from "./utils/getNameWithTime";
import Image from "./components/Image";

class App extends Component {

    constructor() {
        getNameWithTime('App', 'CONSTRUCTOR');
        super();

        this.state = {
            title: 'Состояние и жизненный цикл',
            counter: 0,
            src: null,
        }
    }

    componentDidMount() {
        getNameWithTime('App','DID MOUNT');
    }

    componentDidUpdate() {
        getNameWithTime('App','DID UPDATE');
    }

    // componentDidCatch(error, errorInfo) {
    //     console.log('COMPONENT DID CATCH')
    //     console.log(error);
    //     console.log(errorInfo);
    // }

    // static getDerivedStateFromError(error) {
    //     console.log('GET DERIVED STATE FROM ERROR');
    //     console.log(error);
    // }

    render(){
        const { title, counter, src } = this.state;
          getNameWithTime('App','RENDER');
          return (
            <div className="App">
                <h1>
                    <a href="https://ru.reactjs.org/docs/react-component.html" target="_blank" rel="noreferrer">
                        { title }
                    </a>
                </h1>

                <p>{ counter }</p>
                <button onClick={() => this.setState(current => ({counter: current.counter + 1}))}>Increment counter</button>

                <div>
                    <button onClick={() => this.setState({ src: `https://placebeard.it/400x200?v=${Math.random()*450}` })}>Generate image src</button>
                    <button onClick={() => this.setState({ src: null })}>Delete image src</button>
                </div>

                {/*{src && <Image src={src} />}*/}
                </div>
        );
    }
}

export default App;
