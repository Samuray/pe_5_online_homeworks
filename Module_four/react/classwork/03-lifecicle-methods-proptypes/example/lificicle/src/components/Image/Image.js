import {PureComponent} from "react";
import getNameWithTime from "../../utils/getNameWithTime";

class Image extends PureComponent {

    constructor() {
        getNameWithTime('Image', 'CONSTRUCTOR');
        super();
    }

    componentDidMount() {
        getNameWithTime('Image','DID MOUNT');
    }

    componentDidUpdate() {
        getNameWithTime('Image','DID UPDATE');
    }

    componentWillUnmount() {
        getNameWithTime('Image','WILL UNMOUNT');
    }


    render(){
        getNameWithTime('Image','RENDER');
        const { src } = this.props;

        // if (!src.a.a) return null;

        return (
            <img style={{ backgroundColor: 'lightgray' }} src={src} alt="Some alt" width={400} height={200} />
        );
    }
}

export default Image;
