import {Component} from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import posts from "./constants/posts";

class App extends Component {

    render() {

        return (
            <div className="App">
                <Header title="PropTypes" user={{name: 'Sam', age: 26, avatar: 'https://i.pravatar.cc/40'}}/>
                <section className="posts-container-root">
                    <h1>POSTS</h1>
                    <div className="posts-container">
                        {posts.map(({id, title, body, userId}) =>
                            <div className="post-root">
                                <span>User: {userId}</span>
                                <h3>{title}</h3>
                                <p>{body}</p>
                            </div>
                        )}
                    </div>
                </section>
                <Footer title="PropTypes" year={new Date().getFullYear()}
                        onOrderFunc={() => console.log('Order call')}/>
            </div>
        );
    }
}

export default App;
