import React, { PureComponent } from 'react';
import styles from './Header.module.scss';

class Header extends PureComponent {
    render(){
        const { title, user: { name, age, avatar } } = this.props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <div className={styles.userContainer}>
                      <img src={avatar} alt={`user ${name}`} />
                      <span>{name}, {age}</span>
                  </div>
              </header>
        );
    }
};

export default Header;
