import React, { PureComponent } from 'react';
import classNames from "classnames";
import styles from './Button.module.scss';

class Button extends PureComponent {
    render(){
        const { children, type, onClick } = this.props;

        return (
            <button className={classNames(styles.btn)} type={type} onClick={onClick}>{children}</button>
        );
    }
};

export default Button;
