const sum = (a, b) => {
    return (a + b);
}

describe('Testing sum function', () => {

    test('1 + 1 should equal 2', ()=>{
        expect(sum(1, 1)).toBe(2);
    })

    it('10 + 10 should equal 20', ()=>{
        expect(sum(10, 10)).toBe(20);
    })

})