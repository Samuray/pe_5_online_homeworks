import {fireEvent, render, screen} from "@testing-library/react";

import * as actions from "../../../store/counterSlice"

import Dispatch from "./index";

import * as reduxHooks from 'react-redux'

jest.mock('react-redux');

const mockedDispatcher = jest.spyOn(reduxHooks, 'useDispatch');

describe('Dispatch test', () => {
    test('test render', () => {
        mockedDispatcher.mockReturnValue(jest.fn());

        const view = render(<Dispatch />);

        expect(view).toMatchSnapshot();
    });

    test('dispatch actions', () => {
        const dispatch = jest.fn();
        mockedDispatcher.mockReturnValue(dispatch)

        const mockedIncrement = jest.spyOn(actions, 'increment');

        const view = render(<Dispatch />);

        const button = screen.getByText('Increment');

        fireEvent.click(button);
        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(mockedIncrement).toHaveBeenCalledTimes(1);

        fireEvent.click(button);
        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(mockedIncrement).toHaveBeenCalledWith({step: 10});


    })
})