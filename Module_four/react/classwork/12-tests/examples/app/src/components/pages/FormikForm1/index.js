import React, {useEffect, useState} from "react";
import MyInput from "./MyInput";
import {Field, Form, Formik} from 'formik';
import {isValidDateValue} from "@testing-library/user-event/dist/utils";
import * as Yup from 'yup';

// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

// const validateEmail = (email) => {
//     let error = null;
//
//     if (!email) {
//         error = 'This field is required';
//     } else if (!EMAIL_REGEX.test(email)) {
//         error = 'This is not a valid email';
//     }
//
//     return error;
// }
//
// const validateRequired = (value) => value ? null : 'This field is required';

const FormikForm1 = () => {

    // const validateForm = (values) => {
    //     const newErrors = {};
    //
    //     const {email, password} = values
    //
    //     if (!email) {
    //         newErrors.email = 'This field is required';
    //     } else if (!EMAIL_REGEX.test(email)) {
    //         newErrors.email = 'This is not a valid email';
    //     }
    //
    //     if (!password) {
    //         newErrors.password = 'This field is required';
    //     }
    //
    //     return newErrors;
    // }

    const handleSubmit = (values, {setSubmitting}) => {
        console.log(values);

        setTimeout(() => {
            setSubmitting(false);
        }, 3000)

    }

    return (
        <Formik initialValues={{
            email: 'admin@admin.ua',
            password: '',
        }}
                onSubmit={handleSubmit}
                // validate={validateForm}
            validationSchema={Yup.object({
                email: Yup.string().email('Invalid email address').required('required'),
                password: Yup.string().max(20, 'Must be 20 characters or less').required('required'),
            })}
            >
            <Form noValidate>
                <h3>Uncontrolled from</h3>
                <Field
                    component={MyInput}
                    label="Email"
                    type="email"
                    placeholder="email"
                    name='email'
                    // validate={validateEmail}
                ></Field>
                <Field
                    component={MyInput}
                    label="Password"
                    type="password"
                    placeholder="password"
                    name='password'
                    // validate={validateRequired}
                ></Field>
                <div>
                    <button type='submit'>Submit</button>
                </div>
            </Form>
        </Formik>)
}

export default FormikForm1;