import {useReducer, useState} from "react";


const Reducer = () => {

    //const [count, setCount] = useState(0);

    const counterReducer = (state, action) => {
        switch (action.type) {
            case "INCREASE":
                return {...state, count: state.count + action.payload}
            default:
                return state;
        }
    }

    const init = ({count,count2}) => {
        let count3 = 0;
        if(count2 > 5) {
            count3 = 10;
        }
        return {count: count, count2: count2, count3: count3}
    }

    const [counter, counterDispatch] = useReducer(counterReducer, {count: 0, count2: 0}, init)

    return (
        <>
            <h1>
                UseReducer
            </h1>
            <button onClick={() => {
                counterDispatch(({type: "INCREASE", payload: 1}))
            }}>{counter.count}</button>
            <button onClick={() => {
                counterDispatch(({type: "INCREASE", payload: 10}))
            }}>{counter.count}</button>
        </>
    )
}

export default Reducer;