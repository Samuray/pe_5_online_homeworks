import ReactDom from "react-dom/client";
import Fetch from "./index";
import { act } from "react-dom/test-utils";
import renderer from 'react-test-renderer';

global.IS_REACT_ACT_ENVIRONMENT = true;


let container = null;
let root = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
    root = ReactDom.createRoot(container)

})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

test("Button tests", async () => {

    const fakeFetchResult = {
        count: "10"
    }
    jest.spyOn(global, "fetch").mockImplementation(() =>
        Promise.resolve({
            json: () => Promise.resolve(fakeFetchResult)
        })
    )

    await act(async ()=> {
        root.render(<Fetch />);
    })

    const h2 = container.querySelector('h2');
    expect(h2.textContent).toBe(fakeFetchResult.count)


    await act(async () => {
        h2.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })
    expect(h2.textContent).toBe(fakeFetchResult.count + 1)

    global.fetch.mockRestore();
})