import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {increment} from "../../../store/counterSlice";


const Dispatch = () => {
    const dispatch = useDispatch();

    const clicked = useSelector(state => state.counter.clicked, shallowEqual);
    const status = useSelector(state => state.counter.status, shallowEqual);



    return (
        <div><hr />
            Clicked: {clicked}<br />
            Status: {status}<br />
            <button onClick={()=>{ dispatch(increment({step: 10})) }}>Increment</button>
        </div>
    );
}

export default Dispatch;