import {useState} from "react";
import {shallowEqual, useSelector} from "react-redux";

const Component = () => {
    const [count, setCount] = useState(0);

    function countHandler() {
        setCount(count => count + 1);
    }

    return (<div><hr />
        <SubComponent count={count} countHandler={countHandler}></SubComponent>
    </div>)
}

const ComponentWithRedux = () => {
    const [count, setCount] = useState(0);

    const status = useSelector(state => state.counter.status, shallowEqual);

    function countHandler() {
        setCount(count => count + 1);
    }

    return (<div><hr /> {status} <hr />
        <SubComponent count={count} countHandler={countHandler}></SubComponent>
    </div>)
}

const SubComponent = ({count, countHandler}) => {
    return <><span role="counter">{count}</span><br /><button onClick={countHandler}>MyButton</button></>
}

export {ComponentWithRedux};
export default Component;