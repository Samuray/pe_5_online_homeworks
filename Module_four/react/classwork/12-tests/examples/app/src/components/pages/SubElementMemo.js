import {memo} from "react";


function SubElementMemo({title}) {

   // console.log('SubElementMemo rendering');

    return <h1>{title}</h1>
}

export default memo(SubElementMemo);