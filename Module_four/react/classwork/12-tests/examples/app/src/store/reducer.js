export const initState = {
    count: 0
}

export default function reducer(state = initState, action) {
    switch (action.type) {
        case 'INCREMENT':
            return {...state, count: state.count + 1}
        case 'SET_COUNT':
            return {count: action.payload}
        default:
            return state;
    }
}