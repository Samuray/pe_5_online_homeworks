import reducer, {initState} from "./reducer";

describe('Reducer works', () => {
    test('should return the initial state', ()=> {
        expect(reducer(undefined, {type: undefined})).toEqual(initState);
    })

    test('should change count', ()=> {
        expect(reducer(initState, {type: 'SET_COUNT', payload: 11})).toEqual({
            count: 11
        });
    })

    test('should increment', ()=> {
        expect(reducer({count: 100}, {type: 'INCREMENT'})).toEqual({
            count: 101
        });
    })

})