import { combineReducers, configureStore } from '@reduxjs/toolkit'
import moviesReducer from './features/filmsSlice'

const store = configureStore({
  reducer: {
    movies: moviesReducer
  }
})
// Create the root reducer separately so we can extract the RootState type
const rootReducer = combineReducers({
  movies: moviesReducer
})
export const setupStore = preloadedState => {
  return configureStore({
    reducer:  rootReducer,
    preloadedState
  })
}

export default store