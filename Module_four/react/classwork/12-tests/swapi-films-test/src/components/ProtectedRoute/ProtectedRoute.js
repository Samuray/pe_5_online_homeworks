import React from "react";
import { Navigate } from "react-router-dom";

export default function ProtectedRoute({ isAuth, Component }) {
  console.log(isAuth);
  return isAuth ? Component : <Navigate to="/login" replace />;
}
