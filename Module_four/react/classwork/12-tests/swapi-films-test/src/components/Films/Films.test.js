import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { renderWithProviders } from '../../utils/test-utils';
import { MemoryRouter } from "react-router-dom";
import { screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom'

import Films from './index';

export const handlers = [
  rest.get('https://ajax.test-danit.com/api/swapi/films', (req, res, ctx) => {
    return res(ctx.json([{
      id: 1,
      name: "A New Hope",
      },
      {
      id: 2,
      name: "The Empire Strikes Back",
      }
    ]), ctx.delay(150))
  })
]

const server = setupServer(...handlers)

// Enable API mocking before tests.
beforeAll(() => server.listen())

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())

// Disable API mocking after the tests are done.
afterAll(() => server.close())

describe("test Films component", () => {



  test("Films smoke test", async () => {
    renderWithProviders(<MemoryRouter><Films /></MemoryRouter>);

    expect(await screen.findByText(/A New Hope/i)).toBeInTheDocument()
    screen.debug();



  })


})