import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Loader from "../Loader";

export default function FullFilm() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [film, setFilm] = useState(null);
  const params = useParams();

  useEffect(() => {
    fetch("https://ajax.test-danit.com/api/swapi/films/" + params.id)
      .then((res) => res.json())
      .then((data) => {
        setIsLoaded(true);
        setFilm(data);
      });
  }, []);
  if (!isLoaded) {
    return <Loader />;
  }
  return (
    <div>
      Film #{params.id}
      <h1>{film.name}</h1>
      <p>{film.openingCrawl}</p>
    </div>
  );
}
