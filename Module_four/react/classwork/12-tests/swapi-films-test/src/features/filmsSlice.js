import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    films: {
        filmsList: [],
        status: null,
        error: ''
    },
    film: {
        filmElement: {},
        status: null,
        error: ''
    }
}

export const loadFilms = createAsyncThunk('movies/loadFilms', async (_, {rejectWithValue}) => {
    try {
        const response = await fetch('https://ajax.test-danit.com/api/swapi/films');
        return await response.json();
    } catch(error) {
        return rejectWithValue(error.message);
    }
});

export const loadFilm = createAsyncThunk('movies/loadFilm', async (id, {rejectWithValue}) => {
    try {
        const response = await fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`);
        return await response.json();
    } catch(error) {
        return rejectWithValue(error.message);
    }
});

const filmsSlice = createSlice({
    name: 'movies',
    initialState,
    reducers: {

    },
    extraReducers: builder => {
        builder
            .addCase(loadFilms.pending, (state) => {
                state.films.status = 'loading';
            })
            .addCase(loadFilms.fulfilled, (state, action) => {
                state.films.filmsList = action.payload;
                state.films.status = 'loaded';
            })
            .addCase(loadFilms.rejected, (state, action) => {
                state.films.error = action.error;
                state.films.status = 'loaded';
            })
            .addCase(loadFilm.pending, (state) => {
                state.film.status = 'loading';
            })
            .addCase(loadFilm.fulfilled, (state, action) => {
                state.film.filmElement = action.payload;
                state.film.status = 'loaded';
            })
            .addCase(loadFilm.rejected, (state, action) => {
                state.film.error = action.error;
                state.film.status = 'loaded';
            })
    }
});

export default filmsSlice.reducer;
