import React, { Component } from "react";

class Footer extends Component {
    render() {
        const { title, countMassage } = this.props;
        return (
            <footer>
                <span className='footer__title'>{title}</span>
                <span className='footer__copyright'>{new Date().getFullYear()}, FE-ONLINE</span>
                <span className="footer__email">Emails: {countMassage}</span>
            </footer>
        )
    }
}

export default Footer;
