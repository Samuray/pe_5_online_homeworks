import React, { Component } from "react";
import Post from "../Post/Post";


class Main extends Component {
    render() {
        const { emails } = this.props;
       return (
           <main>
               <div className='emails'>
                   <h1>EMAILS</h1>
                   <Post emails={emails}/>

                   {/*<div className="email__container">*/}
                   {/*    <span className="email__from">{ emails[0].from }</span>*/}
                   {/*    <h3 className="email__topic">{ emails[0].topic }</h3>*/}
                   {/*    <p className="email__body">{ emails[0].body }</p>*/}
                   {/*</div>*/}

                   {/*<div className="email__container">*/}
                   {/*    <span className="email__from">{ emails[1].from }</span>*/}
                   {/*    <h3 className="email__topic">{ emails[1].topic }</h3>*/}
                   {/*    <p className="email__body">{ emails[1].body }</p>*/}
                   {/*</div>*/}

                   {/*<div className="email__container">*/}
                   {/*    <span className="email__from">{ emails[2].from }</span>*/}
                   {/*    <h3 className="email__topic">{ emails[2].topic }</h3>*/}
                   {/*    <p className="email__body">{ emails[2].body }</p>*/}
                   {/*</div>*/}
               </div>

           </main>
       )
    }
}

export  default Main;
