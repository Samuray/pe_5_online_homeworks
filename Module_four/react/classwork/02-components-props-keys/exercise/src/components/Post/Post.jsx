import { Component } from "react";

class Post extends Component {
    render() {
        const { emails } = this.props;
       return (
           <>
            { emails.map( ({ from, topic, body, id }) => (
                <div className="email__container" key={id}>
                    <span className="email__from">{ from }</span>
                    <h3 className="email__topic">{ topic }</h3>
                    <p className="email__body">{ body }</p>
                </div>
                ))
               }
           </>
       )
    }
}
export default Post;