import React, { Component } from "react";

class Header extends Component {
  render() {
      const { title, name, avatar, countMassage } = this.props;
     return (
         <header>
             <span className='header__title'>{title}</span>

             <nav>
                 <ul>
                     <li>
                         <a href='/home'>Home</a>
                     </li>

                     <li>
                         <a href='/blog'>Blog</a>
                     </li>
                     <li>
                         <a href='/emails'>Emails: {countMassage}</a>
                     </li>
                 </ul>
             </nav>

             <div className='header__user-container'>
                 <img src={avatar} alt={name} />
                 <span>{name}</span>
             </div>
         </header>
     )
  }
}

export default Header;