const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    app.route('/delete/:id')
        .get((req, res) => {
            res.send('form')
        })
        .post(async (req, res) => {
            let query = {_id: ObjectId(req.params.id)};
            let result = null;
            try {
                result = await db.collection('posts').deleteOne(query);
            }catch (err) {
                console.log('Error: ' + err.message);
            }

            res.send('Post with id ' + req.params.id + ' was deleted');
        });
}