const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    app.route('/delete/:id')
        .get((req, res) => {
            res.send('form')
        })
        .post(async (req, res) => {
            let query = {_id: ObjectId(req.params.id)};
            const infoForUpdate = {body: req.body.body};
            let result = null;
            try {
                result = await db.collection('posts').updateOne(query, {$set: infoForUpdate}, {upsert: true});
            }catch (err) {
                console.log('Error: ' + err.message);
            }
            console.log(result);
            res.send('Post with id ' + req.params.id + ' was updated');
        });
}