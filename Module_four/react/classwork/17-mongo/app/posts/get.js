const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    app.get('/all', async (req, res) => {
        let result = null;
        let Posts = [];
        try {
            result = await db.collection('posts').find().forEach(element => {
                Posts.push(element);
            });
        } catch (err) {
            console.log('Error: ' + err.message);
        }

        res.send(JSON.stringify(Posts));
    }).get('/post/:id', async (req, res) => {

        let query = {_id: ObjectId(req.params.id)};
        let result = null;
        try {
            result = await db.collection('posts').findOne(query);
        }catch (err) {
            console.log('Error: ' + err.message);
        }

        res.send(result)

    })
}