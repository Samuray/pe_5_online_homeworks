const express = require('express');
const MongoClient  = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const db = require('./config/db');

const app = express();
app.use(bodyParser.urlencoded({extended: false}));

MongoClient.connect(db.url, {useNewUrlParser: true}, (err, client) => {
     if(err) return console.log(err.message);

     const database = client.db(db.dbName);
     require('./routes')(app, database);

     app.listen(3000, () => {
          console.log('Connected to ' + db.url + ' ' + db.dbName);
          console.log('Server running on http://localhost:3000');
     });
})