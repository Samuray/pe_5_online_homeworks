const addPost = require('./app/posts/add');
const getPost = require('./app/posts/get');
const updatePost = require('./app/posts/update');
const deletePost = require('./app/posts/delete');

module.exports = function (app, db) {
    addPost(app,db);
    getPost(app,db);
    updatePost(app,db);
    deletePost(app,db);
}