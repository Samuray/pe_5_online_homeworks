```js

const emails: [
     { 
        id: 1, 
        from: 'The Postman Team', 
        topic: 'Announcing Postman’s New Plans and Pricing', 
        body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.'
    },
    {
        id: 2,
        from: 'Djinni',
        topic: 'JavaScript вакансії за вашим профілем',
        body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.'
    },
    {
        id: 3,
        from: 'Natalia Pemchyshyn',
        topic: 'JavaScript Developer at GlobalNogic',
        body: 'Привіт!Я рекрутер компанії GlobalNogic. Ми зараз у пошуках Lead/Senior React Developer (фултайм/ремоут) на фармацевтичний проект. Розробляємо девайс, який моніторить стан здоров\'я свійських тварин.Стек: React, Ionic, HTML, CSS, RxJs, Java. Поспілкуємось?'
    },
]


```

## `Footer`
1. Создать компонент `Footer`
2. Стилизовать его (стили можно взять с заданий предыдущего модуля)
3. В хедере должны быть title, информация о юзере, поле с количеством новых email;  

![](assets/Header.jpg)

## `Footer`
1. Создать компонент `Footer`
2. Стилизовать его (стили можно взять с заданий предыдущего модуля)
3. В футере должны быть title, поле с количеством новых email;  

![](assets/Footer.jpg)

## `Main`
1. Создать компонент `Main`
2. Выводить в нем список писем.
3. У письма должно быть состояние "прочитано/не прочитано". Если письмо не прочитано, то оно отображается в счетчиках в хедере и в футере. По клику на письмо оно должно становиться прочитанным. 
4. Стили прочитанного и непрочитанного письма должны отличаться.

## Доп. задание
1. Реализовать функционал удаления письма.
2. Добавить модальное окно с подтверждением.


```html

<div class="email__container">
    <span class="email__from">John</span>
    <h3 class="email__topic">Some topic</h3>
    <p class="email__body">Email 1 Lorem ipsum dolor sit amet</p>
</div>

```

```scss

.email__container {
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 5px 20px;
  margin: 20px 0;
  cursor: pointer;
  border-radius: 5px;
  
  &:hover {
    box-shadow: 4px 4px 8px 0 rgba(34, 60, 80, 0.2);
  }
  
  span {
    font-size: 14px;
    color: grey;
  }
  
  h3 {
    font-size: 24px;
    margin: 10px 0 30px;
  }
  
  p {
    font-size: 16px;
    color: #20232a;
  }
}

```
