import React from "react";
import styles from './Header.module.scss'
import user from './img/user.png'

 const Header = (props) => {
        const { age, name, title, countMessage } = props
        return(
            <header className={styles.header}>
                    <span className={styles.title}>{title}</span>
                <div className={styles.wrapper}>
                    <span className={styles.emails}>Emails: {countMessage}</span>
                    <div className={styles.profile}>
                        <img width='35' src={user} alt="user"/>
                        <span>{name}</span>
                        <span>{age}</span>
                    </div>
                </div>
            </header>
        );
}

export default Header
