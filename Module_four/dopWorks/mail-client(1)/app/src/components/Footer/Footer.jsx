import React from "react";
import styles from './Footer.module.scss'

const Footer = (props) => {
        const { title, countMessage } = props;
        return(
            <footer className={styles.footer}>
                <span className={styles.title}>{title}</span>
                <span>Emails: {countMessage}</span>
            </footer>
        );

}

export default Footer
