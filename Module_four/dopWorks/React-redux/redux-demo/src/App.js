import { Provider } from "react-redux";
import { Route, Routes } from "react-router-dom";
import appStore from "./store/appStore";


const App = () => {
    return (

        <Provider store={appStore}>
            <Routes>

                <Route/>

            </Routes>
        </Provider>
    )

}

export  default  App;


