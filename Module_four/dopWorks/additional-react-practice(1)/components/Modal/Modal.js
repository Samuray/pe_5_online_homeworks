import React from 'react';
import styles from './Modal.module.scss';
import {Button} from "@mui/material";

const Modal= () => {
    const isOpen = true;

    if (!isOpen) return null;

    return (
         <div className={styles.root}>
             <div className={styles.background} />
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button className={styles.btn} variant="contained" color="error">X</Button>
                 </div>
                 <h2>Do you really want to delete?</h2>
                 <div className={styles.buttonContainer}>
                     <Button variant="contained" color="error">Yes</Button>
                     <Button variant="contained">No</Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
