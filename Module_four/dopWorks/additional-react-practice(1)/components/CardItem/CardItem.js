import React from 'react';
import PropTypes from 'prop-types';
import styles from './CardItem.module.scss';
import {Button} from "@mui/material";

const CardItem = () => {

    return (
        <div className={styles.card}>
            <button type="button" className={styles.likeButton}>Like</button>
            <span className={styles.title}>Шуруповерт аккумуляторный CD-37-12 </span>
            <img className={styles.itemAvatar} src="https://cdn.27.ua/499/06/50/1115728_24.jpeg" alt="Шуруповерт аккумуляторный CD-37-12 "/>
            <span className={styles.description}>Шуруповерт, набор аксессуаров (биты, сверла, держатель) 13 единиц, 1 аккумуляторная батарея, зарядное устройство, инструкция, гарантийный талон.</span>

            <div className={styles.btnContainer}>
                <Button variant="contained">Button</Button>
            </div>
        </div>
    )
}

CardItem.propTypes = {};
CardItem.defaultProps = {};

export default CardItem;