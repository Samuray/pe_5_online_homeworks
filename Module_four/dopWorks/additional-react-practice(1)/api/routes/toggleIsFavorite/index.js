const {items, token} = require('../../data');
const {sendBadResponse, sendGoodResponse} = require("../utils");

const toggleIsFavorite = (app) => {
    app.patch('/toggle-favorite', (req, res) => {
        const reqToken = req.header('Authorization');
        if (!reqToken) {
            sendBadResponse(res, 401, `Authorization header is required`);
            return;
        }

        const clearedReqToken = reqToken.replace(/Bearer\s/gi, '').trim();
        if (clearedReqToken !== token) {
            sendBadResponse(res, 401, `Authorization token is incorrect`);
            return;
        }

        if (!req.body?.id) {
            sendBadResponse(res, 400, `Id is required`);
            return;
        }

        const index = items.findIndex(({ id }) => id === req.body?.id)
        if (index === -1) {
            sendBadResponse(res, 404, `Item with id ${req.body?.id} not found`);
            return;
        }

        items[index].isFavorite = !items[index].isFavorite;
        sendGoodResponse(res, items[index]);
    })
}
module.exports = toggleIsFavorite;