import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import SignIn from './pages/SignIn/SignIn';
import CartPage from './pages/CartPage/CartPage';

const Routes = () => {

    return (
        <Switch>
            <Route path="/home">
                <HomePage/>
            </Route>
            <Route exact path="/sign-in">
                <SignIn/>
            </Route>
            <Route exact path="/">
                <Redirect to="/home"/>
            </Route>

            <Route exact path="/cart">
                <CartPage />
            </Route>
        </Switch>
    )
};

export default Routes;
