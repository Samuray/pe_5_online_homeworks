import React, { useEffect } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { initCardItemsCreator } from '../../store/actionsCreators/cardItemsCreator';
import CardItem from '../../components/CardItem/CardItem';

const HomePage = () => {
    const cards = useSelector( state => state.cards.cards, shallowEqual)
    const dispatch = useDispatch();
    useEffect(() => {
    dispatch(initCardItemsCreator())
    },[])
    return (
    <section>
        <h1>Главная</h1>
        {cards.length>0 && cards.map((card) => <CardItem key={card.id} {...card}/>)}
    </section>
    )
};

export default HomePage;
