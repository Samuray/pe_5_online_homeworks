import React, { useEffect } from 'react';
import CartItem from '../../components/CartItem';
import { useSelector, shallowEqual } from 'react-redux';


const CartPage = () => {
    const cartItems = useSelector( state => state.cartItems.cartItems, shallowEqual)

    return (
    <section>
        <h1>Корзина</h1>
        {cartItems.length>0 && cartItems.map((cartItems) =>  <CartItem {...cartItems}/>)}
    </section>
    )
};

export default CartPage;
