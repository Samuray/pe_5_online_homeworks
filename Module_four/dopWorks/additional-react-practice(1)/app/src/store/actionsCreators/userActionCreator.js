import { USER_LOG_IN, USER_LOG_OUT } from "../actions/userActions";

export const userLogInActionCreator = ({ user, token }) => ({
    type: USER_LOG_IN,
    payload: { user, token } 
})

export const userLogOutActionCreator = () => ({
    type: USER_LOG_OUT,
})
