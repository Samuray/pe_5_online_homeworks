import { INIT_CARDS, IS_FAVORITE  } from "../actions/cardItemsActions";
import instanse from "../../api";


export const initCardItemsCreator = () => async (dispatch) => {
    const {data} = await instanse.get('/items');
    dispatch({type:INIT_CARDS, payload:data.data })

}
export const setIsFavoriteCreator = (id) => async (dispatch) => {
    const {data} = await instanse.patch('/toggle-favorite', {
        id
    });
    console.log(data);
    dispatch({type:IS_FAVORITE, payload:data.data })
}


