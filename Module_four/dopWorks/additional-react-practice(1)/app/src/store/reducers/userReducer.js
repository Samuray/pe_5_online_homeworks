import { getFromLS, removeFromLS, saveToLS } from "../../utils/localStorage"
import { USER_LOG_IN, USER_LOG_OUT } from "../actions/userActions"

const initialValues = {
    user: getFromLS('user') || null, 
    isAuth: getFromLS('user') ? true : false, 
}

const userReducer = (state = initialValues, action) => {
    switch (action.type){
        case USER_LOG_IN: {
            const { user, token } = action.payload;

            console.log('action.payload', action.payload);
            saveToLS('user', user);
            saveToLS('token', token);
            
            return {
                user: user, 
                isAuth: true, 
            }
        }

        case USER_LOG_OUT: {
            removeFromLS('user');
            removeFromLS('token');

            return {
                user: null, 
                isAuth: false,
            }
        }

        default:
            return state
    }

}

export default userReducer;