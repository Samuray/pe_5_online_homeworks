import React from "react";
import {Form, Formik} from "formik";
import CustomInput from "../CustomInput/CostomInput";
import { Button } from "@mui/material";
import * as yup from 'yup';
import instance from '../../api';
import { useDispatch } from "react-redux";
import { userLogInActionCreator } from "../../store/actionsCreators/userActionCreator";
import { useHistory } from "react-router-dom";

const SignInForm = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const initialValues = {
        email: '', 
        password: ''
    };
    const onSubmit = async (values) => {
        console.log(values);
        const result = await instance.post('/sign-in', values);
        // ОБЯЗАТЕЛЬНАЯ ОБРАБОТКА ОШИБОК!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        dispatch(userLogInActionCreator(result.data?.data))
        history.push('/home');
        
    }

    const validationSchema = yup.object().shape({
        email: yup.string().required('Поле обязательно').email('Неправильный email'), 
        password: yup.string().required('Поле обязательно'). min(3, 'Пароль должен быть больше 3х символов')
    });

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            <Form>
                <CustomInput label="Email" type='text' name='email'/>
                <CustomInput label="Пароль" type='password' name='password'/>

                <Button type="submit">Submit</Button>
            </Form>

        </Formik>
    )

}

export default SignInForm