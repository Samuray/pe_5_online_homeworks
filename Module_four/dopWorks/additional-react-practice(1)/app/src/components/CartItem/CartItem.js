import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import {Button} from "@mui/material";
import { useDispatch } from 'react-redux';
import { setIsOpenModal, setModalParams } from '../../store/actionsCreators/modalActionCreator';

const CartItem = (props) => {
    const {title, img, count, id} = props;
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(setModalParams({ title, id }));
        dispatch(setIsOpenModal(true));
    }

    return (
        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={title}/>
                </div>
                <span className={styles.title}>{title}</span>
            </div>


            <span className={styles.quantity}>{count}</span>

            <div className={styles.btnContainer}>
                <Button className={styles.btn} variant="contained">-</Button>
                <Button className={styles.btn} variant="contained">+</Button>
                <Button onClick={handleDelete} color="error" className={styles.btn} variant="contained">DEL</Button>
            </div>

        </div>
    )
}

CartItem.propTypes = {};
CartItem.defaultProps = {};

export default CartItem;