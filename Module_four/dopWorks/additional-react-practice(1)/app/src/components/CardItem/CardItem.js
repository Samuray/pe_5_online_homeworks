import React from 'react';
import PropTypes from 'prop-types';
import styles from './CardItem.module.scss';
import {Button} from "@mui/material";
import { ReactComponent as OutlineHeart } from "../../assets/svg/heart-outline.svg";
import { ReactComponent as RedHeart } from "../../assets/svg/heart.svg";
import {setIsFavoriteCreator} from "../../store/actionsCreators/cardItemsCreator";
import { useDispatch } from 'react-redux';
import { addCartItemAC } from '../../store/actionsCreators/cartActionCreator';

const CardItem = (props) => {
const {title, img, description, isFavorite, id} = props;
const dispatch = useDispatch();


    return (
        <div className={styles.card}>
            <button onClick={() => dispatch(setIsFavoriteCreator(id))} type="button" className={styles.likeButton}>{isFavorite ? <RedHeart/> : <OutlineHeart/>}</button>
            <span className={styles.title}>{title}</span>
            <img className={styles.itemAvatar} src={img} alt={title}/>
            <span className={styles.description}>{description}</span>

            <div className={styles.btnContainer}>
                <Button variant="contained" onClick={() => {
                    dispatch(addCartItemAC({ title, img, id }))
                }}>Button</Button>
            </div>
        </div>
    )
}

CardItem.propTypes = {};
CardItem.defaultProps = {};

export default CardItem;