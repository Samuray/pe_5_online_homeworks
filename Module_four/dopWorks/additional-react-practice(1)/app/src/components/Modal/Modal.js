import React from 'react';
import styles from './Modal.module.scss';
import {Button} from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import { setIsOpenModal } from '../../store/actionsCreators/modalActionCreator';
import { removeCartItemAC } from '../../store/actionsCreators/cartActionCreator';

const Modal= () => {
    const {isOpen, id, title} = useSelector(state => state.modal);
    const dispatch = useDispatch();
    const closeModal = () => dispatch(setIsOpenModal(false));

    if (!isOpen) return null;

    return (
         <div className={styles.root}>
             <div className={styles.background} onClick={closeModal}/>
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button onClick={closeModal} className={styles.btn} variant="contained" color="error">X</Button>
                 </div>
                 <h2>Do you really want to delete {title}?</h2>
                 <div className={styles.buttonContainer}>
                     <Button onClick={() => {
                         dispatch(removeCartItemAC(id));
                     }} variant="contained" color="error">Yes</Button>
                     <Button onClick={closeModal} variant="contained">No</Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
