import React from 'react';
import styles from './Header.module.scss';
import { ReactComponent as SignInSVG } from '../../assets/svg/account-arrow-left.svg';
import { ReactComponent as CartSVG } from '../../assets/svg/cart-outline.svg';
import { ReactComponent as SignOutSVG } from '../../assets/svg/account-arrow-right.svg';
import { Link } from 'react-router-dom';
import {useSelector, shallowEqual, useDispatch} from 'react-redux'
import {userLogOutActionCreator} from '../../store/actionsCreators/userActionCreator'

const Header = () => {

    const {user, isAuth} = useSelector(state => state.user, shallowEqual)
    const dispatch = useDispatch()

    return (
        <header className={styles.root}>
            
            {/* Left container */}
            <div>
                {isAuth &&<span>Something</span>}
            </div>

            {/* Centered navigation */}
            <nav>
                <ul>
                    <li>
                        <Link  to="/home">Главная</Link>
                    </li>
                    {isAuth &&
                     <li>
                        <Link  to="">Избранное</Link>
                    </li>}
                </ul>
            </nav>

            {/* Right container */}
            <ul>
            {isAuth && <li>
                    <Link to="/cart"><CartSVG/></Link>
                </li> }

                <li>
                    {isAuth 
                    ? <a onClick={() => dispatch(userLogOutActionCreator())}><SignOutSVG/></a>
                    : <Link to="/sign-in"><SignInSVG /></Link>}
                </li>
            </ul>
        </header>
    );
};

export default Header;
