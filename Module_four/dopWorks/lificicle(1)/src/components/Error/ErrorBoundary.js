import { Component } from "react";
import SomethingWentWrong from "../SomethingWentWrong";


class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, errorInfo) {
        console.log(error)
    }

    static getDerivedStateFromError(error) {
        // Обновить состояние с тем, чтобы следующий рендер показал запасной UI.

        return { hasError: true };
    }

        render() {
        const { hasError } = this.state;
        if (hasError) {
            // Можно отрендерить запасной UI произвольного вида
            return (<SomethingWentWrong/>);
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
