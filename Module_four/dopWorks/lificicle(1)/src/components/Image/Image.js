import {PureComponent} from "react";
import getNameWithTime from "../../utils/getNameWithTime";

class Image extends PureComponent {

    constructor() {
        getNameWithTime('Image', 'CONSTRUCTOR');
        super();
    }

    componentDidMount() {
        getNameWithTime('Image','DID MOUNT');
        window.addEventListener('scroll', this.scroll);
    }

    componentDidUpdate() {
        getNameWithTime('Image','DID UPDATE');
    }

    componentWillUnmount() {
        getNameWithTime('Image','WILL UNMOUNT');
        window.removeEventListener('scroll', this.scroll);
    }

    scroll =() => {
        console.log('SCROLL')
    }

    render(){
        getNameWithTime('Image','RENDER');
        const { src } = this.props;


        return (
            <img style={{ backgroundColor: 'lightgray' }} src={src} alt="Some alt" width={400} height={200} />
        );
    }
}

export default Image;
