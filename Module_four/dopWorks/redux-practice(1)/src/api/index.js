import axios from "axios";

const instance = axios.create({ baseURL: process.env.REACT_APP_BASE_API_URL });

instance.interceptors.response.use((response) => {
    return {
        status: response.status,
        data: response.data
    }
}, ({ response }) => {
    return {
        status: response?.status,
        data: response?.data,
        response
    }
})

instance.interceptors.request.use(
  config => {
      if (localStorage.getItem('authToken')) {
          config.headers['Authorization'] = `Bearer ${localStorage.getItem('authToken')}`
      }

      return config;
  }
);

export default instance;
