import React from "react";
import CartContainer from "../../components/Cart/CartContainer";

const CartPage = ({ cartItems }) => {

    return (
        <section>
            <h1>CART</h1>
            <CartContainer cartItems={cartItems} />
        </section>
    )
}

export default CartPage;