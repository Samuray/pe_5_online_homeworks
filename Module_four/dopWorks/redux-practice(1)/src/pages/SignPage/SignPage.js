import React from "react";
import SignInForm from "../../components/SignInForm";

function SignPage() {

    return (
        <section>
            <SignInForm />
        </section>
    )
}

export default SignPage;