import {HEADER_FORCE_UPDATE} from "../actions";

const initialState = {
    headerForceUpdater: false,
}

const cartReducer = (state = initialState , {type, payload}) => {
    switch (type) {
        case HEADER_FORCE_UPDATE:
            return {...state, headerForceUpdater: !state.headerForceUpdater};

        default: { return state}


    }
}

export default cartReducer