import {combineReducers} from "redux";
import cardReducer from "./cardReducer";
import cartReducer from "./cartReducer";
import forceUpdateReducer from "./forceUpdateReducer";

const reducer = combineReducers({
    card: cardReducer,
    cart: cartReducer,
    forceUpdater: forceUpdateReducer,
})
export default reducer
