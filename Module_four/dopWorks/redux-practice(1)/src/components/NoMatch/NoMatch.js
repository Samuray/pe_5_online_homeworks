import React from "react";
import {Button} from "@mui/material";
import {ReactComponent as ErrorPage} from "../../assets/svg/404.svg";
import {useHistory} from "react-router-dom";

const NoMatch = () => {
    const history = useHistory();
    return (
        <section>
            <h1>Sorry, we weren’t able to find the page you are looking for.</h1>
            <ErrorPage style={{width: "50%", height: "50%"}}/>
            <Button variant='contained' onClick={()=>history.replace("/")}>Go Home</Button>
        </section>
    )
}

export default NoMatch;