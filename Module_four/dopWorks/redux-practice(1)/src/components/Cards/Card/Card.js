import React from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import {Button} from "@mui/material";
import { ReactComponent as DeleteIcon } from '../../../assets/svg/delete.svg'
import { ReactComponent as AddToFavIcon } from '../../../assets/svg/star-plus.svg'
import { ReactComponent as DeleteFromFavIcon } from '../../../assets/svg/star-remove.svg'
import instance from "../../../api";
import {addItemCart, toggleIsFavourite} from "../../../store/actionCreators";
import {useDispatch} from "react-redux";

const Card = (props) => {
    const { title, id, imageUrl, caption, isFavorite, setCartItems, setNotificationConfig} = props;
    const dispatch = useDispatch();

    const handleDelete = async () => {
        const { status } = await instance.delete(`/${id}`);
        if (status !== 200) {
            setNotificationConfig(prev => ({
                ...prev,
                message: `Ooops... We can't delete ${title} now.`,
                isOpen: true,
                isError: true,
            }))
        }

        if (status === 200) {
            setNotificationConfig(prev => ({
                ...prev,
                message: `${title} was successfully deleted`,
                isOpen: true,
                isError: false,
            }))
        }
    }

    const handleToggleFav = () => {
        const body = {
            id,
            title,
            imageUrl,
            caption,
            isFavorite: !isFavorite,
        }
        dispatch(toggleIsFavourite(id, body));

    }

    const handleAddToCard = () => {
        dispatch(addItemCart({
            id,
            title,
            imageUrl,
            caption,
        }))
    }

    return (
        <div className={styles.root}>
            <div className={styles.btnWrapper}>
                <Button onClick={handleToggleFav} className={styles.favButton} variant={isFavorite ? 'text' : 'contained'}>{isFavorite ? <DeleteFromFavIcon /> : <AddToFavIcon />}</Button>
                <Button onClick={handleDelete} className={styles.favButton} color='error' variant='contained'><DeleteIcon /></Button>
            </div>

            <span className={styles.title}>{title}</span>
            <div className={styles.imgContainer} style={{ backgroundImage: `url("${imageUrl}")` }}/>
            <span className={styles.caption}>{caption}</span>
            <Button onClick={handleAddToCard} variant='contained'>Add to Cart</Button>
        </div>
    )
}

Card.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number.isRequired,
    imageUrl: PropTypes.string,
    caption: PropTypes.string,
    isFavorite: PropTypes.bool,
};

Card.defaultProps = {
    title: '',
    imageUrl: '',
    caption: '',
    isFavorite: false,
};

export default Card;