import React, {useState} from "react";
import styles from './SignInForm.module.scss'
import { TextField, Button } from "@mui/material";
import instance from "../../api";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {headerForceUpdate} from "../../store/actionCreators";


const SignInForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
           const { status, data } = await instance.post('login', {email, password})
            if (status === 200) {
                localStorage.setItem('authToken', data);
                dispatch(headerForceUpdate())
                history.push('/');
                return;
            }

            if (status === 401) {
                setError('Wrong email or password')
                return;
            }

            setError('Something went wrong.')


        } catch (error) {
            console.error(error)
        }

    }

    const handleChange = (value, name) => {
        if (error) setError('');
        const valuesMap = {
            email: () => setEmail(value),
            password: () => setPassword(value),
        }

        valuesMap[name]?.();
    }


    return (
        <form onSubmit={handleSubmit} className={styles.form}>
            <TextField
                type="text"
                name="email"
                placeholder="Name"
                value={email}
                className={styles.input}
                onChange={({ target: { value, name } }) => handleChange(value, name)}
            />
            <TextField
                type="password"
                name="password"
                placeholder="Password"
                value={password}
                className={styles.input}
                onChange={({ target: { value, name } }) => handleChange(value, name)}
            />
            <p className={styles.error}>{error}</p>
            <Button variant='contained' type="submit">Log In</Button>
        </form>
    )
}

export default SignInForm;