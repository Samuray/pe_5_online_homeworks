import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import {Button} from "@mui/material";
import { ReactComponent as DeleteIcon } from '../../../assets/svg/delete.svg'


const CartItem = (props) => {
    const { title, id, imageUrl, } = props;

    return (
        <div className={styles.root}>
            <div className={styles.wrapper}>
                <div className={styles.imgContainer} style={{ backgroundImage: `url("${imageUrl}")` }}/>
                <span className={styles.title}>{title}</span>
            </div>

            <Button onClick={() => {}} className={styles.favButton} color='error' variant='contained'><DeleteIcon /></Button>
        </div>
    )
}

CartItem.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number.isRequired,
    imageUrl: PropTypes.string,
};

CartItem.defaultProps = {
    title: '',
    imageUrl: '',
};

export default CartItem;