import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import classNames from "classnames";
import styles from './NotificationScreen.module.scss';

const NotificationScreen = (props) => {
    const {isOpen, setIsOpen, message, isError} = props;

    useEffect(() => {
        if (isOpen) {
            setTimeout(() => setIsOpen(prev => ({ ...prev, isOpen: false })), 2500);
        }
    }, [isOpen])

    return (
        <div className={classNames(styles.notificationRoot, { [styles.showed]: isOpen, [styles.error]: isError })}>
            <p>{message}</p>
        </div>
    )
}

NotificationScreen.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    setIsOpen: PropTypes.func.isRequired,
    message: PropTypes.string,
    isError: PropTypes.bool,
};

NotificationScreen.defaultProps = {
    message: '',
    isError: false
};

export default NotificationScreen;