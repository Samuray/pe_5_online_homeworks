import React, { useState } from 'react';
import './App.scss';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./components/Header/Header";
import Routes from "./Routes";
import NotificationScreen from "./components/NotificationScreen";
import { Provider } from "react-redux";
import store from "./store";

function App() {
    const [cartItems, setCartItems] = useState([]); // Стейт корзины
    const [notificationConfig, setNotificationConfig] = useState({ // Конфиг для блока нотификаций
        isOpen: false,
        message: '',
        isError: false,
    })

    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <NotificationScreen
                        setIsOpen={setNotificationConfig}
                        isOpen={notificationConfig.isOpen}
                        isError={notificationConfig.isError}
                        message={notificationConfig.message}
                    />
                    <Header/>
                    <Routes cartItems={cartItems} setCartItems={setCartItems} setNotificationConfig={setNotificationConfig}/>
                </div>
            </Router>
            </Provider>

    );
}

export default App;
