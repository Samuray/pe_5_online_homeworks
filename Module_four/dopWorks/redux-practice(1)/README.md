### API DOC: [https://ajax.test-danit.com/api-pages/cards.html](https://ajax.test-danit.com/api-pages/cards.html)

1. Организация стора и стейта карточек.
    * первичная загрузка всех карточек
    * добавление новой карточки
    * удаление карточки
    * добавить в избранное/удалить из избранного
2. Организация стора и стейта корзины.
3. Синхронизация хедера и логина.