import { Component } from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import PostsContainer from "./components/PostsContainer";


class App extends Component {

    state = {
        posts: [],
        isLoading: false,
        isError: false
    }

    async componentDidMount() {
        try {
            this.setState({
                isLoading: true,
            })

            const response = await fetch('https://ajax.test-danit.com/api/json/posts')
                .then(e => e.json())

            const hello = await fetch('./hello.json')
                .then(e => e.json());

            console.log(hello);

            this.setState({
                posts: response,
                isLoading: false,
            })
        } catch (e) {
            this.setState({
                isLoading: false,
                isError: true,
            })
        }
    }

    render(){
        const { isLoading, posts, isError } = this.state;
        return (

            <div className="App">
                <img src="./images/1.jpg" alt="sdfsd"/>
                <Header title={123} user={{ name: 'Sam', age: 26,  avatar: 'https://i.pravatar.cc/40'}} />
                <PostsContainer posts={posts} isLoading={isLoading} isError={isError} />
                <Footer title="PropTypes" year={new Date().getFullYear()} onOrderFunc={() => console.log('Order call')} />
            </div>
        );
  }
}

export default App;
