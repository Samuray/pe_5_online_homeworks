import React, { PureComponent } from 'react';
import { CircularProgress } from "@mui/material";// https://mui.com/api/circular-progress/
import PropTypes from 'prop-types';


class Preloader extends PureComponent {
    render(){
        const { color="", size=1 } = this.props;

        return <CircularProgress color={color} size={size} />;
    }
}

Preloader.PropTypes = {
    color: PropTypes.string,
    size: PropTypes.number,
}


export default Preloader;
