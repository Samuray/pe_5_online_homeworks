import React, { PureComponent } from 'react';
import Button from "../Button";
import styles from './Footer.module.scss';
import PropTypes from 'prop-types';

class Footer extends PureComponent {
    render(){
        const { title="", year=1, onOrderFunc=()=>{} } = this.props;

        return (
              <footer className={styles.root}>
                  <span>{title}</span>
                  <span>FE-30, {year}</span>
                  <Button onClick={onOrderFunc}>Order Call</Button>
              </footer>
        );
    }


}

Footer.PropTypes = {
    title: PropTypes.string,
    year: PropTypes.number,
    onOrderFunc: PropTypes.func,
}


export default Footer;
