import React from 'react';
import Header from '../../../Header';
import TabsSection from '../../sections/TabsSection';
const ShippingDetailsPage = () => {
    return (
        <>
            <div>
                <Header />
                <TabsSection />
            </div>
        </>
    );
};
export default ShippingDetailsPage;
