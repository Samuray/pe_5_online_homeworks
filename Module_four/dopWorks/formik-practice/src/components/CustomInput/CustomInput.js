import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from "@mui/material";
import { useField } from "formik";

const CustomInput = (props) => {
    const [field, meta] = useField(props);
    const { label, type } = props;

    const Error = meta.touched && meta.error;

    return (
        <>
            <TextField { ...field }
                       label={label}
                       type={type}
                       color={Error ? "error" : "primary"}/>
            <span className="error">{Error ? meta.error : ""}</span>
        </>
    )
}

CustomInput.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
};

export default CustomInput;