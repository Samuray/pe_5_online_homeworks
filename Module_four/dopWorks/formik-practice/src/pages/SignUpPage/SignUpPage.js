import React from "react";
import { Formik, Form  } from "formik";
import CustomInput from "../../components/CustomInput";
import { Button } from "@mui/material";
import * as Yup from "yup";

function SignUpPage() {

    const validationSchema = Yup.object().shape({
        name: Yup.string()
                 .required("Нужно ввести имя")
                 .min(2,"Не меньше 2-х символов")
                 .max(24, "Не больше 24-х символов")
                 .matches(/[A-Za-z/s]/, "только латиница )))"),
        age: Yup.string()
                .required("Нужно ввести возраст")
                .matches(/[0-9]/, "только цифры )))"),
        email: Yup.string()
                  .email("Не правильный формат имейл!")

    });

    const initialValues = {
        name: "",
        age: "",
        email: "",

    }

    const handleSubmit = (value, { resetForm, setSubmitting }) => {
      resetForm();
      setSubmitting(true);
      setTimeout(() => {setSubmitting(false)},3000);

    }

    return (
        <>
            <h1>Sign Up</h1>
            
            <Formik initialValues={initialValues}
                    onSubmit={handleSubmit}
                    validationSchema={validationSchema}>

                    {({ dirty }) => {
                        return (
                            <Form>
                                <CustomInput
                                    name="name"
                                    label="name"
                                    type="text"

                                />
                                <CustomInput
                                    name="age"
                                    label="age"
                                    type="text"

                                />
                                <CustomInput
                                    name="email"
                                    label="email"
                                    type="text"

                                />
                                <Button variant={"contained"} type={"submit"} disabled={!dirty}>Submit</Button>

                            </Form>
                        )
                    }}

            </Formik>
        </>
    )
}

export default SignUpPage;