import React from "react";
import {Switch, Route} from 'react-router-dom'
import HomePage from "../pages/HomePage";
import SignInPage from "../pages/SignInPage";
import SignUpPage from "../pages/SignUpPage";
import EditProfilePage from "../pages/EditProfilePage";


const Routes = () => (
    <Switch>
        <Route exact path="/"><HomePage /></Route>
        <Route exact path="/sign-in"><SignInPage /></Route>
        <Route exact path="/sign-up"><SignUpPage /></Route>
        <Route exact path="/edit"><EditProfilePage /></Route>
    </Switch>
);


export default Routes;