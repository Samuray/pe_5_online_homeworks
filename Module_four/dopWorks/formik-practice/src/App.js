import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./components/Header/Header";
import './App.scss';
import Routes from "./Routes";

const App = () => {

    return (
        <Router>
            <div className="App">
                <Header />
                <section>
                    <Routes />
                </section>
            </div>
        </Router>
    );
}

export default App;
