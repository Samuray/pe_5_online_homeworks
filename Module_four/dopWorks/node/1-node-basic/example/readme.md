# Node.js

1. Для инициализации проекта необходимо выполнить команду `npm init`;


2. Для открытия интерактивной оболочки необходимо запустить в терминале команду `node`;


3. Для выполнения JavaScript файла мы можем указать путь к нему после команды node `node app.js`.


4. [Nodemon](https://www.npmjs.com/package/nodemon)



5. [Глобальные объекты](https://nodejs.org/dist/latest-v16.x/docs/api/globals.html)
   1. `setTimeout()`
   2. `process.env`
   3. `process.argv`
   3. `process.cwd()`
   4. [URL](https://developer.mozilla.org/ru/docs/Web/API/URL)


6. Импорт и экспорт. По умолчанию в Node.js иcпользуется стандарт модулей [CommonJS](https://www.tech-wiki.online/ru/commonjs.html
   ```js
   
   const value = require('./path-to/value') // импорт
   
   module.exports = value; // экспорт
   
   ```
   
   [colors.js](https://www.npmjs.com/package/colors)


7. [File sistem](https://nodejs.org/dist/latest-v16.x/docs/api/fs.html)
   1. readFile
   2. writeFile
   3. mkdir ({ recursive: true })
   4. unlink
   5. rmdir
   6. existsSync
   

8. [Path](https://nodejs.org/dist/latest-v16.x/docs/api/path.html)
   1. resolve
   

9. [Http](https://nodejs.org/docs/latest/api/http.html)
   1. createServer
   2. listen
   3. write
   4. end
   5. statusCode
   6. setHeader
   7. writeHead
   

10. Стримы (Буфер)
    1. fs.createReadStream (.on);
    2. fs.createWriteStream (.write)
    3. readStream.pipe(writeStream)

 