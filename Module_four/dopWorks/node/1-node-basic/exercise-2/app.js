import http from "http";
import fs from 'fs';

const PORT = 3000;

const request = (url, res, contentType = 'text/html') => {
    fs.readFile(url, (err, data) => {
        if(err) {
            console.log(err);
            res.statusCode = 404;
            res.end();
        }
        res.writeHead('200', {"Content-Type": contentType})
        res.write(data);
        res.end();
    });
}

const server = http.createServer((req, res) => {

    switch (req.url) {
        case '/':
            request('./htmlTemplates/home.html', res);
        break;

        case '/styles.css' :
            request('./htmlTemplates/styles.css', res, 'text/css');
            break;

        case '/blog' :
            request('./htmlTemplates/blog.html', res);
            break;

        case '/contacts' :
            request('./htmlTemplates/contacts.html', res);
            break;

        case '/home':
            res.writeHead(301,
                {Location: '/'}
            ).end();
        break;

        default: res.write('<h1>Page not found</h1>')
            res.statusCode = 404;
        res.end();
    }

});

server.listen(PORT);