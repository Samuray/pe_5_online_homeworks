Создайте скрипт, который при вызове будет считывать путь и имя файла и создавать в указанном пути файл с указанным именем и с расширением .js в который будет записывать шаблон кода для функционального реакт компонента.

Пример:

Вызов скрипта к терминале: `create-file src/components/Modal Modal`;
Результат: в папке src/components/Modal содается файл Modal.js c шаблонным текстом

```js
import React from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

const Modal = (props) => {
    const {} = props;
    
    return (
        <>
            
        </>
    )
}

Modal.propTypes = {};
Modal.defaultProps = {};

export default Modal;
```