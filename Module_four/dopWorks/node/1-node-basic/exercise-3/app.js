import { argv } from "process";
import fs from "fs";

console.log(argv)

const path = argv[2];
const name = argv[3];

const data =`
    import React from 'react';
    import PropTypes from 'prop-types';
    import styles from './${name}.module.scss';
    
    const ${name} = (props) => {
        const {} = props;
        
        return (
            <>
                
            </>
        )
    }
    
    ${name}.propTypes = {};
    ${name}.defaultProps = {};
    
    export default ${name};`

const ifPathExists = fs.existsSync(path);

if (!ifPathExists){
    fs.mkdir(path, {recursive: true}, (err)=>{
        err && console.log(err);
        fs.writeFile(`${path}/${name}.js`,data,(err)=>{
            err && console.log(err)});
    });
}

