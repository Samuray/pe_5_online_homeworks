import token from "../../utils/token.js";
import { sendBadResponse } from "../../utils/sendFunction.js";

const posts = (app) => {
    app.get('/posts', (req, res)=>{
        if (req.headers.authorization !== token ) {
            sendBadResponse(res, 401, 'Invalid token')
        }

        res.send("authorized")
    })
};

export default posts;