import { emailRegexp } from "../../constants/regexp/regexp.js";
import { sendBadResponse, sendGoodResponse } from "../../utils/sendFunction.js";
import users from "../../users.js";
import { v4 as uuidv4 } from 'uuid';
import token from "../../utils/token.js";

const register = (app) => {
    app.post('/registration', (req, res)=>{
       if ( !emailRegexp.test(req.body.email) ) {
           sendBadResponse(res, 400, "Invalid email");
       }

       if ( req.body.password !== req.body.repeatPassword ) {
           sendBadResponse(res, 400, "Passwords not match");
       }

       const newUser = {
           id: uuidv4(),
           email: req.body.email,
       }

       users.push({...newUser, password: req.body.password });

       sendGoodResponse(res, {...newUser, token});

    })
};

export default register;
