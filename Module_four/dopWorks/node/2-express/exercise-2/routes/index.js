import register from "./registration/register.js";
import posts from "./posts/posts.js";

const routes = (app) => {
    register(app);
    posts(app)
};

export default routes;