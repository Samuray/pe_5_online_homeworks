const products = [
	{
		name: 'banana',
		price: 100,
	},{
		name: 'orange',
		price: 200,
	},{
		name: 'lemon',
		price: 100,
	},{
		name: 'apple',
		price: 150,
	},
]

export default products;