import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
const port = 3000;

const app = express();

// app.use((req, res, next) => {
//     console.log('--------------------------------------------');
//     console.log('FROM USE');
//     console.log('PATH: ', req.path );
//     console.log('--------------------------------------------');
//     next();
// })

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cookieParser())

app.get('/',(req, res) => {
    console.log('Cookies: ', req.cookies.vqa_vid)

    res.send('<h1>HELLO WORLD</h1>');
})

app.post('/data',(req, res) => {
    console.log('PATH: ', req.path);
    console.log('BODY: ', req.body);

    if (!req.body?.name || !req.body?.age) {
        res.status(400).send({ status: 'error', message: 'Body is required' });
    }

    const newBody = {...req.body};
    newBody.age = +req.body.age + 1;



    res.send({ status: 'success', data: newBody });
})


app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})