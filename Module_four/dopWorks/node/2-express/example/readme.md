# [Express](https://expressjs.com/ru/)

### [Установка](https://expressjs.com/ru/starter/installing.html)
### [Hello World](https://expressjs.com/ru/starter/hello-world.html)
### [Роутинг](https://expressjs.com/ru/starter/basic-routing.html)
### [Статика](https://expressjs.com/ru/starter/basic-routing.html)



### [Маршрутизация подробнее](https://expressjs.com/ru/guide/routing.html)
### [Промежуточные обработчики](https://expressjs.com/ru/guide/writing-middleware.html)


### [body-parser](https://www.npmjs.com/package/body-parser) или [urlencoded](http://expressjs.com/ru/api.html#express.urlencoded)
### [cookie-parser](https://www.npmjs.com/package/cookie-parser)
### [cors](https://www.npmjs.com/package/cors)

