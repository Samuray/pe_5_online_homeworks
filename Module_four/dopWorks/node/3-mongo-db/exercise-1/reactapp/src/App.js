import React from 'react';
import './App.scss';
import { BrowserRouter as Router } from "react-router-dom";
import Header from "./components/Header/Header";
import AppRoutes from './Routes'


function App() {
    return (
        <Router>
            <div className="App">
                <Header/>
                <AppRoutes />
            </div>
        </Router>
    );
}

export default App;
