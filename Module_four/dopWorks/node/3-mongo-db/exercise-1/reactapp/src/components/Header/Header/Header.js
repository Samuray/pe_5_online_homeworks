import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import HeaderNav from "../HeaderNav/HeaderNav";

const Header = () => {

    return (
        <header className={styles.root}>
            <span>POSTS</span>
            <HeaderNav/>
        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string ,
    user: PropTypes.shape(
        {
            name: PropTypes.string,
            age: PropTypes.number,
            avatar: PropTypes.string
        }
    )
}

Header.defaultProps = {
    title: "Hello",
    user: {
        name:"Marina",
        age: 25,
        avatar: ""
    }
}

export default Header;
