import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import styles from './PostContainer.module.scss';
import PostItem from '../PostItem';

const PostContainer = () => {

	const [posts, setPosts] = useState([]);
	useEffect(() => {
		try{
			(async () => {
				const data = await fetch(process.env.REACT_APP_API_URL).then((res) => res.json());
				setPosts(data);
			})();
		} catch (err) {
			console.error(err);
		}


	}, [])

    return (
        <div className={styles.root}>
					{posts.length && posts.map(({ _id: id, title, text }) => <PostItem key={id} id={id} text={text} title={title} setPosts={setPosts} />)}
        </div>
    )
}

PostContainer.propTypes = {
	posts: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string,
		text: PropTypes.string,
		title: PropTypes.string,
	}))
};
PostContainer.defaultProps = {
	posts: null,
};

export default PostContainer;