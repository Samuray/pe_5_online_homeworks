import React from 'react'
import PostForm from '../../components/PostForm'

const AddPostPage = () => {
	return (
		<section>
			<h1>ADD POST</h1>
			<PostForm />
		</section>
	)
}

export default AddPostPage