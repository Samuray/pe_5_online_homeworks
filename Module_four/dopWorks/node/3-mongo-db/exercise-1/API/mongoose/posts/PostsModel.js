import PostsSchema from "./PostsSchema.js";
import mongoose from "mongoose";

const PostsModel = mongoose.model('posts', PostsSchema);

export default PostsModel;