import PostsModel from "../../mongoose/posts/PostsModel.js";

const addNewPost = (app) => {
    app.post('/posts', async (req, res) => {
      if (!req.body.title || !req.body.text) {
          res.status(400).send({
              messageError: "title and text are required"
          })
      }

      const { title, text } = req.body;

      const newPost = new PostsModel({ title, text });
      const data = await newPost.save();
      res.status(200).send(data)
    })
};

export default addNewPost;