import PostsModel from "../../mongoose/posts/PostsModel.js";

const editPost = (app) => {
    app.put('/posts', async (req, res) => {
        if (!req.body.title || !req.body.text || !req.body.id) {
            res.status(400).send({
                messageError: "title and text are required or id missing"
            })
        }

        const {title, text, id} = req.body;

        try {
            const data = await PostsModel.findByIdAndUpdate(id, {title, text});

            const postData = await PostsModel.find();
            res.status(200).send(postData);
        } catch (error) {
            console.error(error);
            res.status(500).send({ messageError: "Something went wrong" })
        }
    })
};

export default editPost;