import PostsModel from "../../mongoose/posts/PostsModel.js";

const getPosts = (app) => {
    app.get('/posts', (req, res) => {
        PostsModel.find((err, data) => {
            if (err) {
                res.status(500).send(err);
            }

            res.status(200).send(data);
        })
    })
};

export default getPosts;