import getPosts from "./posts/getPosts.js";
import addNewPost from "./posts/addNewPost.js";
import deletePost from "./posts/deletePost.js";
import editPost from "./posts/editPost.js";

const routes = app => {
    getPosts(app);
    addNewPost(app);
    deletePost(app);
    editPost(app);
};

export default routes;