1. Deploy Node.js app
2. Deploy React app
3. [eslint config airbnb](https://medium.com/@pppped/extend-create-react-app-with-airbnbs-eslint-config-prettier-flow-and-react-testing-library-96627e9a9672)
4. [stylelint](https://stylelint.io/user-guide/get-started)
5. [husky](https://typicode.github.io/husky/#/?id=manual)
6. git and MR
7. DRY, YAGNI, KISS

## Почитать
1. [Стив Круг, "Не заставляйте меня думать"](https://www.livelib.ru/book/1002113099-ne-zastavlyajte-menya-dumat-krug-stiv)
2. [Роберт Мартин, "Чистый код"](https://www.livelib.ru/book/1002868901-chistyj-kod-sozdanie-analiz-i-refaktoring-robert-martin)
3. [Николас Закас, "JavaScript для профессиональных веб-разработчиков"](https://www.livelib.ru/book/1001243229-javascript-dlya-professionalnyh-vebrazrabotchikov-nikolas-zakas)
4. [Кайл Симпсон, "{Вы не знаете JS} Асинхронная обработка и оптимизация"](https://www.livelib.ru/book/1003163385-vy-ne-znaete-js-asinhronnaya-obrabotka-i-optimizatsiya-kajl-simpson)

