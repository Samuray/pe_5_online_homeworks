import styles from './App.module.scss';
import Header from "./components/Header";
import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import Modal from "./components/Modal";
import { useEffect, useState } from "react";
import { Button } from "@mui/material";

function App() {

    const [items, setItems] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [cart, setCart] = useState([]);
    const [currentName, setCurrentName] = useState("");
    const [isAddModal, setIsAddModal] = useState(true);

    const openModal = (name, type = "add") => {
        if (type === "delete"){
            setIsAddModal(false);
        } else {
            setIsAddModal(true);
        }
        setIsOpen(true);
        setCurrentName(name)
    }
    const closeModal = () => {
        setIsOpen(false)
    }
    const addToCart = (currentName) => {
    setCart(current => {
        const index = current.findIndex(({ name }) => {
            return currentName === name
        })
        if (index === -1){
            return [...current, { name: currentName, count: 1 }]
        }

        const newState = [...current];
        newState[index].count = current[index].count + 1;
        return newState;
    })
        closeModal();
    }

    const deleteCart = (currentName) => {
        setCart(current => {
            const index = current.findIndex(({ name }) => {
                return currentName === name
            })
            const newState = [...current];
            newState.splice(index, 1);
            return newState;
        })
        closeModal();
    }

    useEffect(() => {
        (async () => {
           const { data } = await fetch("./items.json")
                .then(res => res.json());
           setItems( data );
        })()
    },[]);

  return (
      <BrowserRouter>
        <div className={styles.app}>
          <Header cart={cart}/>
          <section>
            <Routes items={items} openModal={openModal} cart={cart} deleteCart={deleteCart}/>
            <Modal isOpen={isOpen}
                   setIsOpen={setIsOpen}
                   title={currentName}
                   actions={
                        <>
                            <Button variant="contained" color="success" onClick={() => {

                                    if(isAddModal){
                                        addToCart(currentName);
                                    } else {
                                        deleteCart(currentName)
                                    }

                                }}>Ok</Button>
                            <Button variant="contained" color="error" onClick={closeModal}>Cancel</Button>
                        </>
                   }/>
          </section>
        </div>
      </BrowserRouter>
  );
}

export default App;
