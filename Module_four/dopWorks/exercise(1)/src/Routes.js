import { Switch, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import CartPage from "./pages/CartPage";
import FavouritePage from "./pages/FavouritePage";

const Routes = ({ items, openModal, cart }) => (
    <Switch>
        <Route exact path='/'>
            <HomePage items={items} openModal={openModal} />
        </Route>

        <Route exact path='/cart'>
            <CartPage cart={cart} openModal={openModal} />
        </Route>

        <Route exact path='/favourite'>
            <FavouritePage />
        </Route>
    </Switch>
)

export default Routes;