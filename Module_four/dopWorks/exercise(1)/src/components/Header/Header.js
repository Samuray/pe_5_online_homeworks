import React from 'react';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";

const Header= ({ cart }) => {
    console.log(cart);
        return (
              <header className={styles.root}>
                  <nav>
                      <ul>
                          <li>
                              <NavLink exact activeClassName={styles.active} to="/">Home</NavLink>
                          </li>
                          <li>
                              <NavLink activeClassName={styles.active} to="/cart">Cart: {cart.length}</NavLink>
                          </li>
                          <li>
                              <NavLink activeClassName={styles.active} to="/favourite">Favourites</NavLink>
                          </li>
                      </ul>
                  </nav>
              </header>
        );
};

export default Header;
