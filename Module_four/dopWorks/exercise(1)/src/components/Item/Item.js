import React from 'react';
import styles from './Item.module.scss';
import { Button } from "@mui/material";
import { ReactComponent as AddToFavorite } from "../../assets/star-plus.svg";
import { ReactComponent as RemoveFromFavorite } from "../../assets/star-remove.svg";

const Item = ({ id, name, color, isFavorite, openModal }) => {
    return (
        <div className={styles.item}>
            <div className={styles.favourite}>
                {!isFavorite ? <AddToFavorite /> : <RemoveFromFavorite />}
            </div>
            <div style={{ backgroundColor: `${color}` }} />
            <span>{name}</span>
            <Button onClick={() => {openModal(name)}} variant="contained">Add to card</Button>
        </div>
    )
}

export default Item;