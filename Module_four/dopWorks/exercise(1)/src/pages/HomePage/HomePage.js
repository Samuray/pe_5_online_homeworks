import React from "react";
import Item from "../../components/Item";

const HomePage = ({ items, openModal }) => {

    return (
        <div>
            <h1>HOME</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {
                    items.map(({ id, name, color, isFavorite }) => <Item key={id} id={id} name={name} color={color} isFavorite={isFavorite} openModal={openModal}/>)
                }
            </div>
        </div>
    )
}

export default HomePage;