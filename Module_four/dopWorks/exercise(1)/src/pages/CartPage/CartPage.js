import React from "react";
import CartItem from "../../components/CartItem";

const CartPage = ({ cart, openModal }) => {

    return (
        <>
            <h1>CART</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {
                    cart.map(({ name, count, color, id }) => <CartItem key={id} name={name} count={count} color={color} openModal={openModal}/>)
                }
            </div>
        </>
    )
}

export default CartPage;