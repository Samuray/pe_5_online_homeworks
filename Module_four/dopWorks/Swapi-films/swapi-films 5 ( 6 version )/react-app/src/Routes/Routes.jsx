import { Route, Routes } from "react-router-dom";
import HomePages from "../pages/HomePages/HomePages";
import FilmsPages from "../pages/FilmsPages/FilmsPages";
import FullFilmsPages from "../pages/FullFilmsPages/FullFilmsPages";
import NotFoundPages from "../pages/NotFoundPages/NotFoundPages";

const Routers = () => {
        return (
              <Routes>

                 <Route  path="/" element={<HomePages/>}/>
                 <Route path="/films/:id" element={<FullFilmsPages/>}/>
                 <Route path="/films" element={<FilmsPages/>}/>
                 <Route path="*" element={<NotFoundPages/>}/>

              </Routes>
            );
}

export default Routers;