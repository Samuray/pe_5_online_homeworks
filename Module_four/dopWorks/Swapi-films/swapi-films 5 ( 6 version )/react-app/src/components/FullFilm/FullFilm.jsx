import React from "react";
import { useEffect, useState } from "react";
import Loader from "../Loader/Loader";
import Button from "../Button/Button";
import { useParams, useNavigate } from "react-router-dom";
import {replaceBehavior} from "@testing-library/user-event/dist/keyboard/plugins";

const FullFilm = () => {
    const [isLoaded, setIsLoaded] = useState(true);
    const [film, setFilm] = useState(null);
    const navigate = useNavigate();
    const { id } = useParams();

    const goBack = () => navigate(-1);
    // const goHome = () => navigate("/", {replace: true});



    useEffect(() => {(async () => {
        await fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`)
            .then(res => res.json())
            .then( data => {
                setIsLoaded(false);
                setFilm(data);
            });
    })();
    }, [id]);

    if (isLoaded) {
        return <Loader />;
    }

    return (
        <>
            <div>
                Film : {film.name}
                <p>{film.openingCrawl}</p>
            </div>
            <Button text={"Return"} changeClicked={goBack}/>
            {/*<Button text={"Home"} changeClicked={goHome}/>*/}
        </>
    );
}
export  default  FullFilm;
