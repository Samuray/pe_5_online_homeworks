import React from "react";
import { useState } from "react";
import Detail from "../Detail/Detail";
import { Link } from "react-router-dom";

const Film = ({ name, openingCrawl, id }) => {

    const [clicked, setClicked] = useState(false);

    return <li>
        {<h2>{name}</h2>}

        {clicked
            ?
            <Detail id={id} openingCrawl={openingCrawl} />
            :
            <Link to={`/films/${id}`}>{"Детальнее"}</Link>
        }
    </li>
}

export default Film;