import React from "react";

const Button = ({ text, changeClicked }) => {
    const handlerClicked = () => {
        changeClicked()
    }
    return <button onClick={handlerClicked}>{text}</button>
}

export  default  Button;
