import React from "react";
import { Navigate } from 'react-router-dom';
import { Formik, Form } from "formik";
import CastomInput from "../CastomInput/CastomInput";
import { Button } from "@mui/material";
import * as Yup from "yup";

export default function LogIn({ isAuth, onUserLogIn }) {

    const initialValues = {
        email: "",
        password: "",
    }

    const validationSchema = Yup.object().shape({
        email: Yup.string()
                  .required("Нужно ввести email")
                  .email("Не правильный формат имейл!"),
        password: Yup.string()
                   .required("Нужно ввести пароль")
                   .min(9,"Не меньше 9-ти символов")
                   .matches(/[A-Za-z/s]/, "только латиница )))"),
    });

    return (
        isAuth
        ?
        <Navigate to="/films" replace/>
        :
            <Formik
                initialValues={initialValues}
                onSubmit={onUserLogIn}
                validationSchema={validationSchema}>

                {({ dirty }) => {
                    return (
                <Form onSubmit={onUserLogIn} style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <CastomInput
                        id="user_email"
                        name="email"
                        label="email"
                        type="text"
                    />
                    <CastomInput
                        name="password"
                        label="password"
                        type="text"
                    />
                    {/*<label><input type="email" id="user_email" placeholder="Введите имейл"  /></label><br/>*/}
                    {/*<label><input type="password" placeholder="Введите пароль"  /></label><br/>*/}
                    <Button variant={"contained"} type={"submit"} disabled={!dirty}>Submit</Button>
                </Form>
                    )
                }}
            </Formik>
    );
}