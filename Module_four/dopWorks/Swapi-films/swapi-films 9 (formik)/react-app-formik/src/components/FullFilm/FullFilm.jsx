import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Loader from "../Loader/Loader";

const FullFilm = () => {
    const [isLoaded, setIsLoaded] = useState(true);
    const [film, setFilm] = useState(null);
    const params = useParams();

    useEffect(() => {(async () => {
         await fetch(`https://ajax.test-danit.com/api/swapi/films/${params.id}`)
            .then(res => res.json())
            .then( data => {
                setIsLoaded(false);
                setFilm(data);
            });
    })();
    }, []);

    if (isLoaded) {
        return <Loader />;
    }

    return (
       <div>
            Film : {film.name}
            <p>{film.openingCrawl}</p>
       </div>
    );
}
export  default  FullFilm;