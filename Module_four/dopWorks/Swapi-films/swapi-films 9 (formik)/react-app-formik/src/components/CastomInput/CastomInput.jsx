import React from 'react';
import { TextField } from "@mui/material";

import { useField } from "formik";

const CustomInput = (props) => {
    const [field, meta] = useField(props);
    const { label, type, id } = props;

    const Error = meta.touched && meta.error;

    return (
        <>
            <TextField { ...field }
                        id={id}
                       label={label}
                       type={type}
                       color={Error ? "error" : "primary"}/>
            <span className="error">{Error ? meta.error : ""}</span>
        </>
    )
}

export default CustomInput;