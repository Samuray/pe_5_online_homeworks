import { FILMS_SET, LOADING_FILMS, FILM_SET } from "../actions";

export const setFilms = () => async (dispatch) => {
    dispatch(setLoadingFilms(true));
    const result = await fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json());
    dispatch({type: FILMS_SET, payload: result});
    dispatch(setLoadingFilms(false));
};

export const setFilm = (id) => async (dispatch) => {
    dispatch(setLoadingFilms(true));
    const res = await fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`).then(res => res.json());
    dispatch({type: FILM_SET, payload: res})
    dispatch(setLoadingFilms(false));
}

export const setLoadingFilms = (isLoading) => ({type: LOADING_FILMS, payload: isLoading });