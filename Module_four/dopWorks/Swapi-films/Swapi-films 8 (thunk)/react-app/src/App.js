import { BrowserRouter as Router } from "react-router-dom";
import Routers from "./Routes/Routes";
import {Provider} from "react-redux";
import store from "./store/store";


const App = () => {
    return (
        <Provider store={store}>
            <Router>
                <Routers/>
            </Router>
        </Provider>
        )
}

export default App;
