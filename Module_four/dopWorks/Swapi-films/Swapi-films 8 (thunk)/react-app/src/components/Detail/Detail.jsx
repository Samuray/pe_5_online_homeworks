import React from "react";

const Detail = ({ episodeId, openingCrawl }) => {
    return (
        <div>
            <h3>Episode: {episodeId}</h3>
            <p>openingCrawl {openingCrawl}</p>
        </div>
    );
}

 export default Detail;