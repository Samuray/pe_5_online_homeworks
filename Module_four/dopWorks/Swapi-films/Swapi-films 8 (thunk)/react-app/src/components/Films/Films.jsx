import React from "react";
import { useEffect } from "react";
import Film from "../Film/Film";
import Loader from "../Loader/Loader";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { setFilms } from "../../store/actionCreators";

const Films = () => {
    const { films, isLoading } = useSelector((state) => ({
        isLoading: state.isLoading,
        films: state.films,
    }),  shallowEqual);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setFilms())
    },[]);

    if (isLoading) {
        return  <Loader/>
    } else {
        return (
            <ol>
                { films.map(({ id, name, openingCrawl, episodeId }) => {
                    return <Film key={id} name={name} id={id} episodeId={episodeId} openingCrawl={openingCrawl}/>
                })}
            </ol>
        );
    }
}
export  default  Films;