import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Loader from "../Loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import {setFilm} from "../../store/actionCreators";

const FullFilm = () => {
    const dispatch = useDispatch();
    const { film, isLoading } = useSelector(state => state);
    const { id } = useParams();

    useEffect(() => {
        dispatch(setFilm(id))
    }, []);

      return   isLoading
            ?
            <Loader />
            :
            <div>
                Film : {film.name}
                <p>{film.openingCrawl}</p>
            </div>

    }

export  default  FullFilm;
