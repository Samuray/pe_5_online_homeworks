
import { Route, Routes } from "react-router-dom";
import HomePages from "../pages/HomePages/HomePages";
import FilmsPages from "../pages/FilmsPages/FilmsPages";
import FullFilmsPages from "../pages/FullFilmsPages/FullFilmsPages";

const Routers = () => {
        return (
              <Routes>

                 <Route exact path="/" element={<HomePages/>}/>
                 <Route exact path="/films" element={<FilmsPages/>}/>
                 <Route path="/films/:id" element={<FullFilmsPages/>}/>

              </Routes>
            );
}

export default Routers;