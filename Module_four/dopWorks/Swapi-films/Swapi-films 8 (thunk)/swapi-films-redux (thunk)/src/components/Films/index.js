import Loader from "../Loader";
import Film from "../Film";
import { useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setFilms } from "../../store/actionCreators";

 const Films = () => {
    const { films, isLoading } = useSelector((state) => ({
        isLoading: state.isLoading,
        films: state.films,
    }),  shallowEqual);

    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(setFilms());
    }, []);

    if (isLoading) {
        return <Loader/>;
    } else {
        return (
            <ol>
                {
                        films.map(film => <Film key={film.id} id={film.id} name={film.name}/>)
                }
            </ol>
        );
    }
}

export default Films;