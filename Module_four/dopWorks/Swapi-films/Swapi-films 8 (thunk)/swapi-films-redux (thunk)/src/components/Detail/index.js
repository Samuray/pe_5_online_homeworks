import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Loader from "../Loader";
import {useDispatch, useSelector} from "react-redux";
import  { setFilm } from "../../store/actionCreators";

export default function Detail() {
    const dispatch = useDispatch();
    const { film, isLoading } = useSelector(state => state);
    const { id } = useParams();

    useEffect(() => {
    dispatch(setFilm(id))
    }, [])
    return (
        isLoading
            ?
            <Loader />
            :
            <div>
                <p>Film: {film.name}</p>
                <p>Episode: {film.episodeId}</p>
                <p>{film.openingCrawl}</p>
            </div>
    );

}