import { FILMS_SET, LOADING_FILMS, FILM_SET } from "../actions";

const initialState = {
    films: [],
    isLoading: true,
    film:[],
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case FILMS_SET: {
            return  { ...state, films: payload }
        }
        case LOADING_FILMS: {
            return {...state, isLoading: payload }
        }
        case FILM_SET: {
            return {...state, film: payload }
        }
        default:
            return state;
    }
}

export default reducer;