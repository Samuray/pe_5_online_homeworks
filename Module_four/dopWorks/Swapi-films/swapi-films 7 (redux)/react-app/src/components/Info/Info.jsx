import { NavLink } from "react-router-dom";
import styles from "./Info.module.scss";

const Info = () => {
    return (
        <>
        <nav>
            <ul>
                <li>
                    <NavLink to="/" className={styles.a}>Home</NavLink>
                </li>
                <li>
                    <NavLink to="/films" className={styles.a}>Films</NavLink>
                </li>
            </ul>
        </nav>
        </>
    )
}
export default Info;