import { Navigate } from 'react-router-dom';

const ProtectedRoute = ({isAuth, content}) => {


    return isAuth ? content : <Navigate to="/login" replace/>;
}

export default ProtectedRoute;