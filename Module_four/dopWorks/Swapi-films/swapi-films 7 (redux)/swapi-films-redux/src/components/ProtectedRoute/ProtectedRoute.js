import React from "react";
import { Navigate } from "react-router-dom";

 const ProtectedRoute = ({ isAuth, Component }) => {
  return isAuth ? Component : <Navigate to="/login" replace />;
}
