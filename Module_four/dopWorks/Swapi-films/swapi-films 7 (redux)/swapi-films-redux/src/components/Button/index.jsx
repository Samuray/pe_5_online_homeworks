import React from "react";


 const Button = ({text, changeClicked}) =>  {
    const clickHandler = () => {
        changeClicked(true);
    }
    return <button onClick={clickHandler}>{text}</button>
}

export default Button;