import { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";

 const Header = ({ isAuth, clickHandler }) => {

    const [email, setEmail] = useState('');

    // Здесь используем хук для работы с email

    useEffect(() => {
        if (isAuth) {
            const authData = JSON.parse(localStorage.getItem('authData'));
            setEmail(authData);
        } else {
            setEmail('');
        }

    }, [isAuth])

    return (
        <>
            <header className="header">

                {
                    isAuth
                        ?
                        <div className="header__content">
                            <h2>{email}</h2>
                            <button className="header__btn" onClick={clickHandler}>Log Out</button>
                        </div>
                        :
                        <div className="header__content">
                            <h2>Login App</h2>
                        </div>

                }

                {/* <button>Log Out</button> */}

            </header>

            <Outlet />

        </>
    );
}

export default Header;