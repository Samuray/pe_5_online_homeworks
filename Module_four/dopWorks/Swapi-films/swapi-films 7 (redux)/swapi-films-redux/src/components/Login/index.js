import { Navigate } from 'react-router-dom';

 const Login = ({isAuth, onUserLogIn}) => {

    return (
        isAuth
        ?
        <Navigate to="/films" replace/>
        :
        <form onSubmit={onUserLogIn} style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
            <label><input type="email" id="user_email" placeholder="Введите имейл" required /></label><br/>
            <label><input type="password" placeholder="Введите пароль" required /></label><br/>
            <button type="submit">Авторизуватись</button>
        </form> 
    );
}

export default Login;