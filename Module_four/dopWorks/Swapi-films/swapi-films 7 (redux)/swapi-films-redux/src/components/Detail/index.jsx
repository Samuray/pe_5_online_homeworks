import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Loader from "../Loader";

 const Detail = () => {
    const [isLoading, setIsLoading] = useState(true);

    const [film, setFilm] = useState([]);

    const { id } = useParams();

    useEffect(() => {
        fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`)
            .then(res => res.json())
            .then(film => {
                setFilm(film);
                setIsLoading(false);
            })
    }, [id])
    return (
        isLoading
            ?
            <Loader />
            :
            <div>
                <p>Film: {film.name}</p>
                <p>Episode: {film.episodeId}</p>
                <p>{film.openingCrawl}</p>
            </div>
    );

}

export default Detail;