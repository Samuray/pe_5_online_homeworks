import { Link } from "react-router-dom";

 const Film = ({ name, id }) => {

    return <li>
        <span>{name}</span>
        <Link to={`/films/${id}`}>Детальнее</Link>
    </li>
}

export default Film;