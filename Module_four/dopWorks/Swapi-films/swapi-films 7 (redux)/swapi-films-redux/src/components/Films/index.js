import Loader from "../Loader";
import Film from "../Film";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { setFilms } from "./loaderSlice";

 const Films = () => {
    const [isLoading, setIsLoading] = useState(true);
    
    const films = useSelector(state => state.loader.films);
    const dispatch = useDispatch();

    useEffect(() => {
      fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(res => res.json())
        .then(films => {
          dispatch(setFilms(films))
          setIsLoading(false);
        })
    }, [dispatch])
  
    if (isLoading) {
      return <Loader />;
    }
    return ( 
    <ol>
    {films.map((film) => {
          return <Film key={film.id} id={film.id} name={film.name} />
        })}
    </ol>);
}

export default Films;