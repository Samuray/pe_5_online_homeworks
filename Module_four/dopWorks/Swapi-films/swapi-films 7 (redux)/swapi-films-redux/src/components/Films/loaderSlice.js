import { createSlice } from '@reduxjs/toolkit';

const loaderSlice = createSlice({
    name: 'films',
    initialState: {
        films: []
    },
    reducers: {
        setFilms: (state, action) => {state.films = action.payload}
    }
})

export const { setFilms } = loaderSlice.actions;
export default loaderSlice.reducer;