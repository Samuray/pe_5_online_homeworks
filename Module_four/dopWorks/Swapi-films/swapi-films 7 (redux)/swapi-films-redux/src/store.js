import { configureStore } from '@reduxjs/toolkit';
import loaderReducer from './components/Films/loaderSlice';

  configureStore ({
  reducer: {
    loader: loaderReducer
  }
})

export default configureStore;