import { useState, useEffect } from "react";
import Loader from "./components/Loader/Loader";
import Film from "./components/Film/Film";
import axios from "axios";

const App = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [list, setList] = useState([]);

    useEffect(() => {
        (async () => {
             await axios.get(`https://ajax.test-danit.com/api/swapi/films`)
                    .then (({ data }) => {
                        setList(data);
                        setIsLoading(false)
                    });
        })();
    },[]);

    if (isLoading) {
      return  <Loader/>
    }

    return (
        <ol> { list.map((filmInfo) => {
            return <Film key={filmInfo.id} name={filmInfo.name} episodeId={filmInfo.id} openingCrawl={filmInfo.openingCrawl} characters={filmInfo.characters}/>
        })} </ol>
        )
}

export default App;
