import { useState } from "react";
import Button from "../Button/Button";
import Detail from "../Detail/Detail";
import Characters from "../Characters/Characters";

 const Film = ({ name, episodeId, openingCrawl, characters }) => {

    const [clicked, setClicked] = useState(false);

    return <li>
        <h2>{name}</h2>
        {clicked
            ?
            <>
            <Detail episodeId={episodeId} openingCrawl={openingCrawl}/>
            <Characters characters={characters}/>
            </>
            :
            <Button text='Детальнее' changeClicked={setClicked} />

        }
    </li>
}

export default Film;