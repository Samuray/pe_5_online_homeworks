import React, {useEffect, useState} from "react";
import Loader from "../Loader/Loader";
import Actors from "../Actors/Actors";
import axios from "axios";

const Characters = ({ characters }) => {

    const [loading, setLoading] = useState(true);
    const [char, setChar] = useState([]);


useEffect(() => {
  const actors = [];
   characters.map(async (elem) => {
       await axios.get(elem)
                    .then(({ data }) => {
                       actors.push(data)
                       setChar(actors)
                       setLoading(false)
                   });
    });
}, [characters]);


    if (loading) {
        return  <Loader/>
    }
    console.log(char)
return (
    <>
        <ul>
            {
                char.map((data) => {
                  return  <Actors name={data.name} key={data.id}/>
            })
            }
        </ul>
    </>
)
}

export default Characters;