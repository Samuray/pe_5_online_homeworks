import React, { useState, useEffect } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import Films from "./components/Films/Films";
import Header from './components/Header';
import Detail from './components/Detail/Detail';
import NotFound from './NotFound';
import LogIn from "./components/LogIn";
import './App.css';


const App = () => {

    const [isAuth, setIsAuth] = useState(false);

    // Обработчик для формы
    const onUserLogIn = (e) => {
        e.preventDefault();
        const email = document.getElementById('user_email').value;
        setIsAuth(true);
        localStorage.setItem('authData', JSON.stringify(email));
    }

    // Обработчик при клике на логаут
    const onUserLogOut = () => {
        localStorage.removeItem('authData')
        setIsAuth(false);
    }

    // Проверка залогинен ли пользователь при запуске приложения
    useEffect(() => {
        const authData = JSON.parse(localStorage.getItem('authData'));
        if (authData) {
            setIsAuth(true);
        }
    }, []);

    return (
        <Routes>
            <Route path='/' element={<Header isAuth={isAuth} clickHandler={onUserLogOut} />}>
                <Route index element={<Navigate to='/films' replace />} />
                <Route path='films' element={<ProtectedRoute isAuth={isAuth} content={<Films />} />} />
                <Route path='films/:id' element={<ProtectedRoute isAuth={isAuth} content={<Detail />} />} />
                <Route path='login' element={<LogIn isAuth={isAuth} onUserLogIn={onUserLogIn} />} />
            </Route>
            <Route path='*' element={<NotFound />} />
        </Routes>
    )

}

export default App;
