import { Navigate, Route, Routes } from "react-router-dom";
import FilmsPages from "../pages/FilmsPages/FilmsPages";
import FullFilmsPages from "../pages/FullFilmsPages/FullFilmsPages";
import NotFoundPages from "../pages/NotFoundPages/NotFoundPages";
import HeaderPages from "../pages/HeaderPages/HeaderPages";
import LogInPages from "../pages/LogInPages/LogInPages";
import ProtectedRoute from "../components/ProtectedRoute/ProtectedRoute";
import { useEffect, useState } from "react";

const Routers = () => {

    const [isAuth, setIsAuth] = useState(false);

    // Обработчик для формы
    const onUserLogIn = (e) => {
        e.preventDefault();
        const email = document.getElementById('user_email').value;
        setIsAuth(true);
        localStorage.setItem('authData', JSON.stringify(email));
    }

    // Обработчик при клике на логаут
    const onUserLogOut = () => {
        localStorage.removeItem('authData')
        setIsAuth(false);
    }

    // Проверка залогинен ли пользователь при запуске приложения
    useEffect(() => {
        const authData = JSON.parse(localStorage.getItem('authData'));
        if (authData) {
            setIsAuth(true);
        }
    }, []);
        return (
              <Routes>

                     <Route  path="/" element={<HeaderPages isAuth={isAuth} onUserLogOut={onUserLogOut}/>}>
                         <Route index element={<Navigate to="/films" replace />}/>
                         <Route  path="films" element={<ProtectedRoute isAuth={isAuth} content={<FilmsPages />} />}/>
                         <Route path="films/:id" element={<ProtectedRoute isAuth={isAuth} content={<FullFilmsPages />} />}/>
                         <Route path='login' element={<LogInPages isAuth={isAuth} onUserLogIn={onUserLogIn} />} />
                    </Route>
                        <Route  path="*" element={<NotFoundPages/>}/>

              </Routes>
            );
}

export default Routers;