import Detail from "../Detail/Detail";
import { NavLink } from "react-router-dom";
import React from "react";
import { useState } from "react";
import Button from "../Button/Button";


const Film = ({ name, openingCrawl, id, episodeId }) => {

    const [clicked, setClicked] = useState(false);

    return <li>
        <NavLink to={`/films/${id}`}>{<h2>{name}</h2>}</NavLink>

        {clicked
            ?
            <Detail episodeId={episodeId} openingCrawl={openingCrawl} />
            :
            <Button text='Детальнее' changeClicked={setClicked} />

        }
    </li>
}

export default Film;