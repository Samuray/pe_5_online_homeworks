import React from "react";
import { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import styles from "./Header.module.scss";

const Header = ({ isAuth, onUserLogOut }) => {

    const [email, setEmail] = useState('');

    // Здесь используем хук для работы с email

    useEffect(() => {
        if (isAuth) {
            const authData = JSON.parse(localStorage.getItem('authData'));
            setEmail(authData);
        } else {
            setEmail('');
        }

    }, [isAuth])

    return (
        <>
            <header className={styles.header}>

                {
                    isAuth
                        ?
                        <div className={styles.header__content}>
                            <h2>{email}</h2>
                            <button className={styles.header__btn} onClick={onUserLogOut}>Log Out</button>
                        </div>
                        :
                        <div className={styles.header__content}>
                            <h2>Login App</h2>
                        </div>

                }

                {/* <button>Log Out</button> */}

            </header>

            <Outlet />

        </>
    );
}

export default Header;