import React from "react";
import { useState, useEffect} from "react";
import Film from "../Film/Film";
import Loader from "../Loader/Loader";

const Films = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [list, setList] = useState([]);

    useEffect(() => {
        (async () => {
            await fetch(`https://ajax.test-danit.com/api/swapi/films`)
                .then(res => res.json())
                .then (list => {
                    setList(list);
                    setIsLoading(false)
                });
        })();
    },[]);

    if (isLoading) {
        return  <Loader/>
    }


    return (
        <ol>
            { list.map(({ id, name, openingCrawl, episodeId }) => {
            return <Film key={id} name={name} id={id} episodeId={episodeId} openingCrawl={openingCrawl}/>
        })}
        </ol>
    );
}
export  default  Films;