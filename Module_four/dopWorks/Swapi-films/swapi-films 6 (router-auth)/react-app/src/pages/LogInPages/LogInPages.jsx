import React from "react";
import LogIn from "../../components/LogIn/LogIn";

const LogInPages = (isAuth, onUserLogIn) => {
    return (

        <LogIn isAuth={isAuth} onUserLogIn={onUserLogIn}/>

    );
}
export default LogInPages;