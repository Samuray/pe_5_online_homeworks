import React from "react";
import Header from "../../components/Header/Header";


const HeaderPages = ({ isAuth, onUserLogOut }) => {
    return (

        <Header isAuth={isAuth} onUserLogOut={onUserLogOut}/>

    )
}

export  default HeaderPages;