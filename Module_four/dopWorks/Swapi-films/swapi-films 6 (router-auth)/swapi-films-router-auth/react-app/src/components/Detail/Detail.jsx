import React from "react";

const Detail = ({ id, openingCrawl }) => {
    return <div>
        <h3>Episode: {id}</h3>
        <p>openingCrawl: {openingCrawl}</p>
    </div>
}

 export default Detail;