import React from "react";
import { useState } from "react";
import Button from "../Button/Button";
import Detail from "../Detail/Detail";
import { Link } from "react-router-dom";

 const Film = ({ name, openingCrawl, id }) => {

    const [clicked, setClicked] = useState(false);

    return <li>
        <Link to={`/films/${id}`}>{<h2>{name}</h2>}</Link>

        {clicked
            ?
            <Detail id={id} openingCrawl={openingCrawl} />
            :
            <Button text='Детальнее' changeClicked={setClicked} />

        }
    </li>
}

export default Film;