import { Route, Switch } from "react-router-dom";
import HomePages from "../pages/HomePages/HomePages";
import FilmsPages from "../pages/FilmsPages/FilmsPages";
import FullFilmsPages from "../pages/FullFilmsPages/FullFilmsPages";
import NotFoundPages from "../pages/NotFoundPages/NotFoundPages";

const Routes = () => {
        return (
                <Switch>
                   <Route exact path="/films/:id">
                       <FullFilmsPages/>
                   </Route>
                    <Route exact path="/films">
                        <FilmsPages/>
                    </Route>
                    <Route exact path="/" >
                        <HomePages/>
                    </Route>
                    <Route exact path="*">
                        <NotFoundPages/>
                    </Route>
                 </Switch>
            );
}

export default Routes;