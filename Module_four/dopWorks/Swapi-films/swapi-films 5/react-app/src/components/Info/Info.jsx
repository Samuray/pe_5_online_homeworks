import { NavLink } from "react-router-dom";

const Info = () => {
    return (
        <>
        <nav>
            <ul>
                <li>
                    <NavLink to="/">Home</NavLink>
                </li>
                <li>
                    <NavLink to="/films">Films</NavLink>
                </li>
            </ul>
        </nav>
        </>
    )
}
export default Info;