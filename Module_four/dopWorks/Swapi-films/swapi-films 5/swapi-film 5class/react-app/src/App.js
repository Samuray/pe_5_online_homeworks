import React from "react";
import { Route, Routes } from "react-router-dom";
import Films from "./components/Films/Films";
import FullFilm from "./components/FullFilm/FullFilm";
import NotFoundPages from "./components/NotFoundPages/NotFoundPages";

const App = () => {
    return (
        <Routes>
            <Route path="/" element={<Films />}></Route>
            <Route path="/films" element={<Films />}></Route>
            <Route path="/films/:id" element={<FullFilm />}></Route>
            <Route  path="*" element={<NotFoundPages/>}/>
        </Routes>
    );
}

export default App;
