import React from "react";
import {Link} from "react-router-dom";

const Button = ({ text }) => {
    return <Link to={"/"}>{text}</Link>
}

export  default  Button;