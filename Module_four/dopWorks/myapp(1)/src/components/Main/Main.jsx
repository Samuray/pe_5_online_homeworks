import { Component } from "react";
import styles from "./main.module.scss";

class Main extends Component {
    render() {
        const { emails, readEmail } = this.props;
        return(
            <>
                {emails.map(({ from, topic, body, id, isRead }) =>
                    <div className={`${styles.emailContainer} ${isRead ? styles.isRead : ''}`} key={id} onClick={() => readEmail(id)}>
                        <span>{from}</span>
                        <h3>{topic}</h3>
                        <p>{body}</p>
                    </div>
                )}
            </>

        )
    }
}

export  default Main;