import { Component } from "react";
import styles from "./header.module.scss";
import user from './img/user.png'


class Header extends Component {
    render() {
        const { title, countMassage, age, name } = this.props;
        return(
            <header className={styles.header}>
                <span className={styles.title}>{title}</span>
                <div className={styles.wrapper}>
                    <span className={styles.emails}>Emails: {countMassage}</span>
                    <div className={styles.profile}>
                        <img width='35' src={user} alt="user"/>
                        <span>{name}</span>
                        <span>{age}</span>
                    </div>
                </div>

        </header>
        )


    }

}

export default Header;