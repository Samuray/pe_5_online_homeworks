import { Component } from "react";
import styles from "./footer.module.scss";

class Footer extends Component {

    render() {
        const { title, countMassage } = this.props;
        return(
            <footer className={styles.footer}>
                <span className={styles.title}>{title}</span>
                <span>Emails: {countMassage}</span>
             </footer>
        )
    }
}

export default Footer;