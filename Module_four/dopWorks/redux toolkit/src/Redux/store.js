import { configureStore } from '@reduxjs/toolkit';
import  modalReducer   from "./slice/modalSlice";
import productsReducer from "./slice/productSlice";


export const store = configureStore({
  reducer: {
    modal: modalReducer,
    product: productsReducer,
  },
});