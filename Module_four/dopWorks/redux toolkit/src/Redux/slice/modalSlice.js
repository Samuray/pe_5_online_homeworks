import { createSlice } from '@reduxjs/toolkit';


const initialState = {
    isModal: false,
    idModalProduct: null,
};

const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        toggleShowModal: (state) => {
            console.log(state)
             state.isModal = !state.isModal
        },
        idModalProduct: (state,action) => {
            console.log(state)
            console.log(action)
            state.idModalProduct = action.payload
        },
    },
});

export const { toggleShowModal, idModalProduct } = modalSlice.actions;

export default modalSlice.reducer;

