import { createSlice } from '@reduxjs/toolkit';
import {
    addToCart,
    removeOneCart,
    removeProductCart,
    toggleFavorite,
} from "./operations";


const initialState = {
    isLoading: false,
    isError: null,
    product: JSON.parse(localStorage.getItem("products")) || [],
};

const productSlice = createSlice({
    name: "product",
    initialState,
    reducers: {
        isLoading: (state) => {
            state.isLoading = !state.isLoading
            state.isError = null
        },
        isError: (state, action) => {
            state.isError = action.payload
            state.isLoading = false
        },
        fetchCarts: (state, action) => {
            state.isLoading = false
            state.isError = null
            state.product = [...state.product, ...action.payload]
        },
        favorite: (state, action) => {
            const favoritesUpdate = toggleFavorite(state.product, action.payload);
            state.product = [...favoritesUpdate]
        },
        addInCart: (state, action) => {
            const addToCartUpdate = addToCart(state.product, action.payload);
            state.product = [...addToCartUpdate]
        },
        removedOneCart: (state, action) => {
            const removeOneCartUpdate = removeOneCart(state.product, action.payload);
            state.product = [...removeOneCartUpdate]
        },
        removeCart: (state, action) => {
            const removeProductCartUpdate = removeProductCart(
                state.product,
                action.payload
            );

            state.product = [...removeProductCartUpdate]
        }
    }
});

export const { isLoading, isError, favorite, addInCart, removeCart, fetchCarts, removedOneCart } = productSlice.actions;

export default productSlice.reducer;