import React from "react";
import style from "./List.module.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";
import { Button } from "../Button/Button";
import { useDispatch, useSelector } from "react-redux";
import {
  idModalProduct,
  toggleShowModal,
} from "../../Redux/slice/modalSlice";
import { addInCart, favorite } from "../../Redux/slice/productSlice";

export default function List({ productList }) {
  const dispatch = useDispatch();
  const isModal = useSelector((state) => state.modal.isModal);
  const modalProduct = useSelector((state) => state.modal.idModalProduct);

  const modalActive = (id) => {
    dispatch(idModalProduct(id));
    dispatch(toggleShowModal());
  };
  return (
    <div className={`${style.wrapper} container`}>
      {productList.map((product) => {
        return (
          <Card
            product={product}
            key={product.id}
            addToCart={(id) => dispatch(addInCart(id))}
            favorite={product.isFavourite}
            isFavorite={(id) => dispatch(favorite(id))}
            modal={modalActive}
          />
        );
      })}
      {isModal && (
        <Modal
          isOpen={modalActive}
          header={"Add to cart"}
          closeButton={true}
          textmodal={"Are you sure you want to add this item to your cart?"}
          actions={
            <div className={style.modalFooter}>
              <Button
                size={"l"}
                click={() => {
                  dispatch(addInCart(modalProduct));
                  modalActive();
                }}
                color={"blue accent-3"}
                text={"OK"}
              />
              <Button
                size={"l"}
                click={modalActive}
                color={"blue accent-3"}
                text={"Cancel"}
              />
            </div>
          }
        />
      )}
    </div>
  );
}

Card.propTypes = {
  productList: PropTypes.array,
  addToCart: PropTypes.func,
  isFavorite: PropTypes.func,
};
