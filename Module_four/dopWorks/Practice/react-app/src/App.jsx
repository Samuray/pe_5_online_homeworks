import React, { useEffect, useState } from "react";
import styles from  './App.module.scss';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import CardList from "./components/CardList/CardList";
import Preloader from "./components/Preloader/Preloader";

const App = () =>  {

  const [cards, setCards] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(null);
  const [carts, setCarts] = useState(JSON.parse(localStorage.getItem("carts")) || []);
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem("favorites")) || []);

  useEffect(() => {
    (async () => {
      await fetch("product.json")
          .then((res) => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error("Error to loads!");
            }
          })
          .then((card) => {
            setCards(   card );
            setIsLoading( false );
          })
          .catch((error) => {
              setIsError(error.message);
              setIsLoading(false);
          });
    })();
  });

   const addToCart = (code) => {
        let cards = [...carts, code];
        setCarts({ carts: cards });
        localStorage.setItem("carts", JSON.stringify(cards));
    };

   const selectFavorite = (isFavorites) => {
        setFavorites({ favorites: isFavorites });
        localStorage.setItem("favorites", JSON.stringify(isFavorites));
    };

    return(
        <div className={styles.App}>
            <header>
                <Navbar cards={carts} favorites={favorites}/>
            </header>
            <main className={styles.main}>
                {isLoading && <Preloader />}
                {isError && <div>{isError}</div>}
                {cards && !cards.length ? (
                    <div>{"Product list empty!!!"}</div>
                ) : (
                    <CardList productList={cards} favorites={favorites} addToCart={addToCart} selectFavorite={selectFavorite} />
                )}
            </main>
            <footer>
                <Footer/>
            </footer>
        </div>
    )
}

export default App;
