import React from "react";
import styles from "./CardList.module.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";

 const CardList = ({ productList, favorites, selectFavorite, addToCart }) => {
     console.log(favorites);

    return (
      <div className={`${styles.wrapper} container`}>
        {productList.map((product) => {
          return (

            <Card
              key={product.id}
              urlImg={product.imgUrl}
              alt={product.productName}
              productName={product.productName}
              code={product.vendorСode}
              color={product.color}
              price={product.price}
              infoTitle={product.productName}
              addToCart={addToCart}
              favoritesArr={favorites}
              onClickSetFavorites={selectFavorite}
            />
          );
        })}
      </div>
    );
}

CardList.propTypes = {
  productList: PropTypes.array,
};

 export default CardList;