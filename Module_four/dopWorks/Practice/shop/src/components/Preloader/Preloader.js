import React from "react";
import style from "./Preloader.module.scss";

 const Preloader = () => {
    return (
      <div className={style.preloader}>
        <div className={style.loader}> </div>
      </div>
    );

}

export default Preloader;
