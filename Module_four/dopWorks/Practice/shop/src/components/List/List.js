import React, {useState} from "react";
import style from "./List.module.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";
import  Button  from "../Button/Button";

export default function List({ productList,addToCart,isFavorite }) {
    const [modal, setModal] = useState(false);
    const [cod, setCod] = useState(0);

    const modalActive = (id) => {
        setCod(id)
        setModal(!modal)
    };
    return (
        <div className={`${style.wrapper} container`}>
            {productList.map((product) => {
                return (
                    <Card
                        product={product}
                        key={product.id}
                        addToCart={addToCart}
                        favorite={product.isFavourite}
                        isFavorite={isFavorite}
                        modal={modalActive}
                    />
                );
            })}
            {modal && (
                <Modal
                    isOpen={modalActive}
                    header={"Add to cart"}
                    closeButton={true}
                    textmodal={"Are you sure you want to add this item to your cart?"}
                    actions={
                        <div className={style.modalFooter}>
                            <Button
                                size={"l"}
                                click={() => {
                                    addToCart(cod);
                                    modalActive();
                                }}
                                color={"blue accent-3"}
                                text={"OK"}
                            />
                            <Button
                                size={"l"}
                                click={modalActive}
                                color={"blue accent-3"}
                                text={"Cancel"}
                            />
                        </div>
                    }
                />
            )}
        </div>
    );
}

Card.propTypes = {
    productList: PropTypes.array,
    addToCart: PropTypes.func,
    isFavorite: PropTypes.func,
};
