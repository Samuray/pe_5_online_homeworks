import React  from "react";
import PropTypes from "prop-types";
import style from "./Favorites.module.scss";

 const Favorites = ({ code, favoritesArr, onClickSetFavorites }) =>  {
     const isFavorites = favoritesArr.includes(code);
    return (
      <span className={style.favorite}>
        {isFavorites ? (
          <i
            className={` material-icons`}
            onClick={() => {
              onClickSetFavorites(favoritesArr.filter((n) => n !== code));
            }}> </i>
        ) : (
          <i
            className={` material-icons`}
            onClick={() => {
              onClickSetFavorites([...favoritesArr, code]);
            }}> </i>
        )}
      </span>
    );

}
Favorites.propTypes = {
  code: PropTypes.string,
  favoritesArr: PropTypes.array,
  onClickSetFavorites: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
  favoritesArr: [],
};

export default Favorites;




