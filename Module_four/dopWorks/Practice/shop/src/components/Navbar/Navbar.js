import React from "react";
import {
  NavLink
} from "react-router-dom";

import style from "./Navbar.module.scss";


//
//  const Navbar = () => {
//     return (
//       <nav>
//         <div className="nav-wrapper  blue darken-3">
//           <div className="container">
//             <a
//               href="#!"
//               className="brand-logo"
//               style={{ color: "white", fontSize: 40, fontWeight: "bold" }}
//             >
//               <i
//                 className="material-icons"
//                 style={{ color: "#ff9100", fontSize: 45 }}
//               >
//
//               </i>
//               Shop of Phones
//             </a>
//             <ul className="right hide-on-med-and-down">
//               <li>
//                 <a href="#!">
//                   <i className="material-icons left"> </i>
//                   Home
//                 </a>
//               </li>
//               <li>
//                 <a href="#!">
//                   <i className="material-icons left"> </i>
//                   Favorites
//                 </a>
//               </li>
//               <li>
//                 <a href="#!">
//                   <i className="material-icons left"> </i>
//                   Cart
//                 </a>
//               </li>
//             </ul>
//           </div>
//         </div>
//       </nav>
//     );
//
// }
//
// export default Navbar;

 const Navbar = () => {

  return (
      <nav>
        <div className="nav-wrapper  blue darken-3">
          <div className="container">
            <NavLink to='/'>
              <div
                  className="brand-logo"
                  style={{ color: "white", fontSize: 40, fontWeight: "bold" }}
              >
                <i
                    className="material-icons"
                    style={{ color: "blue", fontSize: 45 }}
                >

                </i>
                Shop of Phones
              </div>
            </NavLink>
            <ul className="right hide-on-med-and-down">
              <li>
                <NavLink to='/' className={style.selected}>
                  <i className="material-icons left"> </i>
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink to='/favorites' className={style.selected}>
                  <i className="material-icons left"> </i>
                  Favorites
                </NavLink>
              </li>
              <li>
                <NavLink to='/cart' className={style.selected}>
                  <i className="material-icons left"> </i>
                  Cart
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
  );
};

export default Navbar;


