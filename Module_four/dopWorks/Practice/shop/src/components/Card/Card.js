import React from "react";
import PropTypes from "prop-types";
import style from "./Card.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarAdd } from "../../assets/svg/star-check-outline.svg";
import { ReactComponent as StarRemove} from "../../assets/svg/star-check.svg";


const Card = ({ product, favorite, isFavorite, modal }) =>  {

    const { imgUrl, alt, productName, vendorСod, color, price, id } = product;
    return (
        <div className={`${style.card} card hoverAble`}>
          <div className="card-image waves-effect waves-block waves-light">
            <h6 className={`${style.code} grey-text text-darken-4 right`}>
              {`code: ${ vendorСod }`}
            </h6>
            <img className={`${style.img} activator`} src={ imgUrl } alt={alt} />
          </div>
          <div className={`${style.card_content} card-content`}>
            <span
              className={`${style.card_title} card-title activator grey-text text-darken-4`}
            >
              <h6 className={style.cardTitle}>{productName}</h6>
            </span>

            <h6 className={`grey-text text-darken-4`}>
              {`Color: ${color}`}
            </h6>
            <h5 className={`${style.price} grey-text text-darken-4`}>
              {`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price)}
              `}
            </h5>
            <p className={style.footer}>
              <Button
                  click={()=>modal(id)}
                color={"blue accent-3"}
                // icon={<i className="material-icons left"> </i>}
                text={"Add to cart"}
              />
              <span className={style.favorite}
                    onClick={() => isFavorite(id)}>
             {favorite ? <i className={`${style.favorite__icon} material-icons`}>
                     <StarRemove />
             </i> :
                 <i className={`${style.favorite__icon_sel} material-icons`}>
                     <StarAdd />
                 </i>}
           </span>

            </p>
          </div>
          <div className="card-reveal">
            <span className="card-title grey-text text-darken-4">
              { productName }
              <i className="material-icons right">close</i>
            </span>
            <p>
              iPhone 13 works with existing power adapters, EarPods with
              Lightning Connector, and USB‑A to Lightning cables. Because there
              are billions of those out in the world, new ones often go unused.
              So we’re removing them from the box — across the entire iPhone
              family. This reduces carbon emissions and avoids the mining and
              use of precious materials. It also shrinks the package, allowing
              more boxes per shipment and fewer shipments overall.
            </p>
          </div>
        </div>
    );
}

Card.propTypes = {
  favorite: PropTypes.func,
  isFavorite: PropTypes.func,
  modal: PropTypes.func,

};

export default Card;


