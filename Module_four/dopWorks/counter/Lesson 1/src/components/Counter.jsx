import { Component } from "react";

class Counter extends Component {
    render() {
        return (
            <span>{this.props.text}%</span>
        );
    }
}

export default Counter;