import { Component } from "react";

class Button extends Component {
    render() {
        return (
            <button onClick={this.props.clickHandler}>{this.props.sign}</button>
        );
    }
}

export default Button;