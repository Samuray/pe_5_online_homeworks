import { Component } from "react";
import Counter from './components/Counter';
import Button from "./components/Button";

class App extends Component {
    state = {
        value: 50, 
        step: 10
    }

    changeValueSub = () => {
        this.setState((prev) => {
            return {
                value: Math.max(0, prev.value - this.state.step)
            }
        })
    }

    changeValuePlus = () => {
        this.setState((prev) => {
            return {
                value: Math.min(100, prev.value + this.state.step)
            }
        })
    }
    
    render() {
        return (
            <div className="App">
                <Button sign='-' clickHandler={this.changeValueSub} />
                <Counter text={this.state.value}/>
                <Button sign='+' clickHandler={this.changeValuePlus} />
            </div>
        );
    }
}

export default App;