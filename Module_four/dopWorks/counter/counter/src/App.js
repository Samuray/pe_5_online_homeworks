import { useState } from 'react';
import Button from "./components/Button";
import './App.css';

function App() {

  const [counter, setCounter] = useState(50);

const changeClickedIncr = () => {
    return  setCounter(() => counter + 10);
}
    const changeClickedDecr = () => {
        return  setCounter(() => counter - 10);
    }

return (
    <>
        <div className={"diver"}>
              <span>{counter}</span>
              <Button text={"Incr"} changeClicked={changeClickedIncr}></Button>
              <Button text={"Decr"} changeClicked={changeClickedDecr}></Button>
        </div>
    </>


)
}

export default App;
