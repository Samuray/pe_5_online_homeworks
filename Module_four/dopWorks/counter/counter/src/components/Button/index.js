export default function Button({ text, changeClicked }) {
    const clickHandler = () => {
        changeClicked();
    }
    return <button onClick={clickHandler}>{text}</button>
}