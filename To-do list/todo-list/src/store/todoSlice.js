import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchTodos = createAsyncThunk(
    "todos/fetchTodos",
    async function (_,{rejectWithValue}) {
        try {
            const response = await fetch("https://jsonplaceholder.typicode.com/todos?_limit=15");
            const data = await response.json();
            if (!response.ok) {
                throw new Error("SERVER ERROR !!!")
            }
            return data;
        }catch(error) {
            return rejectWithValue(error.message)
        }
    }
);

export const deleteTodo = createAsyncThunk(
    "todos/deleteTodo",
    async function (id, {rejectWithValue, dispatch}) {
        try {
            const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
                method: 'DELETE'
            });
            if (!response.ok) {
                throw new Error("YOPTA ERROR !!!")
            }
            dispatch(removeTodo({id}));
        }catch (error) {
            return rejectWithValue(error.message);
        }
    }
);

export const toggleTodos = createAsyncThunk(
    "todos/toggleTodos",
    async function (id, {rejectWithValue, dispatch, getState}) {
        const todo = getState().todos.todos.find(todo => todo.id === id);
        try {
            const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
                method:"PATCH",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    completed: !todo.completed,
                })
            })
            if (!response.ok) {
                throw new Error("POLNAYA YOPTA ERROR !!!")
            }
            dispatch(toggleComplete({id}))

        } catch (error){
            return rejectWithValue(error.message);
        }
    }
);

export const addNewTodo = createAsyncThunk(
    'todos/addNewTodo',
    async function(text,{rejectWithValue,dispatch}) {
        try {
            const todo = {
                title: text,
                userId: Date.now(),
                completed: false,
            }
            const response = await fetch(`https://jsonplaceholder.typicode.com/todos`, {
                method:"POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(todo)
            })

            if (!response.ok) {
                throw new Error("WTF ERROR !!!")
            }
            const data = await response.json();
           dispatch(addTodo(data));

        } catch (error) {
            return rejectWithValue(error.message);
        }

    }

);

const setError = (state, action)=> {
    state.status = "rejected";
    state.error = action.payload;
}
const todoSlice = createSlice({
    name: 'todos',
    initialState: {
        todos: [],
        status: null,
        error: null,
    },
    reducers: {
        addTodo(state, action) {
            state.todos.push(action.payload);
        },
        toggleComplete(state, action) {
            const toggledTodo = state.todos.find(todo => todo.id === action.payload.id);
            toggledTodo.completed = !toggledTodo.completed;
        },
        removeTodo(state, action) {
            state.todos = state.todos.filter(todo => todo.id !== action.payload.id);
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchTodos.pending, (state) => {
            state.status = "loading";
            state.error = null;
        })

            .addCase(fetchTodos.fulfilled, (state, action) => {
            state.status = "resolved";
            state.todos = action.payload;
        })
            .addCase(fetchTodos.rejected,setError)
            .addCase(deleteTodo.rejected,setError)
            .addCase(toggleTodos.rejected,setError)
    }
});

 const {addTodo, toggleComplete, removeTodo} = todoSlice.actions;

export default todoSlice.reducer;