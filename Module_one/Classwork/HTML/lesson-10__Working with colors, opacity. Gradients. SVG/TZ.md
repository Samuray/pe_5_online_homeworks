1. Сделать линию в header градиеном

2. В секции 2 тень при момощи радиального градиента

3. Текст THE SECRET OF A COMPANY IS THE TEAM добавить text-shadow

4. К карточкам применить calc() и добавить box-shadow

5. К тайтлу WHAT THEY SAID ABOUT US 

   ```css
   background-color: red;
   background-image: linear-gradient(45deg, #f3ec78, #af4261);
   background-size: 100%;
   background-repeat: repeat;
   -webkit-background-clip: text;
   -webkit-text-fill-color: transparent;
   -moz-background-clip: text;
   -moz-text-fill-color: transparent;
   ```

6. К кнопке Check Our Projects применить по состоянию ховера градиент и border-image через градиент, сделать анимацию transition cubic-bezier(0.250, 0.460, 0.450, 0.940)

