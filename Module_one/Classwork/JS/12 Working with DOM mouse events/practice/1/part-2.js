/**
 * Задание 2.
 *
 * Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.
 *
 * Условия:
 * - Решить задачу грамотно.
 */


    // const  button = document.createElement("button");
    // button. textContent = "Войти";
    // document.body.append(button);
    //
    // const onBtnClick = () => {
    //     alert("Добро пожаловать!");
    // };
    // button.addEventListener("click", onBtnClick);
    //
    // const onMouseOver = function () {
    //     alert("При клике по кнопке вы войдёте в систему.");
    //     button.removeEventListener("mouseover", onMouseOver);
    // }
    // button.addEventListener("mouseover", onMouseOver);