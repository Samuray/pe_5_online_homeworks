/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */

    let btn1 = document.getElementById("decrement-btn");
    let btn2 = document.getElementById('increment-btn');
    let p = document.getElementById("counter");

    let counter = 0;
    btn2.addEventListener("click", () => {
        counter++;
        p.textContent = "Counter: " + counter;
    })
    btn1.addEventListener("click", () => {
        if(counter > 0){
            counter--;
        }
        p.textContent = "Counter: " + counter;
    })
