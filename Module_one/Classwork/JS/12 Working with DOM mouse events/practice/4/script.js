/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 *
 */

    // const input = document.getElementById("input");
    // const btn = document.getElementById('validate-btn');
    //
    // btn.addEventListener("click", () => {
    //     if( input.value.length > 5 || !input.value.includes(" ")){
    //         input.style.borderColor = "green";
    //         input.value = "";
    //     } else{
    //         input.style.borderColor = "red";
    //         input.value = "";
    //     }
    // });
