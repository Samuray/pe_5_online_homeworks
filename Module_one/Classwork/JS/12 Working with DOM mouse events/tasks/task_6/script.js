// По клику на кнопку удаления (с иконкой) - удалить соответствующий элемент
// (родительский .pane) из разметки


        const div = document.getElementById("messages-container");

        div.addEventListener("click", (evt) => {
        if(evt.target.closest(".remove-button")) {
                evt.target.closest(".pane").remove();
        }
        });

