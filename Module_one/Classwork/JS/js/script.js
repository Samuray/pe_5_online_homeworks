"use strict";
// ----------------------------------------------------- Java basics ----------------------------------------------------------------------------------
// const answer1 = +( prompt ("number1"));
// const answer2 = +( prompt ("number2"));
//
// console.log(answer1 - answer2);
// console.log(answer1 + answer2);
// console.log(answer1 * answer2);
// console.log(answer1 / answer2);
// console.log(answer1 % answer2);

// ------------------------------------------------------- Working data types ---------------------------------------------------------------------------

// //Task 1
//
// console.log(Number.MAX_VALUE);
// console.log(Number.MIN_VALUE);
// console.log(Number.MAX_SAFE_INTEGER);
// console.log(Number.MIN_SAFE_INTEGER);
// console.log(Number.NaN);

// //Task 4
//
// let number1 = prompt("Введите число");
// console.log(number1);
// let number2 = prompt("Введите второе число");
// console.log(number2);
// let number3 = prompt("Введите третье число");
// console.log(number3);
// if (isNaN(number1) || isNaN(number2) || isNaN(number3)) {
// alert("⛔️ Ошибка! Все три введённых значения должны быть числами!")
// }
//
// let sum = (+number1 + +number2 + +number3) / 3;
// console.log(sum);

// //Task 6
//
// console.log(true + false);
// console.log(8 / "2");
// console.log("number" + 5 + 1);
// console.log(5 + 1 + "number");
// console.log(null + 1);
//
// console.log(undefined + 1);

// //Task 7
//
// const userName = prompt("What your name?");
// const userLastName = prompt("What your last name?");
// const userYear = prompt(" What year were you born?");
//
// console.log(`${userName} ${userLastName} ${userYear}-го года рождения`);

// //Task 1
//
// for (let i = 0; i <=300; i++) {
//     if(i % 2 !== 0 && i % 5 !== 0){
//         console.log(i)
//     }
// }

// //Task 2
//
// let num1 = +prompt("use number");
// while (isNaN(num1) || num1 === "" || num1 === null || num1 === 0) {
//     num1 = +prompt("use number");
// }
// let num2 = +prompt(" use number again");
// while (isNaN(num2) || num2 === "" || num2 === null || num2 === 0) {
//     num2 = +prompt("use number again");
// }
//
// console.log(`« Поздравляем. Введённые вами числа: ${num1} и ${num2} ».`);

// //Task 3
//
// let userName = prompt("What your name?");
// while (userName === "") {
//     userName = prompt("What your name?");
// }
// let userLastName = prompt("What your last name?");
// while (userLastName === "") {
//     userLastName = prompt("What your name?");
// }
// let userYear = prompt(" What year were you born?");
// while (userYear < 1910 || userYear > 2022) {
//     userYear = prompt(" What year were you born?");
// }
//
// console.log(`«Добро пожаловать, родившийся в ${userYear}, ${userName} ${userLastName}.».`);

// //Task 4
//
//     function showAsked(){
//         let name;
//         let score;
//         do {
//             name = prompt("name, lastName");
//         } while (!name || name.trim().split(" ").length < 2 );
//         do{
//             score = +prompt("score");
//         } while (!score || isNaN(+score) || score < 0 || score > 100);
//         console.log(`"К студенту ${name} прикреплена оценка ${createUser(score)}"`);
//     }
//
//     function createUser(score) {
//         switch (true) {
//             case score >= 95 || score <= 100:
//                 return " A ";
//             case score >=90  || score <= 94:
//                 return " A- ";
//             case score >= 85 || score <= 89:
//                 return " B+ ";
//             case score >= 80 || score <= 84:
//                 return " B ";
//             case score >= 75 || score <= 79 :
//                 return " B- ";
//             case score >= 70 || score <= 74:
//                 return " C+ ";
//             case score >= 65 || score <= 69:
//                 return " C ";
//             case score >= 60 || score <= 64:
//                 return " C- ";
//             case score >= 55 || score <= 59:
//                 return " D+ ";
//             case score >= 50 || score <= 54:
//                 return " D ";
//             case score >= 25 || score <= 49:
//                 return " E ";
//             case score >= 0 || score <= 24:
//                 return " F ";
//         }
//     }
//     do {
//         showAsked();
//     } while (confirm("Продолжить?"));
//     alert("«✅ Работа завершена.».");

// //Task 5
//
// function getResult(){
//     if (confirm !== false) {
//         let a = +prompt("Введите число");
//         while (a === "" || isNaN(a) || a === null || a === 0) {
//             a = +prompt("Введите число");
//         }
//         let b = +prompt("Введите еще одно число");
//         while (b === "" || isNaN(b) || b === null || b === 0) {
//             b = +prompt("Введите еще одно число");
//         }
//         let operator = prompt("Введите знак операции");
//         let result = sum(a, b, operator);
//         console.log(`Над числами ${a} и ${b} была произведена операция ${operator}. Результат: ${result}.`);
//
//             }
// }
//     function sum(a, b, operator) {
//         switch (operator) {
//             case "+":
//                 return a + b;
//             case "-":
//                 return a - b;
//             case "*":
//                 return a * b;
//             case "/":
//                 return a / b;
//             case "%":
//                 return a % b;
//         }
//     }
//
// do {
//     getResult();
// } while (confirm("Continue?"));
//     alert("✅ Работа завершена.");


// //Task 1
//
//     let number = prompt("Введите число!");
//    console.log(number);
//     if ( number === "" || isNaN(number) || number === null) {
//         alert("Необходимо ввести число!");
//         number = prompt("Введите число!");
//         if (number === "" || isNaN(number) || number === null) {
//             alert("⛔️ Ошибка! Вы ввели не число.");
//         }
//     }
// console.log(number % 2 === 0 ? "Ваше число четное": "Ваше число не четное.");


// //TASK 2
//
// let name = prompt ("your name");
// switch (name) {
//     case "Mike":
//         console.log("hello Mike - ceo");
//         break;
//     case "Jane":
//         console.log("hello Jane - cto");
//         break;
//     case "Walter":
//         console.log("hello Walter - программист");
//         break;
//     case "Oliver":
//         console.log("hello Oliver- менеджер");
//         break;
//     case "John":
//         console.log("hello John - уборщик");
//         break;
//     default:
//         console.log("Пользователь не найден");
// }
//
// if (name === "Mike"){
//  console.log("hello Mike - ceo");
// }
// else if (name === 'Jane'){
//     console.log("hello Jane - cto");
// }
// else if (name === 'Walter'){
//     console.log("hello Walter - программист");
// }
// else if (name === 'Oliver'){
//     console.log("hello Oliver- менеджер");
// }
// else if (name === 'John') {
//     console.log("hello John - уборщик");
// }
// else {
//     console.log("пользователь не найден!");
// }

// //Task 3
//
// let num1 = prompt("fill a number");
// let num2 = prompt("fill a number");
// let num3 = prompt("fill a number");
//
//  if(
//      (num1 === "" || isNaN(num1) || !num1) ||
//      (num2 === "" || isNaN(num2) || !num2) ||
//      (num3 === "" || isNaN(num3) || !num3))
//  {
//      alert("⛔️ Ошибка! Одно из введённых чисел не является числом.");
//  } else {
// if (num1 > num2 && num1 > num3) {
//     console.log(num1)
//  } else if  (num2 > num3) {
//      console.log(num2)
// } else  {
//     console.log(num3)
//         }
// }

// //Task 4
//
// const coins = +prompt("Бросьте монеты");
// const drink = prompt("Какой напиток будете?");
//
// function makeCoffee (drink,price,coins) {
//     if(coins === price) {
//         console.log(`Ваш ${drink} готов. Спасибо за сумму без сдачи!`);
//     }
//     else if(coins > price) {
//         console.log(`Ваш ${drink} готов. Возьмите сдачу: ${coins - price} .`);
//     }else {
//         console.log(`"Не хватает денег. Добавьте ${price - coins}"`);
//     }
// }
// if(drink === "кофе"){
//     makeCoffee(drink,25,coins);
// }else if(drink === "капучино"){
//     makeCoffee(drink,50,coins);
// }else if(drink === "чай"){
//     makeCoffee(drink,10,coins);
// }else {
//     console.log("Напиток не найден!");
// }

// ------------------------------------------------------- Function ----------------------------------------------------------------------------------
//Task 1

// function sum (a,b) {
//     return a + b;
// }
//
// console.log(sum(5,7));

// //Task 2
//
// function getArg () {
//     console.log(arguments.length);
// }
// getArg(1,11,11,11,11);

// //Task 3
//
// function count (num1,num2) {
// if (num1 > num2) {
//     console.log("⛔️ Ошибка! Счёт невозможен.");
// }else if (num1 === num2) {
//     console.log("⛔️ Ошибка! Нечего считать.");
// }else {
//     alert("🏁 Отсчёт начат.")
//     for (let i = num1; i <= num2; i++) {
//         console.log(i);
//     }
//     alert("✅ Отсчёт завершен.");
// }
// }
// count(3,9);

// // Task 4
//
// function countAdvanced (num1,num2, num3) {
//     if (num1 > num2) {
//         console.log("⛔️ Ошибка! Счёт невозможен.");
//     }else if (num1 === num2) {
//         console.log("⛔️ Ошибка! Нечего считать.");
//     }else if (arguments.length !== 3) {
//         console.log("⛔️ Ошибка! Не хватает чисел!");
//     }else if (!Number.isInteger(num1) ||
//             !Number.isInteger (num2) ||
//             !Number.isInteger(num3) ) {
//         console.log("⛔️ Ошибка! Не допустимое число!");
//     } else {
//         alert("🏁 Отсчёт начат.")
//         for (let i = num1; i <= num2; i++) {
//             if (!(i % num3)) {
//                 console.log(i);
//             }
//         }
//         alert("✅ Отсчёт завершен.");
//     }
// }
// countAdvanced(2,10,2);

// ------------------------------------------------------- Object ------------------------------------------------------------------------------------


//Block 1

// //Task 1
//
// const user = {
//     name: "Goga",
//     surname: "Ivanov",
//     profesion: "it",
//     sayHi() {
//         console.log(`"Hello ${this.name}!"`);
//     },
//
// }
// user.sayHi();

// //Task 2
//
// const user = {
//     name: "Goga",
//     surname: "Gogashvilli",
//     profession: "it programmer",
//     sayHi() {
//         console.log(`"Hello ${this.name} ${this.surname}!"`);
//     },
//     set(propertyName, propertyValue) {
//         if(propertyName in this){
//             this[propertyName] = propertyValue;
//         }else {
//             console.log("Error!!!")
//         }
//     },
// }
// user.set("name", "Vasya");
// user.set("surname", "Vasilek");
// user.sayHi();

// //Task 3
//
// const user = {
//     name: "Goga",
//     surname: "Gogashvilli",
//     profession: "it programmer",
//     sayHi() {
//         console.log(`"Hello ${this.name} ${this.surname}!"`);
//     },
//     set(propertyName, propertyValue) {
//         if(propertyName in this){
//             this[propertyName] = propertyValue;
//         }else {
//             console.log("Error!!!")
//         }
//     },
//     setKey(key,value) {
//         if(key in this) {
//             console.log("Cвойство с таким именем уже существует.")
//         }else {
//             this[key] = value;
//         }
//     }
// }
// user.set("name", "Vasya");
// user.set("surname", "Vasilek");
// user.sayHi();
// user.setKey("age", 35);
// console.log(user);

// //Task 4
//
// const myObject = {
//     name: "Vasya",
//     age: 35,
//     profession: "meneger",
//     parents: {
//         mather: "sheff",
//         father: "boss",
//     }
// }
// function rec (obj) {
//     for(let key in obj) {
//         if( typeof obj[key] !== "object") {
//             console.log(`${key} ${obj[key]}`);
//         }else {
//             rec(obj[key]);
//             for(let keys in obj[key]) {
//                 if( typeof obj[key][keys] !== "object") {
//                     console.log(`${keys} ${obj[key][keys]}`);
//                 }
//             }
//         }
// }
//     }
//     rec(myObject);

// //Task 5
//
// const priceList = {
//     sizes: {
//         small: {
//             price: 15,
//             calories: 250,
//         },
//         medium: {
//             price: 25,
//             calories: 340,
//         },
//         large: {
//             price: 35,
//             calories: 440,
//         },
//     },
//     types: {
//         breakfast: {
//             price: 4,
//             calories: 25,
//         },
//         lunch: {
//             price: 5,
//             calories: 5,
//         },
//         dinner: {
//             price: 10,
//             calories: 50,
//         },
//     },
//     getTotalPrice(size,type){
//         return  this.sizes[size].price + this.types[type].price;
//     },
//     getTotalCalories(size,type) {
//         return  this.sizes[size].calories + this.types[type].calories;
//     }
// };
//
// console.log(`Цена:${priceList.getTotalPrice("medium","dinner")}`);
// console.log(`Колории:${priceList.getTotalCalories("medium","dinner")}`);

// function assistant(size,type) {
//     let Price = priceList.sizes[size].price + priceList.types[type].price;
//     let Calories = priceList.sizes[size].calories + priceList.types[type].calories;
//
//     return {
//         totalPrice: Price,
//         totalCalories: Calories,
//     }
// }
// console.log(assistant("small", "dinner"));

//Block 2

// //Task 1
//
// function createUser(name = null,lastName = null,profession = null) {
//     const user = {
//         name,
//          lastName,
//          profession,
//     }
//     return user;
//
// }
//
// console.log(createUser("Vasya","Petrenko", "Bodygard"));

//Block 3

//Task 1


    // const user = {
    //     name,
    //     lastName,
    //     profession,
    // }
    // const defUser = {};
    //         Object.defineProperty(defUser, {
    //             "name": {
    //                 value: user.value,
    //                 writable: false,
    //                 configurable: true,
    //             },
    //             "lastName": {
    //                 value:user.value,
    //                 writable: false,
    //                 configurable: true,
    //             },
    //             "profession": {
    //                 value: user.value,
    //                 writable: false,
    //                 configurable: true,
    //             },
    //         });

// const user = {
//     _name: "Goga",
//     _lastName:"Gogashvilli",
//     profession: "cleaner",
//     set name (value) {
//         if(value.length > 3) {
//             return this._name = value;
//         }
//     },
//     get lastName() {
//     return this._lastName;
//     }
// }
// user.name = "Irishka";
// console.log(user);

// //Task 2
//
// const myObject = {
//     name:"Goga",
//     lastName:"Gogashvilli",
//     age: 35,
// }
//
// function preventFreezeObject(obj) {
//     Object.preventExtensions(obj);
//     for (const key in obj) {
//         if(obj.hasOwnProperty(key)) {
//             Object.defineProperty(obj,key,{
//                 writable: false,
//                 configurable: false,
//             });
//         }
//     }
//     return obj;
// }
//
// console.log(preventFreezeObject(myObject));
// myObject.name = "Vasya";
// delete myObject.lastName;
// myObject.profession = "cleaner";
// // console.log(myObject);

//Block tasks

// //Task 2
//
// let name = prompt("Введите имя");
// let lastName = prompt("Введите фамилию");
//     while (name === "" && lastName === "") {
//          name = prompt("Введите имя");
//          lastName = prompt("Введите фамилию");
//     }
// let question = prompt("Хотите кушать?");
//
// const person = {
//     name,
//     lastName,
//     question,
//     eat() {
//         if (this) {
//             alert("Пойду поем");
//         }
//     },
//     go() {
//         alert("Тогда я пошел");
//     },
// }
// if(person.question === "yes"){
//     person.eat();
// }else {
//     person.go();
// }
//
// console.log(person);

// //Task 3
//
// let question = prompt("Что вы хотите узнать о студенте?");
//
// const danItStudent = {
//     name: "Grisha",
//     lastName: "Katz",
//     homeworks: "7",
//     getInfo(value){
//     // Object.keys(this).forEach(function(key) {
//     //     if(value === key || value === this[key]) {
//     //         console.log(this[key]);
//     //     }else {
//     //         console.log("Извините таких данных нету");
//     //     }
//     // });
//                if (Object.hasOwnProperty.call(this, value) ) {
//                     console.log(this[value]);
//                 }else if (this[value]) {
//                    console.log(this[value]);
//                }
//                else {
//                    console.log("Извините таких данных нету");
//                }
//     }
// }
//
// console.log(danItStudent);
// danItStudent.getInfo(question);

// //task 4
//
// const danItStudent = {
//     name: "Grisha",
//     lastName: "Katz",
//     homeworks: 7,
// }
// console.log(danItStudent);
//
// let key = prompt('Какое свойство вы хотите изменить?');
// let value = prompt("На какое значение?");
//
// function changeProperty (propertyName, propertyValue) {
//     if (Object.hasOwnProperty.call(danItStudent, propertyName)) {
//                 danItStudent [propertyName] = propertyValue;
//                console.log(danItStudent);
//             }
// }
// changeProperty(key,value);

// //Task 5
//
// let name = prompt("Name?");
// let lastName = prompt("LastName?");
// let age = +prompt("Age?");
//     while (age === "" || age < 15 || age > 100){
//          age = +prompt("Age?")}
// let children = prompt("Children?");
// let eyeСolor = prompt("Eye color?");
//
//
// const user = {
//     name,
//     lastName,
//     age,
//     children,
//     eyeСolor,
// }
// console.log(user);
// Object.keys(user).forEach(function(key) {
//     console.log(`Свойство ${key}: ${user[key]}`);
// });

// //Task6
//
// function createUser(name = null,lastName = null,profession = null) {
//     const user = {
//         name,
//          lastName,
//          profession,
//     }
//     return user;
//
// }
//
// console.log(createUser("Vasya","Petrenko", "Bodygard"));


// //Task 7
//
// let name = prompt("Name?");
// let lastName = prompt("LastName?");
// let age = +prompt("Age?");
//
// function createUser(name,lastName,age) {
//     const person = {
//         name,
//         lastName,
//         age,
//         sayHi(){
//             console.log(`Привет, я ${this.name}, ${this.lastName}, мне ${this.age}`)
//         }
//     }
//     return person;
// }
//
// createUser(name,lastName, age).sayHi();

//---------------------------------------------------------- DOM -----------------------------------------------------------------------------------

    // //Task 1

    // const root = document.getElementById("root");
    // const div = document.createElement("div");
    // let size;
    // do {
    //     size = prompt("size")
    // }
    // while (size === "" || isNaN(size)  || size <= 0 || typeof +size !== "number");
    //
    // //div.style.width = `${size}px`;
    // //div. style.height =` ${size}px`;
    // div.style.cssText = `width: ${size}px; height: ${size}px; background-color: red; margin: 100px auto;`;
    //
    // root.innerHTML = "";
    //
    // div.classList.add("square");
    // root.prepend(div);

    // //Task 2
    //
    // function createSquares (numberSquares) {
    //
    //     for (let i = 0; i < numberSquares; i++) {
    //         let size;
    //         do {
    //             size = prompt("enter size")
    //         } while (!chekNumber(size));
    //
    //         let error = 0;
    //         do {
    //             let color = prompt("enter color in rgb:0-255.0-255.0-255")
    //             let colorArray = color.split(".");
    //             error = 0;
    //             colorArray.forEach((element) => {
    //                 if (!chekNumber(element) || element < 0 || element > 255) {
    //                     error++;
    //                 }
    //             });
    //         } while (error !== 0);
    // }
    //  do {
    //      numberSquares = prompt("numberSquares");
    //     if (numberSquares >= 10) {
    //         console.log("NO!!!");
    //         continue;
    //     }
    //  } while (!chekNumber (numberSquares) || numberSquares <= 0 || numberSquares >=10);
    //
    //  function  chekNumber (number) {
    //    if (number.trim() === "" || isNaN(+number) || typeof +number !== "number") {
    //        return true;
    //         }
    //      }
    // }
    //
    // createSquares(5);



    // const btn = document.getElementById("btn");
    // // console.log(btn);
    // // btn.onclick = () => {
    // //     alert ("hi")
    // // };
    //
    // btn.addEventListener("click", ()=>{
    //     alert("hi")
    // });
    // document.addEventListener("DOMContentLoaded", function (e) {
    //
    // });


 //____________________________________________________ AddEventListener  _________________________________________________________________________


   // // Task 1
   //
   //   const  button = document.createElement("button");
   //   button. textContent = "Enter";
   //   document.body.append(button);
   //
   //   const onBtnClick = () => {
   //      alert("Welcome!")
   //  };
   //   button.addEventListener("click", onBtnClick);



    // // Task 2
    //
    // const  button = document.createElement("button");
    // button. textContent = "Enter";
    // document.body.append(button);
    //
    // const onBtnClick = () => {
    //     alert("Welcome! We are in system!")
    // };
    // button.addEventListener("click", onBtnClick);
    //
    // const onMouseOver = function () {
    //     alert("При клике по кнопке вы войдёте в систему.");
    //     button.removeEventListener("mouseover", onMouseOver);
    // }
    // button.addEventListener("mouseover", onMouseOver);

    // // // Task3
    // //
    // const phrase = "Добро пожаловать!";
    // function getRandomColor() {
    //     const r = Math.floor(Math.random() * 255);
    //     const g = Math.floor(Math.random() *255);
    //     const b = Math.floor(Math.random() * 255);
    //     return `rgb(${r}, ${g}, ${b})`}
    //
    // const h = document.createElement("h1");
    // h.textContent = phrase ;
    // document.body.append(h);
    // const  button = document.createElement("button");
    //  button. textContent = "Раскрасить";
    //  h.after(button);
    //
    // function changeColor () {
    //  let textArray = h.textContent.split("").map(element => {
    // let span = document.createElement("span");
    // span.textContent = element;
    // span.style.color = getRandomColor()
    //      return span;
    //  });
    // h.innerHTML = "";
    // h.append(...textArray);
    //  }
    //
    //  button.addEventListener("click", changeColor);

//---------------------------------------------------------------- DOM -----------------------------------------------------------------------------

    // // Task 6

    // const textarea = document.createElement("textarea");
    // document.body.append(textarea);
    // const userTracking = (event) => {
    //     const checkCorrectData = () => {
    //         let text = event.target.value;
    //         if (text.length < 20) {
    //             alert("Заполните поле!");
    //         }
    //     };
    //     let interval = setTimeout(checkCorrectData, 2000);
    // };
    //
    // textarea.addEventListener("blur",userTracking);

       // // Task 5
       //
       //  const textarea = document.createElement("textarea");
       //  document.body.append(textarea);
       //  const contentChecker = (event) => {
       //    let key = event;
       //    if (key.charCode < 97 || key.charCode > 122) {
       //        event.preventDefault();
       //        alert("Заглавные буквы вводить запрещено!")
       //    }
       //    console.log(event);
       //  };
       //  textarea.addEventListener("keypress", contentChecker);


       // // TASK 4
       //
       //   const textarea = document.createElement("textarea");
       //   document.body.append(textarea);
       //  const contentChecker = (event) => {
       //      let key = event;
       //      if (key.key >= 0 || key.key <= 9 ) {
       //          event.preventDefault();
       //      const p = document.createElement('p');
       //      p.textContent = "Нельзя вводить цифры!";
       //      p.style.color = "red";
       //      textarea.after(p);
       //      }
       //  };
       //  textarea.addEventListener("keypress", contentChecker);


      // //  Task 3
      //
      //
      //   const p = document.getElementsByTagName("p")[0];
      //   document.addEventListener("scroll", function () {
      //   if (p.getBoundingClientRect().top + p.offsetHeight < window.innerHeight) {
      //           alert("Теперь вам все видно!");
      //       }
      //   });
// ----------------------------------------------------------------------------------------------------------------------------------------------------
//     //Task 1
//
//     const  ul = document.querySelector (`#menu`);
//
//     ul.addEventListener(`click`,(evt) => {
//         evt.preventDefault();
//         const liItems = document.querySelectorAll("#menu li");
//         console.log(liItems)
//         Array.from(liItems).forEach(element => {
//             element.classList.remove("active");
//         });
//         evt.target.parentElement.classList.add("active");
//
//     });



    //     //Task 2
    //
    // const list = document.getElementById("messages");
    // list.addEventListener("click", function (evt) {
    //     if (evt.target.closest(".close-btn")) {
    // evt.target.closest(".pane").remove();
    //     }
    // });

// localStorage.setItem('myKey', 'myValue');
// let localValue = localStorage.getItem('myKey');
// console.log(localValue); //"myValue"
//
//
// localStorage.removeItem("myKey");
// localStorage.clear();
//
// localStorage["Ключ"] = "Значение" ;//установка значения
// localStorage["Ключ"];// Получение значения
// delete localStorage["Ключ"] ;

// let obj = {
//     item1: 1,
//     item2: [123, "two", 3.0],
//     item3:"hello"
// };
//
// let serialObj = JSON.stringify(obj); //сериализуем его
//
// localStorage.setItem("myKey", serialObj); //запишем его в хранилище по ключу "myKey"
//
// let returnObj = JSON.parse(localStorage.getItem("myKey"));
// console.log(returnObj);

//-----------------------------------------------------Local/storage---------------------------------------------------------------------------------

        // Task 1

    // const button = document.getElementById('button');
    // const reminder = document.getElementById("reminder")
    // const seconds = document.getElementById("seconds")
    // function creatReminder() {
    //     button.disabled = true;
    //  if (reminder.value.trim() === "") {
    //      alert("Ведите текст напоминания.");
    //  }
    //  if (seconds.value < 1) {
    //      alert("Время задержки должно быть больше\n" +
    //          "одной секунды.")
    //  }
    //  const timer = setTimeout(function () {
    //      alert(reminder.value);
    //      clearTimeout(timer);
    //      reminder.value = "";
    //      seconds.value = "";
    //      button.disabled = false;
    //  }, seconds.value * 1000);
    // }
    // button.addEventListener("click", creatReminder);
    // function checkEnter(event) {
    //     if(event.keyCode === 13) {
    //     creatReminder();
    //     }
    // }
    // reminder.addEventListener("keydown", checkEnter);
    // seconds.addEventListener("keydown",checkEnter);

