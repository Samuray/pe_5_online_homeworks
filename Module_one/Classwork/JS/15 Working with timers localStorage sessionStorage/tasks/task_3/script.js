// реализовать баннер по использование cookies, который будет фиксированным внизу страницы
// и занимать треть экрана.
// В нем должен быть текст и кнопка. По клику на эту кнопку - баннер должен скрываться
// и после перезагрузки страницы не показываться вновь


const banner = document.querySelector("#cookiesBanner");
const button = document.querySelector("#buttonCookies");


button.addEventListener("click", (evt) => {
   let newBanner = banner.style.display = "none";
   localStorage.setItem("data", newBanner);
});

window.addEventListener("load",function(){
    if(localStorage.getItem("data") === "none"){
        banner.style.display = "none";
    }
});


