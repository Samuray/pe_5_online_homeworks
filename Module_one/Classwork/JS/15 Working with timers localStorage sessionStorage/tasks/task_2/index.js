// Создать таймер. По клику на кнопку начинать отсчет.
// По клику на еще одну кнопку - обнулять. Таймер вывести на страницу


const start = document.querySelector("#begin");
const finish = document.querySelector("#finish");
const div = document.querySelector(".time");
let myTime = 0;

start.addEventListener("click", () => {
    function timer(){
        myTime += 1;
       let timerId =  setTimeout(timer,1000);
       div.style.cssText = `margin-left: 850px; margin-top: 250px; font-size: 300px`
        div.innerHTML = `${ myTime }`;
        finish.addEventListener("click",() => {
            clearTimeout(timerId);
            div.innerHTML = `${ myTime = 0 }`;
        });
    }
    timer();
});
