let caseValue = localStorage.getItem('case');
let ul = document.getElementById('cases-list');
ul.innerHTML = caseValue;

let input = document.getElementById('data');
input.addEventListener('change', addCase);

function addCase() {
  localStorage.setItem('case', input.value);
}
