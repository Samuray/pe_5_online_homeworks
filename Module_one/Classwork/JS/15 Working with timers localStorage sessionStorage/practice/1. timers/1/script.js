/**
 * Задание 1.
 *
 * Написать программу для напоминаний.
 *
 * Все модальные окна реализовать через alert.
 *
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */

    const button = document.getElementById('button');
    const reminder = document.getElementById("reminder")
    const seconds = document.getElementById("seconds")
    function creatReminder() {
        button.disabled = true;
     if (reminder.value.trim() === "") {
         alert("Ведите текст напоминания.");
     }
     if (seconds.value < 1) {
         alert("Время задержки должно быть больше\n" +
             "одной секунды.")
     }
     const timer = setTimeout(function () {
         alert(reminder.value);
         clearTimeout(timer);
         reminder.value = "";
         seconds.value = "";
         button.disabled = false;
     }, seconds.value * 1000);
    }
    button.addEventListener("click", creatReminder);
    function checkEnter(event) {
        if(event.keyCode === 13) {
        creatReminder();
        }
    }
    reminder.addEventListener("keydown", checkEnter);
    seconds.addEventListener("keydown",checkEnter);