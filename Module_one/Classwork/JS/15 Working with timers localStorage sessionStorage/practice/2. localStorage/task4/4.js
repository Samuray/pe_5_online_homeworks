/**
 * Задание 5.
 *
 * Сделать форму с сохранением данных в localStorage.
 */

    document.querySelectorAll('input').forEach(e => {
        e.addEventListener('change', (event)=>{
            localStorage.setItem(event.target.id, event.target.value)
        })
        if(localStorage.getItem(e.id)) {
            e.value = localStorage.getItem(e.id)
        }
    });