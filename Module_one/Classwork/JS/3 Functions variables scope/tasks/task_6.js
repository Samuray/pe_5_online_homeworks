// Спросить у пользователя его имя.
// Написать функцию, которая выводит в консоль количество букв в имени пользователя. 
// Обработать кейс, когда в начале слова могут быть введены пробелы, нужно вычислить длинну
// без их учета. 
// Если введены только цифры (полученное имя - это число), 
// то вывести в консоль - Ошибка, введите имя. 



// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/length