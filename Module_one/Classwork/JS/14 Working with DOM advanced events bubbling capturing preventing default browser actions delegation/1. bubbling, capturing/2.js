/**
 * События: прекращение всплытия.
 */

const cart = document.querySelector('.cart');
const ul = document.querySelector('ul');
const listItems = document.querySelectorAll('li');

const handler = event => {
  event.stopPropagation();
  console.log(`event.target: ${event.target.nodeName}`);
};

const globalHandler = (event, globalTarget) => {
  console.log(`
event.target: ${event.target.nodeName}
event.currentTarget: ${globalTarget}
  `);
};

window.addEventListener('click', () => globalHandler('window'));
document.addEventListener('click', () => globalHandler('document'));
document.body.parentElement.addEventListener('click', handler);
document.body.addEventListener('click', handler);

cart.addEventListener('click', handler);
ul.addEventListener('click', handler);

listItems.forEach(item => item.addEventListener('click', handler));
