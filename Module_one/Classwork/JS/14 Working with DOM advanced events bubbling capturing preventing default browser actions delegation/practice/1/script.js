/**
 * Задача 1.
 *
 * Необходимо «оживить» навигационное меню с помощью JavaScript.
 *
 * При клике на элемент меню добавлять к нему CSS-класс .active.
 * Если такой класс уже существует на другом элементе меню, необходимо
 * с того, предыдущего элемента CSS-класс .active снять.
 *
 * У каждый элемент меню — это ссылка, ведущая на google.
 * С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот внешний ресурс.
 *
 * Условия:
 * - В реализации обязательно использовать приём делегирования событий (на весь скрипт слушатель должен быть один).
 */



// document.querySelector("ul").addEventListener("click", (e) => {
//     if (e.target.tagName === "A") {
//         // console.log(e.target);
//         e.preventDefault();
//         document.querySelectorAll("a.active").forEach((element) => {
//             element.classList.remove("active");
//         });
//
//         e.target.classList.add("active");
//
//         document.querySelectorAll("section div.active").forEach((item) => {
//             item.classList.remove("active");
//         });
//
//         const id = e.target.getAttribute("data-id");
//         document.querySelector(id).classList.add("active");
//     }
// });

    document.querySelector("ul").addEventListener("click", (e) => {
        if (e.target.tagName === "A") {
            // console.log(e.target);
            e.preventDefault();
            document.querySelectorAll("a.active").forEach((element) => {
                element.classList.remove("active");
            });

            e.target.classList.add("active");

            document.querySelectorAll("section div.active").forEach((item) => {
                item.classList.remove("active");
            });

            document.querySelectorAll('A').forEach((elem,index) => {
            if( elem === e.target){
                // document.querySelector(`section div:nth-child(${index + 1})`).classList.add("active");
                document.querySelectorAll(`section div`)[index].classList.add("active");
            }
            })

        }
    });