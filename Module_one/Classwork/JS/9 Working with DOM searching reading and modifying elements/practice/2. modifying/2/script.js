/**
 * Задание 2.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 600.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */

    // let list = Array.from(document.querySelectorAll(".store li"));
    // console.log(list);
    // list.forEach(elem => {
    //     let prod = +elem.innerText.split(":")[1];
    //   if( prod === 0 ) {
    //       elem.innerText = elem.innerText.replace("0", "«закончился»");
    //         elem.style.color = "red";
    //         elem.style.fontWeight = "bold";
    //   }
    // });
