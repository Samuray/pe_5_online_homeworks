/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */

    // let elem = document.querySelector(".remove");
    // console.log(elem);
    // elem.remove();
    //
    // // document.querySelector(".bigger").classList.replace('bigger',"active");
    // document.querySelector(".bigger").classList.add("active");
    // document.querySelector(".bigger").classList.remove("bigger");