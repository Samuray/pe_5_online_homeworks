/**
 * Задание 1.
 *
 * Получить и вывести в консоль следующие элементы страницы:
 * - По идентификатору (id): элемент с идентификатором list;
 * - По классу — элементы с классом list-item;
 * - По тэгу — элементы с тэгом li;
 * - По CSS селектору (один элемент) — третий li из всего списка;
 * - По CSS селектору (много элементов) — все доступные элементы li.
 *
 * Вывести в консоль и объяснить свойства элемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */


    // console.log(document.getElementById("list"));
    // console.log(document.getElementsByClassName("list-item"));
    // console.log(document.getElementsByTagName("li"));
    // console.log(document.querySelector("li:nth-child(2)"));
    // console.log(document.querySelectorAll("li"));

