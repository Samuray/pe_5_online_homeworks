/**
 * Задание 7.
 *
 * Объяснить поведение каждое операции.
 */

console.log(true + false);
console.log(8 / "2");
console.log("number" + 5 + 1);
console.log(5 + 1 + "number");
console.log(null + 1);

console.log(undefined + 1);
