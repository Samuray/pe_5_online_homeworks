/**
 * ЗАДАНИЕ 1
 *
 * Вывести в консоль:
 * - Максимальное положительное число;
 * - Минимальное отрицательное число;
 * - Максимальное допустимое числовое значение в JavaScript;
 * - Минимальное допустимое числовое значение в JavaScript;
 * - Не число.
 *
 * Заметка: в этом задании необходимо проявить смекалку
 * и умение искать решения нестандартных задач.
 */
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Number

/* Максимальное доступное в языке число с плавающей точкой (дробное). */


console.log(Number.MAX_VALUE);
console.log(Number.MIN_VALUE);
console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MIN_SAFE_INTEGER);
console.log(NaN);

