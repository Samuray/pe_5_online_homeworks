"use strict";

// parseInt('123');
// parseFloat('123.123');
// +'12';
// Number('123');
//
// +String(123);//'123'
// let myNumber = 123;
// let myNumber2 = myNumber.toString();
// myNumber += '';
// myNumber = myNumber + '';
//
// console.log(typeof myNumber.toString())
//
// Boolean(0);
// Boolean(NaN);
// Boolean("");

// console.log(!1);//false
// console.log(!0);//true
// console.log(!!0);//false
//
// let age = +prompt('Введіть ваш вік');//3
// let name = '';
// if(age && age>=0) {
//     alert(age);
// }
//
// if(age > 18 && age < 60) {
//     if(name) {
//         console.log('Ваш вік підходить')
//     }
// } else if (age >= 60) {
//     console.log('Ваш вік похилий');
// } else if (age > 4) {
//     console.log('Ви занадто малі')
// } else {
//     console.log('Ви маля')
// }
//
// switch (true) {
//     case (age > 18 && age < 60):
//
//         if(name) {
//             console.log('Ваш вік підходить');
//         }
//         break;
//     case (age >= 60):
//         console.log('Ваш вік похилий');
//         break;
//     case (age > 4):
//         console.log('Ви занадто малі')
//         break;
//     default:
//         console.log('Ви маля')
// }
//
//
// switch (age) {
//     case 1:
//         console.log('Вам 1 рік');
//         break;
//     case 2:
//         console.log('Вам 2 рік');
//         break;
//     default:
//         console.log('....');
// }
//
// if(age > 10)
//     console.log('');
//     console.log('12');
//
// if(age > 10) {
//     console.log('');
// }

for(let i = 1; i < 11; i++) {
    console.log(i)
}

let i = 1;
while (i < 11) {
    console.log(i);
    i++;
}

let k = 1;
do {
    console.log(k);
    k++;
} while (k < 11)


let age;

do {
    age = +prompt('Введіть ваш вік');
} while (!age || age < 0)

do {
   ///
} while (confirm('Repeat?'))