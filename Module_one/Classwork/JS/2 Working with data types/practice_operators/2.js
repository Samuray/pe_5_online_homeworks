/**
 * Задание 2.
 *
 * Написать программу, которая будет приветствовать пользователя.
 * Сперва пользователь вводит своё имя, после чего программа выводит в консоль сообщение с учётом его должности.
 *
 * Список должностей:
 * Mike — CEO;
 * Jane — CTO;
 * Walter — программист:
 * Oliver — менеджер;
 * John — уборщик.
 *
 * Если введёно не известное программе имя — вывести в консоль сообщение «Пользователь не найден.».
 *
 * Выполнить задачу в двух вариантах:
 * - используя конструкцию if/else if/else;
 * - используя конструкцию switch.
 */

    let name = prompt ("your name");
    switch (name) {
        case "Mike":
            console.log("hello Mike - ceo");
            break;
        case "Jane":
            console.log("hello Jane - cto");
            break;
        case "Walter":
            console.log("hello Walter - программист");
            break;
        case "Oliver":
            console.log("hello Oliver- менеджер");
            break;
        case "John":
            console.log("hello John - уборщик");
            break;
        default:
            console.log("Пользователь не найден");
    }

    if (name === "Mike"){
     console.log("hello Mike - ceo");
    }
    else if (name === 'Jane'){
        console.log("hello Jane - cto");
    }
    else if (name === 'Walter'){
        console.log("hello Walter - программист");
    }
    else if (name === 'Oliver'){
        console.log("hello Oliver- менеджер");
    }
    else if (name === 'John') {
        console.log("hello John - уборщик");
    }
    else {
        console.log("пользователь не найден!");
    }