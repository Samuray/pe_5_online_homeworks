/**
 * Задание 3.
 *
 * Пользователь вводит 3 числа.
 * Вывести в консоль сообщение с максимальным числом из введённых.
 *
 * Если одно из введённых пользователем чисел не является числом,
 * вывести сообщение: «⛔️ Ошибка! Одно из введённых чисел не является числом.».
 *
 * Условия: объектом Math пользоваться нельзя.
 */

let num1 = prompt("fill a number");
let num2 = prompt("fill a number");
let num3 = prompt("fill a number");

if(
     (num1 === "" || isNaN(num1) || !num1) ||
     (num2 === "" || isNaN(num2) || !num2) ||
     (num3 === "" || isNaN(num3) || !num3)){
    console.log("«⛔️ Ошибка! Одно из введённых чисел не является числом.»");
} else {
    if(num1 > num2 && num1 > num3) {
        console.log(num1);
    }else if(num2 > num1 && num2 > num3){
        console.log(num2);
    }else {
        console.log(num3);
    }
}