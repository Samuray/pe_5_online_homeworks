/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */
const LIGHT_CELL = '#ffffff';
const DARK_CELL = '#161619';
const V_CELLS = 8;
const H_CELLS = 8;

        // let fillChessBoard = function () {
        //     const board = document.createElement('section');
        //     board.classList.add('board');
        //     document.body.prepend(board);
        //     let isRowOdd = true;
        //     for (let i = 0; i < V_CELLS * H_CELLS; i++) {
        //         const cell = document.createElement('div');
        //         cell.classList.add('cell');
        //         board.append(cell);
        //         if (i % 8 === 0){
        //             isRowOdd = !isRowOdd
        //         }
        //         if (i % 2) {
        //             cell.style.backgroundColor = isRowOdd ? LIGHT_CELL : DARK_CELL
        //
        //         } else {
        //             cell.style.backgroundColor = isRowOdd ? DARK_CELL : LIGHT_CELL
        //         }
        //     }
        // }
        // fillChessBoard();
