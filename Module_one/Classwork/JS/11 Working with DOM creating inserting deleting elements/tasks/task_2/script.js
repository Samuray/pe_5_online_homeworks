// Задание

// Программа будет выводить то или иное сообщение на страницу в зависимости от возраста пользователя.
// Сообщение представляет из себя html-элемент div. В нем будет сообщение (далее alert-message) и тип
// (тип алерта - это тот или иной класс для div).

// Логика:
// Спросить у пользователя его имя и возраст.
// Если число (также проверить, что там число) меньше 18, вывести на страницу
// сообщение, где alert-message - Sorry, you are not be able to visit us. Тип - 'alert-danger'
// Если больше 18 - alert-message 'Hi, ${name}.', тип 'alert-success';


    let name = prompt("Введите имя");
    let age;
    do{
        age = +prompt("Введите возраст");
    } while (isNaN(age) || !age);

    function alertMessage(name,age){
        let div = document.createElement("div");
        let p = document.createElement("p");
        if(age < 18){
            p.textContent = "Sorry, you are not be able to visit us.";
            div.append(p);
            document.body.append(div);
            div.classList.add("alert-danger");
        } else if (age > 18) {
            p.textContent = `Hi, ${name}`;
            div.append(p);
            document.body.append(div);
            div.classList.add("alert-success");
        }
    }
    alertMessage(name,age);