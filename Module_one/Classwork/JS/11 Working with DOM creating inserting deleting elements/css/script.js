const newDiv = document.createElement('div');
const fragment = document.createDocumentFragment();

// newDiv.remove();

// document.body.appendChild(newDiv);
document.body.append(newDiv);

newDiv.textContent = 'test';
newDiv.classList.add('my-class');

const newP = document.createElement('p');

// newDiv.after(newP);

newP.classList.add('new-p');

// newDiv.before(document.createElement('p'));
// newDiv.prepend(document.createElement('p'));
// newDiv.append(document.createElement('p'));
// newDiv.after(document.createElement('p'));
//
// newDiv.insertAdjacentHTML('beforebegin', '<div class="class1" style="">cacasc</div>');//before
// newDiv.insertAdjacentHTML('beforeend', '<div></div>');//append
// newDiv.insertAdjacentHTML('afterbegin', '<div></div>');//prepend
// newDiv.insertAdjacentHTML('afterend', '<div></div>');//after

document.body.innerHTML = document.body.innerHTML + newP.outerHTML;

newP.classList.toggle('class-2');
newP.className += ' class-2';
newP.setAttribute('class', newP.getAttribute('class')
    + ' acascscsc');

const thirdDiv = document.querySelector('div:nth-child(3)');
const divs = [
    document.createElement('p'),
    document.createElement('span')
];
thirdDiv.after(...divs);

