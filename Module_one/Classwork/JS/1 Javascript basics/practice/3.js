/**
 * ЗАДАНИЕ 3
 *
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи parseInt(), parseFloat(), унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 */

let str = '123';
console.log(typeof str);
console.log(typeof parseInt(str));
console.log(typeof parseFloat(str));
console.log(typeof +str);
