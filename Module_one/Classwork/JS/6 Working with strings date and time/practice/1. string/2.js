/**
 * Задание 2.
 *
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */

        // function capitalizeAndDoublify(text) {
        //     let newText = "";
        //     for (let i of text) {
        //         newText = newText + i.repeat(2);
        //     }
        //     return newText.toUpperCase();
        // }
        //
        // console.log(capitalizeAndDoublify("hello"));
