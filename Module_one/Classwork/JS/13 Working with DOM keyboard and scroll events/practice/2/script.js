/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */



        document.body.addEventListener("keyup", (evt) => {
            if(evt.code === "Enter"){
                document.getElementById("enter-counter").innerText++;
            } else  if(evt.code === "Space"){
                document.getElementById("space-counter").innerText++;
            } else if(evt.code === "Backspace"){
                document.getElementById("backspace-counter").innerText++;
            }
        });


