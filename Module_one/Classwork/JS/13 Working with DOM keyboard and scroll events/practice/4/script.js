/**
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Shift + D на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */

    const input = document.getElementById("new-task");
    const btn = document.getElementById("clear");
    const ul = document.querySelector(".tasks-list");

    input.addEventListener("keyup", (evt) => {
        if( evt.code === 'Enter' && input.value){
           let li = document.createElement("li");
           li.innerText = input.value;
            ul.append(li);
            input.value = "";
        }
    });
    document.body.addEventListener("keyup",(evt) => {
        if(evt.shiftKey && evt.code === "KeyD"){
            ul.lastChild.remove();
        } else if(evt.shiftKey && evt.altKey && evt.code === "Backspace"){
          liRemove();
        }
    })
    btn.addEventListener("click",liRemove);

    function liRemove() {
        if(confirm("Хотите удалить?") === true){
            ul.querySelectorAll("li").forEach(elem => {
                elem.remove();
            });
        }
    }