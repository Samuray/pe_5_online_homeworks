/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */


    document.body.style.fontSize = "16px";
    document.body.addEventListener("keyup", (evt) => {
       if(evt.code === "Equal" && evt.shiftKey && (parseInt(document.body.style.fontSize) < 30)){
            document.body.style.fontSize = (parseInt(document.body.style.fontSize)+1) + "px";
       }
        if(evt.code === "Minus" && evt.shiftKey && (parseInt(document.body.style.fontSize) > 10)){
            document.body.style.fontSize = (parseInt(document.body.style.fontSize)-1) + "px";
        }
    });