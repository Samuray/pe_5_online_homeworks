/**
 * Задание 1.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 */


        const h1 = document.createElement("h1");
        h1.textContent = "Нажмите любую клавишу.";
        document.body.append(h1);

        document.body.addEventListener("keypress", (evt) => {
            h1.textContent = `Нажатая клавиша: ${evt.key}`
        });