/**
 * События: основы.
 */

const cart = document.querySelector('.cart');

const handler = event => {
  console.log('event:', event);
  console.log('event.type:', event.type);
  console.log('event.target:', event.target);
};

cart.addEventListener('click', handler);
