/**
 * Задача 3.
 *
 * Улучшить функцию-исследователь навыков разработчика из предыдущего задания.
 *
 * После ведения пользователем своих навыков функция выводит их на экран посредством функции alert.
 * После чего спрашивает правильно-ли пользователь ввёл данные о своих навыках.
 *
 * --- Если пользователь ответил «нет» ---
 * Программа спрашивает его какие навыки необходимо удалить из списка.
 * Если пользователь ввёл навык для удаления из списка, которого в списке не существует, программа
 * оповещает пользователя об ошибке и повторно запрашивает данные.
 *
 * Программа запрашивает данные о навыках для удаления из списка до тех пор,
 * пока пользователь не кликнет по кнопке «Отменить» в диалоговом окне.
 *
 * --- Если пользователь ответил «да» ---
 * Программа выводит данные о навыках в консоль.
 */


function developerSkillInspector(params) {
    let question = "";
    let getSkill = [];
    while (question !== null) {
        question = prompt("Введите навык");
        if (question !== null && question.length > 1) {
            getSkill.push(question);
        }
    }
    alert("Ваши навыки: " + getSkill);
    if (!confirm("Вы верно ввели навыки?")) {
        let thirdQuestion;
        while (
            (thirdQuestion = prompt("Какой навыки необходимо удалить?")) !== null
            ) {
            if (getSkill.includes(thirdQuestion)) {
                getSkill.splice(getSkill.indexOf(thirdQuestion), 1);
            } else {
                alert("Такого навыка не существует");
            }
        }
    }
    alert("Ваши навыки: " + getSkill);
}

developerSkillInspector();










































    // function developerSkillInspector() {
    //     let question = "";
    //     let getSkill = [];
    //
    //     while (question !== null) {
    //         question = prompt("Введите навык");
    //         if(question !== null && question.length > 1) {
    //             getSkill.push(question);
    //         }
    //     }
    //     alert(getSkill);
    //     let questionNext = prompt("Все правильно?");
    //     if(questionNext === '') {
    //         console.log(getSkill);
    //     } else {
    //             let questionDel = prompt("Введите навык для удаления");
    //             getSkill.forEach((elem, i) => {
    //                 if (elem === questionDel) {
    //                     getSkill.splice(i, 1);
    //                 }
    //                 // else if(elem !== questionDel){
    //                 //     while (questionDel !== null) {
    //                 //         console.log("Ошибка! такого навыка нет!");
    //                 //         questionDel = prompt("Введите навык для удаления");
    //                 //     }
    //                 // }
    //             });
    //         console.log(getSkill);
    //      }
    // }
    // developerSkillInspector();