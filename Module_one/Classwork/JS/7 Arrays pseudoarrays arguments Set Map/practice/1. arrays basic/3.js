/**
 * Задача 3.
 *
 * Напишите функцию mergeArrays для объединения нескольких массивов в один.
 *
 * Функция обладает неограниченным количеством параметров.
 * Функция возвращает один массив, который является сборным из массивов,
 * переданных функции в качестве аргументов при её вызове.
 *
 * Условия:
 * - Все аргументы функции должны обладать типом «массив», иначе генерировать ошибку;
 * - В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.
 *
 * Заметки:
 * - Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно.
 */


             // function  mergeArrays (...params) {
             //         let newArr = [];
             //     params.forEach((element, i) => {
             //         if (!Array.isArray(element)) {
             //             throw new Error(`Ошибка! Элемент с индексом ${i} не является массивом!`) ;
             //        }
             //     });
             //     newArr = newArr.concat(...params);
             //     return newArr;
             // }
             //  console.log(mergeArrays([9],[8],[8],[9]));


        // const arrayOne = [1, 2, "Vadim"];
        // const arrayTwo = [4, 5, "Bogdan", 2];
        // const arrayThree = [4, 5, "Bogdan", 2];
        //
        // function mergeArrays() {
        //     let emptyArray = [];
        //     let i = 1;
        //     for (const iterator of arguments) {
        //         if (!Array.isArray(iterator)) {
        //             console.log("Не массив " + i);
        //         }
        //         emptyArray = emptyArray.concat(iterator);
        //         i++;
        //     }
        //     return emptyArray;
        // }
        //
        // console.log(mergeArrays(arrayOne, arrayTwo, arrayThree, ""));