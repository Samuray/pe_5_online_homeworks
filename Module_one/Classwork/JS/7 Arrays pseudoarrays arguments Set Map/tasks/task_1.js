// Создать массив объектов students (в количестве 7 шт).
// У каждого студента должно быть имя, фамилия и направление обучения - Full-stask
// или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
// `Привет, я ${имя}, студент Dan, направление ${направление}`.
// Перебрать каждый объект и вызвать у каждого объекта метод sayHi;

    // const students = [
    //     {
    //         name: "Vasya",
    //         lastName:" Vasenko",
    //         prof: "Full-stask",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Petya",
    //         lastName: "Petrenko",
    //         prof: "Full-stask",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Gosha",
    //         lastName: "Gogenko",
    //         prof:" Front-end",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Dima",
    //         lastName: "Dimenko",
    //         prof: "Full-stask",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Stas",
    //         lastName: "Stasenko",
    //         prof: "Front-end",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Vlad",
    //         lastName: "Vladenenko",
    //         prof: "Full-stask",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     },
    //     {
    //         name:"Egor",
    //         lastName: "Egorenko",
    //         prof: "Front-end",
    //         sayHi(){
    //             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.prof}`)
    //         }
    //     }];
    //
    // students.forEach((elem) => {
    //     elem.sayHi();
    // })