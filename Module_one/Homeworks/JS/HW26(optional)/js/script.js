"use strict";


const images = [
    {
        src:"./img/0.jpg"
    },
    {
        src:"./img/1.jpg"
    },
    {
        src:"./img/1-47-1.jpg"
    },
    {
        src:"./img/70.jpg"
    },
    {
        src:"./img/bd.jpg"
    },
    {
        src:"./img/shd.jpg"
    },
];

let mainContainer = document.querySelector('.carousel_wrapper')
let previousButton = document.getElementById("previous");
let nextButton = document.getElementById("next");
let currentSlide = 0;
let slideWidth = 400;

function render(){
    const imgArr = images.map(elem => {
        return `<div class="slide" style="background-image: url('${elem.src}')"></div>` });
    mainContainer.innerHTML = imgArr.join(' ');
}

    function translate(){
    mainContainer.style.transform = `translateX(-${ slideWidth * currentSlide }px)`
}

    function nextSlide (){
    if (currentSlide >= images.length - 1){
     currentSlide = -1;
    }
    currentSlide = currentSlide + 1;
    translate();
}
    function prevSlide(){
    if (currentSlide === 0){
        currentSlide = images.length;
    }
    currentSlide = currentSlide - 1;
    translate();
}
previousButton.addEventListener("click",prevSlide);
nextButton.addEventListener("click",nextSlide);
render();