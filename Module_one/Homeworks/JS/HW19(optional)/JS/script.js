"use strict";

function countDate (stopPoints,backLogs,date){
  let initialValue = 0;
    let allPoints = stopPoints.reduce((previousValue, currentValue) => previousValue + currentValue, initialValue);
    let allLogs = backLogs.reduce((previousValue, currentValue) => previousValue + currentValue, initialValue);
    let days = Math.round((allLogs / allPoints));
    console.log(days);
    let today = Date.now();
    let newDate = Date.parse(date);
    let time = Math.round((newDate - today) / 1000 / 60 / 60 / 24) ;
    console.log(time);
    if((time - days) > 0) {
      console.log(`Усі завдання будуть успішно виконані за ${ Math.round((time-days)) } днів до настання дедлайну!`)
    }
    if((time - days) < 0) {
      console.log(`Команді розробників доведеться витратити додатково ${-((time - days) * 8)} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
    if((time - days) === 0) {
      console.log("Усі завдання будуть успішно виконані в срок!");
    }

}
countDate([10,10,10],[30,20,25], "2022.08.18");