"use strict";

let num = prompt("Fill a number");
while (!num || isNaN(num) || num === "string") {
    num = prompt("Fill a number", num);
}

function countFactorial(num) {
    return (num !== 1) ? num * countFactorial(num - 1) : 1;
}
    alert(countFactorial(num));