'use strict';

    const form = document.querySelector(".password-form");
    const input = document.getElementById("password-input");
    const input2 = document.getElementById("input-password");
    const iconEye = document.querySelector(".fa-eye");
    const iconEyeSlash = document.querySelector(".fa-eye-slash");
    const iconEye2 = document.getElementById("eye");
    const iconEyeSlash2 = document.getElementById("eye-slash");
    const btn = document.querySelector(".btn");
    const div = document.getElementById("error");

    function getInput(evt,eye,eyeSlash){
        if (evt.target.getAttribute("type") === "password") {
            eye.classList.remove("hidden");
            eyeSlash.classList.add("hidden");
            evt.target.setAttribute("type", "text");
        } else {
            eyeSlash.classList.remove("hidden");
            eye.classList.add("hidden");
            evt.target.setAttribute("type", "password");
        }
    }

    input.addEventListener("click", (evt) => {
        let eye = iconEye;
         let eyeSlash = iconEyeSlash;
        getInput(evt,eye,eyeSlash);
    });

    input2.addEventListener("click", (evt) => {
       let eye = iconEye2;
       let eyeSlash = iconEyeSlash2;
        getInput(evt,eye,eyeSlash);
    });

    form.addEventListener("submit", function (evt) {
        evt.preventDefault();
        let x = input.value,
            y = input2.value;

        if (x === y) {
            alert("You are welcome!");
            return true;
        }
        else {
         div.innerHTML = "Нужно ввести одинаковые значения!";
            return false;
        }
    });


