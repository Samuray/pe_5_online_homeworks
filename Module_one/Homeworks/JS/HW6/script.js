"use strict";

function createNewUser() {
    const firstName = prompt("Введите имя");
    const lastName = prompt("Введите фамилию");
    const   birthday = prompt("Введите дату вашего рождения в формате: dd.mm.yyyy,");
    let newUser;
    newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        getAge() {
            const userBirthdayValue = this.birthday.split('.');
            console.log(userBirthdayValue);
            const userBirthdayDate = new Date(+userBirthdayValue[2], +userBirthdayValue[1]-1, +userBirthdayValue[0]);
            console.log(userBirthdayDate);
            return Math.floor(((Date.now() - userBirthdayDate) / (24 * 3600 * 365.25 * 1000)));
        },
        getPassword() {
            return this.firstName.charAt(0).toUpperCase()
                + this.lastName.toLowerCase()
                + this.birthday.slice(-4);
        },
    };
    return newUser;

}

const user = createNewUser();
console.log(`Ваш логин: ${user.getLogin()} \n Ваш возраст: ${user.getAge()} \n Ваш пароль: ${user.getPassword()}`);