"use strict";

    const btnWrap = document.querySelector('.btn-wrapper');
    document.body.addEventListener('keyup', lightLetters);

    function lightLetters(evt) {
        const keyUp = evt.key.toUpperCase();
        const key = evt.key;
        const className = 'active';
        for (let btn of btnWrap.children) {
            if (btn.classList.contains(className)) {
                btn.classList.toggle(className);
            }
            if (btn.innerText === keyUp) {
                btn.classList.toggle(className);
            } else if (btn.innerText === key) {
                btn.classList.toggle(className);
            }
        }
    }




