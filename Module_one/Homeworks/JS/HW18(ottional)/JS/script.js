"use strict";

function deepClone(obj,target){

    let cloneObj = target ;
    let toStr = Object.prototype.toString;
    for (let prop in obj){
        if (obj.hasOwnProperty(prop)){
            if (typeof obj[prop] !== 'object' && obj[prop] !== 'null'){
                cloneObj[prop] = obj[prop];
            }else {
                cloneObj[prop] = toStr.call(obj[prop]) === '[object Object]' ? {}:[];
                deepClone(obj[prop],cloneObj[prop]);
            }
        }
    }
    console.log(cloneObj);
    return cloneObj;
}

deepClone( {name: "Vasya", lastName:"Vasek", age: 18, profession: "manager", parent:{mather: "Tanya", father:"Gosha"}}, {});
