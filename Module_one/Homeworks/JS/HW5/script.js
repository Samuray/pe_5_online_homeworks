"use strict";

function createNewUser() {
    const firstName = prompt("Введите имя");
    const lastName = prompt("Введите фамилию");
    let newUser;
    newUser = {
        firstName,
        lastName,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        setFirstName(value){
            Object.defineProperty(newUser, "firstName", {
                writable:false,
                configurable:true,
                value: value,
                set function(value) {
                    return this[firstName] = value;
                },
                get function() {
                    return this.firstName;
                },
            });
        },
        setLastName(value) {
            Object.defineProperty(newUser, "lastName", {
                writable: false,
                configurable: true,
                value: value,
                set function(value) {
                    return this[lastName] = value;
                },
                get function() {
                    return this.lastName;
                },
            });
        }
    };
    return newUser;

}

const user = createNewUser();

// Uncomment to call setters

// user.setFirstName("Boba");
// user.setLastName("Bobashvilli");
console.log(`Ваш логин:${user.getLogin()}`);




















