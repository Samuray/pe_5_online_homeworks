"use strict";

    if (!localStorage.theme) {
        localStorage.theme = "light";
        document.body.className = localStorage.theme;
        btn.innerText = document.body.classList.contains("dark") ? "Light" : "Dark";
    }

    btn.addEventListener("click", () => {
        document.body.classList.toggle("dark");
        btn.innerText = document.body.classList.contains("dark") ? "Light" : "Dark";
        localStorage.theme = document.body.className || "light";
    });

    window.addEventListener("load",function(){
        if(localStorage.getItem("theme") === "light"){
            document.body.className = "light"
        }else {
            document.body.className = "dark";
        }
    });