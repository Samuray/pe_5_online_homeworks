"use strict";

const pColor = document.querySelectorAll("p");
for (const elem of pColor) {
    elem.style.color = "#ff0000";
}
const elem = document.getElementById("optionsList");
console.log(elem);
console.log(elem.parentNode);
console.log(elem.childNodes);

const newElement = document.createElement("p");
newElement.classList.add('testParagraph')
newElement.innerHTML = "This is a paragraph";
console.log(newElement);

const element = document.querySelector(".main-header");
const elementChilds = element.children;
console.log(elementChilds);
                                            // Разкоментировать, чтобы присвоить класс!

// for (const item of elementChilds) {
//     item.classList.add("nav-item");
// }
// console.log(elementChilds);

const tags = document.querySelectorAll(".section-title");
console.log(tags);
for (const tag of tags) {
    tag.classList.remove("section-title");
}
console.log(tags);