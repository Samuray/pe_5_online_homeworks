`use strict`;

function filterBy(array, typeData) {
    let results = array.filter((item) => {
        let data = typeData.toLowerCase();
        let toString = Object.prototype.toString;
        let x = toString.call(item).replace('[','').replace(']','').replace('object ','').toLowerCase();
        return x !== data;
    })

    return results;
}

console.log(filterBy(['hello', 'world', 23, '23', null], "number"));




