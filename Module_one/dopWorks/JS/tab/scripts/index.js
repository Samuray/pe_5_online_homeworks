
let currentCategory = 'wolverine';

const container = document.querySelector('#navigation');
console.log(container);

container.addEventListener('click', (event) => {
    currentCategory = event.target.dataset.category;
    console.log(currentCategory);

    const items = document.querySelectorAll('.content-item');
    items.forEach(item => {
        item.classList.remove('visible');

        if (item.dataset.category === currentCategory) {
            item.classList.add('visible');
        }
    })

    const tabs = document.querySelectorAll('li');
    tabs.forEach(item => {
        item.classList.remove('active');

        if (item.dataset.category === currentCategory) {
            item.classList.add('active');
        }
    })

});


