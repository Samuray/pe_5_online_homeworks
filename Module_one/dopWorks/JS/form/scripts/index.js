const inputField = document.querySelector('#email');
const spanErr = document.querySelector('.error');

inputField.addEventListener('blur', (e) => {
    const value = e.target.value;
    if (value.indexOf('@') === -1 || value.indexOf('.com') === -1) {
        spanErr.innerText = 'Invalid email'
        inputField.classList.add('error');
    }
});

inputField.addEventListener('focus', () => {
    spanErr.innerText = '';
    inputField.classList.remove('error');
});

const inputTel = document.querySelector('#tel');

inputTel.addEventListener('input', event => {
    if ( isNaN(+(event.target.value)) ) {
        event.target.value = event.target.value.substr(0, event.target.value.length - 1);
    }
})

const inputPassword = document.querySelector("#password");
const inputShowPassword = document.querySelector(".icon");

inputShowPassword.addEventListener("click", (e) =>{
    if (inputPassword.type === "text") {
        inputPassword.type = "password";
        inputShowPassword.querySelector("images").src = "./svg/eye-off.svg"

    } else {
        inputPassword.type = "text"
        inputShowPassword.querySelector("images").src = "svg/eye.svg"
    }

})



