const carousel = {
    mainContainer: document.querySelector('.carousel_wrapper'),
    textContainer: document.querySelector('.carousel_text-container'),
    paginationContainer: document.querySelector('.carousel_pagination-container'),
    buttons: {},
    imageURLs: [
        'https://picsum.photos/900/450?v=1',
        'https://picsum.photos/900/450?v=2',
        'https://picsum.photos/900/450?v=3',
        'https://picsum.photos/900/450?v=4',
        'https://picsum.photos/900/450?v=5',
    ],
    titles: [
        'The greatest glory',
        'Your time is limited',
        'If life were predictable',
        'If you set your goals',
        'Life is what happens',
    ],
    descriptions: [
        'The greatest glory in living lies not in never falling, but in rising every time we fall.',
        'Your time is limited, so don\'t waste it living someone else\'s life. Don\'t be trapped by dogma – which is living with the results of other people\'s thinking.',
        'If life were predictable it would cease to be life, and be without flavor.',
        'If you set your goals ridiculously high and it\'s a failure, you will fail above everyone else\'s success.',
        'Life is what happens when you\'re busy making other plans.',
    ],
    authors: [
        'Nelson Mandela',
        'Steve Jobs',
        'Eleanor Roosevelt',
        'James Cameron',
        'John Lennon',
    ],
    slideWidth: 900,
    currentSlide: 0,


    render(){
        const imgArr = this.imageURLs.map(elem => {
            return `<div class="slide" style="background-image: url('${elem}')">
                             <div class="slide_shadow"></div>     
                     </div>` });
        this.mainContainer.innerHTML = imgArr.join(' ');
        this.addEventListeners();
    },
    translate(){
        this.mainContainer.style.transform = `translateX(-${this.slideWidth * this.currentSlide}px)`
    },
    nextSlide (){
        if (this.currentSlide >= this.imageURLs.length){
            return;
        }
        this.currentSlide = this.currentSlide + 1;
        this.translate();
    },
    prevSlide(){
        if (this.currentSlide === 0){
            return;
        }
        this.currentSlide = this.currentSlide - 1;
        this.translate();
    },

    btnNext: document.querySelector('.carousel_button-next'),
    btnPrev: document.querySelector('.carousel_button-prev'),

    addEventListeners(){
        this.btnPrev.addEventListener('click', this.prevSlide.bind(this));
        this.btnNext.addEventListener('click', this.nextSlide.bind(this));
    },
    setText(){
        const newTitle = this.titles.map(elem => {
            const newDescriptions = this.descriptions.map(elem => {

            })
            return`<div class="carousel_text-container">
                       <h2 class="${elem}"></h2>
                       <p class="description"></p>
                       <span class="author"></span>
                    </div>`
        })
    }
};

carousel.render();
carousel.translate();
carousel.nextSlide();
carousel.prevSlide();


