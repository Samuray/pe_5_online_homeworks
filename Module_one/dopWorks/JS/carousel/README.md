#Карусель изображений

Дан объект карусели с инициализированными входными данными

````javascript
const carousel = {
  mainContainer: document.querySelector('.carousel_wrapper'),
  textContainer: document.querySelector('.carousel_text-container'),
  paginationContainer: document.querySelector('.carousel_pagination-container'),
  buttons: {},
  imageURLs: [
    'https://picsum.photos/900/450?1',
    'https://picsum.photos/900/450?2',
    'https://picsum.photos/900/450?3',
    'https://picsum.photos/900/450?4',
    'https://picsum.photos/900/450?5',
  ],
  titles: [
    'The greatest glory',
    'Your time is limited',
    'If life were predictable',
    'If you set your goals',
    'Life is what happens',
  ],
  descriptions: [
    'The greatest glory in living lies not in never falling, but in rising every time we fall.',
    'Your time is limited, so don\'t waste it living someone else\'s life. Don\'t be trapped by dogma – which is living with the results of other people\'s thinking.',
    'If life were predictable it would cease to be life, and be without flavor.',
    'If you set your goals ridiculously high and it\'s a failure, you will fail above everyone else\'s success.',
    'Life is what happens when you\'re busy making other plans.',
  ],
  authors: [
    'Nelson Mandela',
    'Steve Jobs',
    'Eleanor Roosevelt',
    'James Cameron',
    'John Lennon',
  ],
}
````

###Задание 1
Написать метод ```render()``` который будет генерировать HTML слайдов
в формате:
```html
    <div class="slide" style="background: url('https://some-url.com')">
        <div class="slide_shadow"></div>     
    </div>
```
для всех значений ```imageURLs``` и отрисовывать их в контейнер с классом "carousel_wrapper".  
По сути - это инициализурующая функция;

###Задание 2
Написать метод ```translate()``` который будет смещать "carousel_wrapper"
по оси X опираясь на значения ширины слайдера (900px) и номер текущего слайда;

Использовать css свойство ```transform: translateX()```

###Задание 3
* Написать методы ```nextSlide()``` и ```prevSlide()``` которые будут перемещать
"carousel_wrapper" на 1 слайд вперед и назад соответственно;


###Задание 4
* Использую методы ```document.createElement``` написать функцию, которая создает кнопки переключения слайдов и вставляет их в контейнер

```html

<button class="carousel_button carousel_button-prev">
    <img src="svg/chevron-left.svg" alt="Left">
</button>

<button class="carousel_button carousel_button-next">
    <img src="svg/chevron-right.svg" alt="Right">
</button>
```

###Задание 5
* Написать метод ```addListeners()``` который будут навешивать листенеры на отрисованные элементы;
* Навесить методы ```nextSlide()``` и ```prevSlide()``` на соответствующие кнопки на событие 'click';
* Вызвать метод ```addListeners()``` в методе ```render()```;

###Задание 6
Написать метод ```setText()``` который будет устанавливать в значения
```html
<div class="carousel_text-container">
   <h2 class="title"></h2>
   <p class="description"></p>
   <span class="author"></span>
</div>
```
соответствующие данные из ```titles```, ```descriptions``` и  ```author```
опираясь на параметр ```currentSlide```;

###Задание 7
* Написать метод ```changeText()``` который будет добавлять класс анимации '.fade-in-out' к 
контейнеру с текстом и менять его содержимое (```setText()```);
* Добавить метод в ```nextSlide()``` и ```prevSlide()```;  

Использовать ```setTimeout()```

###Задание 8
* Написать метод ```renderPagination()``` который будет генерировать HTML пагинации
в формате:
```html
    <span class="pagination-item"></span>
```

основываясь на количестве изображений в ```imageURLs```, и отрисовывать этот 
html в контейнер "carousel_pagination-container";

* Добавлять при отрисовке первому элементу "pagination-item" класс "active";
* Добавить метод в ```render()```;

###Задание 9
* Написать метод ```setActiveSlideOnPaginationClick()``` который будет
при клике на item пагинации переключать карусель на соответствующий слайд.
* Добавлять класс "active" на активный item пагинации;  

Использовать data-аттрибуты

###Задание 10 
* Навесить ```setActiveSlideOnPaginationClick()``` на items пагинации.
  
Использовать делегирование;

* Добавить переключения класса "active" на пагинации в методы ```nextSlide()``` и ```prevSlide()```

###Задание 11
Наслаждаться результатом

###Задание 12 (опциональное)
Сделать слайдер зацикленным
