const http = require('http');
const fs = require('fs');
const PORT = 3000;
const HOST = 'localhost';

let server = http.createServer((req, res) => {
    // res.writeHead(200, {'Content-Type':'text/plain; charset=utf-8'});
    // res.end('Hello Node JS');
    res.writeHead(200, {'Content-Type':'text/html; charset=utf-8'});
    // res.end(`<!DOCTYPE html>
    //     <html lang="en">
    //     <head>
    //         <meta charset="UTF-8">
    //         <title>Node Practice</title>
    //     </head>
    //     <body>
    //        <h1>Hello Node!</h1>
    //     </body>
    //     </html>`);

    if (req.url === '/') {
        fs.createReadStream('./templates/index.html').pipe(res);
    } else if(req.url === '/about') {
        fs.createReadStream('./templates/about.html').pipe(res);
    } else {
        fs.createReadStream('./templates/error.html').pipe(res);
    }

});
server.listen(PORT, HOST, () => {
    console.log(`Сервер запущен: http://${HOST}: ${PORT}`);
});





