const express = require('express');

const app = express();
const PORT = 3000;
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({extended: false}));

app.get('/', (req, res) => {
    res.render('index');
});
app.get('/about', (req, res) => {
    res.render('about');
});
app.get('/user/:username', (req, res) => {
    let data = { username: req.params.username, hobbies: ['sex', 'game',  'auto'] };
    res.render('user', data );
});

app.post('/check-user', (req, res) => {
    const userName = req.body.username;
    if(userName === '') {
        return res.redirect('/')
    } else {
        return res.redirect('/user/' + userName);
    }
})

app.listen(PORT, () => {
    console.log(`Server in Port: ${PORT}`);
})





